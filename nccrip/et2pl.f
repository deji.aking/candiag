      subroutine et2pl(fplev,lnsp,feta,cpls,cels,npl,nel,
     &                 icoord,ptop,nwrds)
c***********************************************************************
c Given an array of GRIDs on eta levels, return an array of GRIDs on
c pressure levels.
c
c The pressure level fields returned by et2pl will be identical to
c those created by gsapl given identical input for both routines.
c 
c
c Input
c   integer nwrds        ...length of a single GRID record
c   real lnsp(nwrds)     ...log of surface pressure
c   real feta(nwrds,nel) ...grids on eta levels
c   integer npl          ...number of coded pressure levels
c   integer cpls(npl)    ...coded pressure levels
c   integer nel          ...number of coded eta levels
c   integer cels(nel)    ...coded eta levels
c   integer icoord       ...type of eta coordinate
c   real ptop            ...lid pressure in Pa
c
c Output
c   real fplev(nwrds,npl) ...output field on pressure levels
c
c Larry Solheim ...Jun,2009
c $Id: et2pl.f 646 2011-04-15 23:49:14Z acrnrls $
c***********************************************************************
      implicit none

      !--- Input/Output
      integer :: nwrds
      real :: fplev(nwrds,npl)   !--- Output field on pressure levels
      real :: lnsp(nwrds)        !--- Input log(surface pressure)
      real :: feta(nwrds,nel)    !--- Input field on eta levels
      integer :: cpls(npl), npl  !--- coded pressure levels (input)
      integer :: cels(nel), nel  !--- coded eta levels (input)
      integer :: icoord
      real :: ptop

      !--- local
      integer, parameter :: maxlev=200
      real :: plevs(maxlev), log_plevs(maxlev), eta(maxlev)
      real :: a(maxlev),b(maxlev)
      real :: sig(maxlev),fsig(maxlev),dflnsig(maxlev+1),dlnsig(maxlev)

      integer :: idx1, idx2, n, nc4to8
      real :: rlup,rldn

      integer, parameter :: maxw=129*256*201
      real :: wrk(maxw)
c-----------------------------------------------------------------------

      if (nel.gt.maxlev .or. npl.gt.maxlev) then
        write(6,*)'ET2PL: Max levels exceeded ',nel,npl,maxlev
        call xit("ET2PL",-1)
      endif

      rlup=0.0
      rldn=0.0

      if(icoord.eq.nc4to8("    ")) icoord=nc4to8(" SIG")
      if(icoord.eq.nc4to8(" SIG")) then
        ptop=max(ptop, 0.00e0)
      else
        ptop=max(ptop, 0.100e-09)
      endif

      !--- Determine pressure levels to be interpolated to
      !--- from input coded pressure levels
      if ( any(cpls(1:npl) .ge. 99101) ) then
        !--- If any of the input coded pressure level values are > 99100 then use
        !--- the modified decoding scheme with higher precision between 10 and 100 hPa
        call lvdcode_ep(plevs,cpls,npl)
      else
        !--- Otherwise use the standard decoding scheme
        call lvdcode(plevs,cpls,npl)
      endif
      if (npl.gt.1) then
        do idx1=2,npl
          if (plevs(idx1).le.plevs(idx1-1)) then
            write(6,*)'ET2PL: Non monotonic pressure levels'
            write(6,*)'Input PLEVS: ',cpls(1:npl)
            write(6,*)' PLEVS after lvdcode: ',plevs(1:npl)
            call xit("ET2PL",-2)
          endif
        enddo
      endif
      do idx1=1,npl
        log_plevs(idx1) = log(plevs(idx1))
      enddo

      !--- Determine eta levels from input coded eta levels
      if ( any(cpls(1:npl) .ge. 99101) ) then
        !--- If any of the input coded eta level values are > 99100 then use
        !--- the modified decoding scheme with higher precision between 10 and 100
        call lvdcode_ep(eta,cels,nel)
      else
        !--- Otherwise use the standard decoding scheme
        call lvdcode(eta,cels,nel)
      endif
      eta(1:nel) = 0.001e0*eta(1:nel)

      !--- Get vertical coordinate coefficients
      call coordab (a, b, nel, eta, icoord, ptop)

      !--- Assign the wrk array used by eapl
      !--- The first nwrds values are lnsp
      wrk(:)=0.0
      wrk(1:nwrds) = lnsp(1:nwrds)
      do idx1=1,nel
        !--- Start at an offset of nwrds+1 into the WRK array
        !--- for the input field values
        idx2=idx1*nwrds+1
        wrk(idx2:idx2+nwrds-1) = feta(1:nwrds,idx1)
      enddo

      n=nwrds+1
      call eapl(wrk(n),nwrds,log_plevs,npl,wrk(n),sig,nel,wrk,
     1          rlup,rldn,a,b,nel+1,fsig,dflnsig,dlnsig)

      !--- Assign the output array containing field values
      !--- on the specified pressure levels
      do idx1=1,npl
        !--- Start at an offset of nwrds+1 into the WRK array
        idx2=idx1*nwrds+1
        fplev(1:nwrds,idx1) = wrk(idx2:idx2+nwrds-1)
      enddo

      end
