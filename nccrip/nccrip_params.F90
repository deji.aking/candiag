module nccrip_params
    !--- Parameters that may be changed by the user

    !--- only_query_file is a logical flag to indicate that the first few
    !--- records of the input data file should be read to determine the
    !--- horizontal and vertical dimension sizes. This info is then written
    !--- to stdout and the program stops.
    logical :: only_query_file = .false.

    !--- query_file_for_this is a character string used to indicate what
    !--- should be output by the file query when only_query_file = T
    !--- Valid values are: all, name
    character(len=10) :: query_file_for_this = "all"

    !--- Model parameters

    !--- runid for the current file
    character(len=64) :: runid = " "
    logical :: runid_from_file=.false.
    logical :: runid_from_cmdl=.false.

    !--- delt is the ATM time step in seconds
    real    :: delt=-1.0
    logical :: delt_from_file=.false.
    logical :: delt_from_cmdl=.false.

    !--- ocn_delt is the OCN time step in seconds
    real    :: ocn_delt=-1.0
    logical :: ocn_delt_from_file=.false.
    logical :: ocn_delt_from_cmdl=.false.

    !--- plid is the pressure (in Pa) at the top of the model where the
    !--- upper boundary condition is applied
    !--- This must be set correctly when model levels are used
    real    :: plid=-1.0
    logical :: plid_from_file=.false.
    logical :: plid_from_cmdl=.false.

    !--- coord and moist must be char*5 becuase this is how they
    !--- are typically written in a model submission job and therefore
    !--- this is how they are read in def_modpar

    !--- coord is the char form of the variable that determines the
    !--- type of vertical discretization. Possible values are:
    !--- " SIG", "ET10"  => sigma coords
    !--- " ETA", "ET15"  => hybrid (eta) coords
    character(len=5) :: coord='     '
    !--- icoord is the integer form of coord (must be set with nc4to8)
    integer :: icoord=-1
    logical :: coord_from_file=.false.
    logical :: coord_from_cmdl=.false.

    !--- moist identifies the moisture variable use in the model
    !--- typical values are "   Q", "QHYB", "SL3D", "SLQB"
    character(len=5) :: moist='     '
    !--- imoist is the integer form of moist (must be set with nc4to8)
    integer :: imoist=-1
    logical :: moist_from_file=.false.
    logical :: moist_from_cmdl=.false.

    !--- reference values for hybrid moisture variable
    real    :: sref=-1.0
    logical :: sref_from_file=.false.
    logical :: sref_from_cmdl=.false.

    !--- exponent for hybrid moisture variable
    real    :: spow=1.0
    logical :: spow_from_file=.false.
    logical :: spow_from_cmdl=.false.

    !--- lay determines the position of the base model levels
    !--- lay=2 is the only value used in practice
    !--- but lay=0,1,2,3,4 is allowed
    integer :: lay=2

    !--- A user input file name containing data description variables
    !--- Currently only a PARM record is processesed, if present
    !--- This will be read before any other files are read
    character(256) :: data_desc_file = " "

    !--- Verbosity flag, a larger value means more output
    integer :: verbose=0

    !--- Include or exclude specific variables or superlabels by adding them to
    !--- one of the following lists
    integer, parameter :: max_exclude=500
    character(len=4)  :: exclude_name(max_exclude)
    character(len=60) :: exclude_label(max_exclude)
    integer :: n_exclude_name=0, n_exclude_label=0

    integer, parameter :: max_include=500
    character(len=4)  :: include_name(max_include)
    character(len=60) :: include_label(max_include)
    integer :: n_include_name=0, n_include_label=0

    !--- start_time and stop_time are used to impose time bounds on all variables
    !--- When a record is read, the ibuf2 value is compared with (start|stop)_time
    !--- and if that value is less than start_time or greater than stop_time then
    !--- the record is not used
    !--- The value of -1 means that (start|stop)_time is not used
    !--- Either variable may be set by the user
    integer(kind=8) :: start_time = -1
    integer(kind=8) :: stop_time  = -1

    !--- file_prefix is used as a prefix on all output file names
    character(len=64) :: file_prefix = " "

    logical :: file_prefix_from_cmdl = .false.
    logical :: with_file_prefix = .true.

    !--- file_suffix is used as a suffix on all output file names
    character(len=64) :: file_suffix = " "

    !--- A logical flag used to determine whether or not a date range will be
    !--- added to output netcdf file names
    logical :: add_date_to_ncout = .true.
    logical :: add_date_to_ncout_from_cmdl = .false.

    !--- A logical flag to indicate that a text file (or files) containing
    !--- lists of all output files should be created
    logical :: write_file_list = .false.

    !--- flist_out will be the name of the file containing a list of all output files
    !--- that is created when write_file_list = T
    !--- If not set by the user then a default name is used
    character(len=256) :: flist_out = " "

    !--- The name of a directory into which output files will be placed
    !--- If this is blank then output files will remain in the execution dir
    character(len=256) :: dataout_dir = " "

    !--- Normally only netcdf files are created for output
    !--- Setting write_ccout true will cause the output data to also be written
    !--- to CCCma binary format files
    logical :: write_ccout = .false.

    !--- Normally temporary files, containing CCCma binary data, are removed
    !--- when they are no longer needed
    !--- Setting keep_tmp_files true will leave these temporary files on disk
    logical :: keep_tmp_files = .false.

    !--- data_in_mem is a logical flag to indicate that all data records
    !--- that are read from input files are to be stored in memory until
    !--- they are written to the output file
    logical :: data_in_mem = .true.

    !--- convert_sp2gg controls the conversion of SPEC records to GRID records
    !--- when SPEC records are read from an input file
    logical :: convert_sp2gg = .true.

    !--- The fillvalue used in the output netcdf file
    !--- This value will replace any 1E38 value found in the input CCCma file
    !--- and the _FillValue attribute will be added to the output netcdf file
    real(kind=4) :: fillvalue = 1e38
    real(kind=8) :: fillvalue8 = 1e38_8
    logical :: add_fillvalue_attr = .true.

    !--- fillvalue_from_cmdl flags a user supplied value for _FillValue
    logical :: fillvalue_from_cmdl = .false.

    !--- Use type float (real) for coordinate variables
    !--- If this is false then coordinate variables be type double
    logical :: coord_as_real = .false.

    !--- Use type float (real) for regular variables
    !--- If this is false then regular variables be type double
    logical :: var_as_real = .true.

    !--- grid_type is an integer flag indicating the type of horizontal grid
    !--- that is found in the input CCCma binary data file
    !--- grid_type = 0  ==> gausian grid
    !--- grid_type = 1  ==> regular lat-lon grid
    !--- grid_type = 2  ==> offset lat-lon grid
    integer :: grid_type = 0

    !--- ib4_in and ib4_out are strings that will be used to identify the type of
    !--- levels (e.g. plev, mlev, olev,...) that appear in input and output records
    !--- These may be set by the user via command line options (in which case they
    !--- will be used for all variables) or set internally according to input file
    !--- type and/or certain variable specific information
    character(32) :: ib4_in=" "
    character(32) :: ib4_out=" "
    logical :: ib4_in_from_cmdl = .false.
    logical :: ib4_out_from_cmdl = .false.

    !--- A user supplied set of pressure levels (in hPa)
    !--- These will be used as the output pressure levels when pressure levels are
    !--- requested and no variable specific levels are defined
    !--- If usr_plev is not defined then an internal default is used
    integer :: usr_nplev = 0
    real(kind=8), allocatable :: usr_plev(:)

    !--- A user supplied set of coded output levels
    !--- If usr_lev is not defined then all input levels are output
    integer :: usr_nlev = 0
    integer, allocatable :: usr_lev(:)

    !--- Logical flag to inidicate that the ibuf7 value is to be used to define
    !--- and extra dimension
    logical :: ib7_as_dim = .false.

    !--- ocn_vel_grid = T means use the ocean velocity grid
    !--- ocn_vel_grid = F means use the ocean tracer grid
    logical :: ocn_vel_grid = .false.

    !--- Logical flag to indicate that the vertical level should be reversed
    logical :: reverse_levs = .false.

    !--- Logical flag to indicate that degenerate dimensions are to be written
    !--- to the output netcdf file
    logical :: with_degenerate_dims = .false.

    !--- Logical flag to indicate that there are multiple realizations of variables
    !--- that appear in the input files (ie multiple ensemble members)
    logical :: with_ensemble = .false.

    !--- n_ensemble may be set by the user to define the number of ensemble members
    !--- This is required when multiple files containing different ensemble members
    !--- are present on the commmand line. It allows the program to define the netcdf
    !--- file before all these file have been read.
    integer :: n_ensemble = 0

    !--- psfile is the name of a file containing the surface pressure defined
    !--- at the same set of times as the records in the input CCCma data file
    !--- This is only used when the input variable is defined on atm model levels.
    character(256) :: psfile = " "

    !--- Did the user supply a psfle name on the commmand line
    logical :: psfile_from_cmdl = .false.

    !--- Is it permissable to allow surface pressure records to be of type "DATA"
    logical :: allow_ps_as_data = .false.

    !--- The ps_grid list stored in memory (and read from psfile) is stored
    !--- sequentially in time
    logical :: seq_ps_grid = .false.

    !--- This logical flag is used to determine is surface pressure is to be added to the
    !--- output netcdf file. Normally ps is added when model level fields are written.
    logical :: add_ps_to_nc = .false.

    !--- strict_ps_time is a logical flag used to allow or not allow the time (ibuf2)
    !--- that is read from psfile to differ from the time read from the corresponding
    !--- file containing the variable to be converted
    logical :: strict_ps_time = .true.
    logical :: strict_ps_time_from_cmdl = .false.

    !--- A flag to indicate that surface pressure needs to be converted from hPa to Pa
    !--- If ps_convert_hPa_to_Pa is false then ps read from the file is used "as is"
    logical :: ps_convert_hPa_to_Pa = .true.

    !--- require_lnsp is used to indicate that surface pressure is required
    !--- When require_lnsp = T then LNSP and PSA will always be read from input files
    !--- even if they are not explicitly or implicitly requested
    logical :: require_lnsp = .true.

    !--- ftype is the input file type (e.g. gp|xp|gs|cp|...)
    !--- This is a global value that, if set, will superceed any other value
    character(len=10)  :: ftype = " "
    logical :: ftype_from_cmdl = .false.

    !--- Logical flag to indicate whether or not the LABL associated with any
    !--- record read from a CCCma format input file is used in matching
    !--- this record with any previous record read to determine if both
    !--- records belong to the same variable
    logical :: var_match_label = .true.

    !--- Logical flag to indicate that all input files are CCCma time series
    logical :: cccma_time_series_in = .false.

    !--- Logical flag to indicate whether or not the "axis" attribute is added
    !--- to coordinate variables. (axis = X, Y, Z or T corresponding to coordinate)
    logical :: add_axis_attr = .false.

    !--- Logical flag to indicate whether or not a "label" attribute is added
    !--- to any variable that has a non-blank LABL associated with it
    logical :: add_label_attr = .false.
    logical :: add_label_attr_from_cmdl = .false.

    !--- The units for time used in the output netcdf file
    !---     valid values: days, hours, minutes, seconds
    !--- or certain abbreviations of these 4 words
    character(64) :: time_units = "days"

    !--- The string that is used for the "units" meta data associated with time
    !--- This string must be of the form: "time_units since Y-M-D H:M:S"
    !--- It can be set by the user on the command line. If it is not set by the user
    !--- then is gets set internally in the nccrip_cmdline subroutine.
    character(64) :: time_units_attrib = " "
    logical :: time_units_attrib_from_cmdl = .false.

    !--- An array to hold the reference time as (Y,M,D,H,M,S)
    integer :: gref_ymd_hms(6)

    !--- The user may specify the format of ibuf(2) that appears in the input CCCma format field.
    !--- If this is not set by the user the program will guess at the ibuf(2) format.
    !---     valid values: YYYY, YYYYMM, YYYYMMDD, YYYYMMDDHH, MODEL, OMODEL, RAW
    character(32) :: usr_ib2_fmt = " "

    !--- int_ib2_fmt is an internally defined varaible that will be set to the ibuf2 format
    !--- in certain circumstances. Any value in usr_ib2_fmt will superceed int_ib2_fmt
    character(32) :: int_ib2_fmt = " "

    !--- int_ib2_(year|mon) will be set internaly in certain circumstances and can
    !--- be used to define a YYYYMM format ibuf2 value for monthly averaged files
    !--- that do not contain a meaningful ibuf2 value, such as gp or xp files
    integer :: int_ib2_year = -1
    integer :: int_ib2_mon  = -1

    !--- Allow the user to define an ibuf2 format for specific variables that
    !--- will superceed any other ibuf2 format specifications
    integer, parameter :: max_var_ib2fmt=100
    !--- var_ib2fmt_toupper = .true. means variable names supplied by the user are
    !--- always converted to upper case. Otherwise they are used verbatim.
    logical :: var_ib2fmt_toupper = .true.
    !--- var_ib2fmt_model will contain a list of variables that require MODEL ibuf2 format
    character(len=4)  :: var_ib2fmt_model(max_var_ib2fmt)
    integer :: n_var_ib2fmt_model=0
    !--- var_ib2fmt_omodel will contain a list of variables that require OMODEL ibuf2 format
    character(len=4)  :: var_ib2fmt_omodel(max_var_ib2fmt)
    integer :: n_var_ib2fmt_omodel=0
    !--- var_ib2fmt_raw will contain a list of variables that require RAW ibuf2 format
    character(len=4)  :: var_ib2fmt_raw(max_var_ib2fmt)
    integer :: n_var_ib2fmt_raw=0
    !--- var_ib2fmt_yyyymmddhh will contain a list of variables that require YYYYMMDDHH ibuf2 format
    character(len=4)  :: var_ib2fmt_yyyymmddhh(max_var_ib2fmt)
    integer :: n_var_ib2fmt_yyyymmddhh=0
    !--- var_ib2fmt_yyyymmdd will contain a list of variables that require YYYYMMDD ibuf2 format
    character(len=4)  :: var_ib2fmt_yyyymmdd(max_var_ib2fmt)
    integer :: n_var_ib2fmt_yyyymmdd=0
    !--- var_ib2fmt_yyyymm will contain a list of variables that require YYYYMM ibuf2 format
    character(len=4)  :: var_ib2fmt_yyyymm(max_var_ib2fmt)
    integer :: n_var_ib2fmt_yyyymm=0
    !--- var_ib2fmt_yyyy will contain a list of variables that require YYYY ibuf2 format
    character(len=4)  :: var_ib2fmt_yyyy(max_var_ib2fmt)
    integer :: n_var_ib2fmt_yyyy=0

    !--- parm_(year|mon) will be set internaly in certain circumstances
    !--- These variables will contain the value of year and mon found in the PARM
    !--- record of an input file, if a PARM record exists in that file
    integer :: parm_year = -1
    integer :: parm_mon  = -1
    logical :: year_from_file=.false.
    logical :: mon_from_file=.false.

    !--- A user supplied value for the time interval between successive records
    !---     valid values: N days, N hours, N minutes, N seconds
    !--- where N is numeric
    !--- If supplied the character string time_interval will be converted to an
    !--- integer time_interval_secs providing this interval in seconds
    character(64) :: time_interval = " "
    integer(kind=8) :: time_interval_secs = -1
    logical :: time_interval_from_cmdl = .false.

    !--- dim_time_interval is an internal variable used to hold the dimensional value
    !--- of the time interval between successive records. This is used to determine
    !--- a lower and upper boundary for each time element. If set then this value will
    !--- override any other internal time bounds value, such as that determine by set_time.
    real(kind=8) :: dim_time_interval = -1.d0

    !--- set_dim_time_interval is used to determine whether or not the internal variable
    !--- dim_time_interval is to be calculated. In some cases it will be desirable to
    !--- use the default time bounds determine in set_time. In these cases the parameter
    !--- set_dim_time_interval will be assigned a false value. The user may also disable
    !--- the internal calculation of dim_time_interval via a command line option.
    logical :: set_dim_time_interval = .true.

    !--- time_shift is used in ibuf2_time to determine how ibuf2 values are to be interpreted
    !--- time_shift =  0 means the output time value is the dimensional equivalent to ibu2
    !--- time_shift = -1 means the output time value is the dimensional equivalent to ibu2 - dt/2
    !--- time_shift =  1 means the output time value is the dimensional equivalent to ibu2 + dt/2
    !--- where dt is the time interval and depends on the ibuf2 format (e.g. if ib2_fmt is YYYY
    !--- then dt is 1 year, ib2_fmt is YYYMM then dt is 1 month etc)
    !--- The value -99 is used to indicate that this has not been set by the user
    integer :: time_shift = -99

    !--- time_offset may be set by the user and will be used to determine time_offset_secs
    !--- time_offset_secs will be added to the time value returned from ibuf2_time
    !--- This offset may be either positive (ibuf2 value is offset forward in time)
    !--- or negative (ibuf2 value is offset backward in time)
    !--- Valid values for time_offset: N days|hours|minutes|seconds where N is numeric
    character(64)   :: time_offset = " "
    integer(kind=8) :: time_offset_secs = 0

    !--- time_cell_methods, if non-blank, will be used to define the
    !--- cell_methods attribute for the time coordinate
    character(64)  :: time_cell_methods = ' '
    logical :: add_time_cell_methods = .false.

    !--- add_time_bnds flags the addition of time_bnds to the output netcdf file
    logical :: add_time_bnds = .false.

    !--- with_inv0 = T means records with ibuf2 = 0 are assumed to contain invariant fields
    !--- with_inv0 = F means records with ibuf2 = 0 are part of this fields time series
    logical :: with_inv0 = .true.

    !--- Logical flag used to indicate that the last longitude is to be removed
    !--- or not removed from data read from a CCCma format file.
    !--- This will normally be the overlap longitude at 360 degrees E
    logical :: remove_cyclic_lon = .false.

    !--- Logical flags used to idicate how the data is represented over
    !--- a time interval (e.g. average, minima, maxima, instantaneous)
    logical :: time_avg = .false.
    logical :: time_min = .false.
    logical :: time_max = .false.
    logical :: time_point = .false.

    !---------------------------
    !--- cmor related parameters
    !---------------------------

    !--- Logical flag to indicate that cmor should be used to
    !--- create and write the output netcdf file
    logical :: use_cmor = .false.

    !--- The name of the MIP table to use (required)
    !--- Valid names are:
    !---   3hr  6hrLev  6hrPlev  aero   Amon  cf3hr  cfDay  cfMon  cfOff  cfSites
    !---   day  fx      grids    Limon  Lmon  Oclim  Oimon  Omon   Oyr
    character(64) :: mip_table = ' '

    !--- The MIP variable name for the current variable (found in current table)
    character(64) :: mip_var_name = ' '

    !--- The MIP variable name for the current variable (found in current table)
    character(64) :: mip_var_units = ' '

    !--- Directory where the mip table may be found
    !--- The file containg the mip table will be found directly below table_dir_cmor
    !--- e.g. table_dir_cmor/CMIP5_3hr  or  table_dir_cmor/CMIP5_Amon
    character(256) :: table_dir_cmor = ' '

    !--- The name of the file containing the grids table used in some cases
    character(64) :: grids_table_file = "CMIP5_grids"

    !--- The name of the project to which the mip tables belong
    !--- This project name is used as a prefix for file names that contain
    !--- individual mip tables (e.g. CMIP5_Amon or CMIP5_3hr)
    character(64) :: project = 'CMIP5'

    !--- A boolean flag used to determine if cmor will create a directory tree
    !--- below "outpath_cmor" that is consistent with the following structure
    !--- <activity>/<product>/<institute_id>/<model_id>/<experiment>/<frequency>/
    !---   <modeling_realm>/<variable_name>/<ensemble member>
    integer :: create_subdirs_cmor = 0

    !--- A string containing dimension names as they appear in cmor tables
    character(256) :: cmor_dimensions = ' '

    !--- Pathname of dir in which the output netcdf file will be created when
    !--- netcdf files are create using cmor routines
    character(256) :: outpath_cmor = ' '

    !--- The runid and experiment id associated with the run
    !--- that spawned the current run
    character(64) :: parent_runid = ' '
    character(64) :: parent_experiment_id = ' '
    character(64) :: parent_experiment = ' '

    !--- MIP experiment id
    character(64)   :: experiment_id = ' '
    character(64)   :: experiment = ' '

    !--- Ensemble member
    integer :: realization = 1

    !--- Identify initialization method or observational datasets used in the model
    integer :: initialization_method = 1

    !--- Physics version used in the model
    integer :: physics_version = 1

    !--- A string identifying the type of forcing used in the current run
    !--- This attribute is required by CMOR
    !--- Set the default to "N/A" so that CMOR does not see and empty string
    character(1024) :: forcing = 'N/A'

    !--- Misc meta data
    character(1024) :: source = ' '
    character(1024) :: global_comment = ' '
    character(1024) :: var_comment = ' '
    character(1024) :: references = 'http://www.cccma.ec.gc.ca/models'
    character(64)   :: original_name
    character(64)   :: model_id = ' '
    character(64)   :: positive = ' '
    character(256)  :: institution = &
    'CCCma (Canadian Centre for Climate Modelling and Analysis, Victoria, BC, Canada)'
    character(64)   :: institute_id = 'CCCma'
    character(64)   :: contact = 'cccma_info@ec.gc.ca'
    character(256)  :: title = ' '
    character(64)   :: calendar = '365_day'

    !--- Esemble member/initialization method/physics version of the parent experiment
    !--- e.g. r1i1p1
    character(64)   :: parent_experiment_rip = ' '

    !--- Branch time
    real(kind=8) :: branch_time = 0.d0

    !--- Branch time as a string in Y:M:D:H format
    character(64) :: branch_time_YMDH = ''

    namelist /nccrip_par/ &
      add_time_bnds, remove_cyclic_lon, with_degenerate_dims, gref_ymd_hms, delt, &
      time_units_attrib, time_cell_methods, usr_ib2_fmt, &
      dataout_dir, &
      mip_table, mip_var_name, mip_var_units, table_dir_cmor, project, &
      create_subdirs_cmor, outpath_cmor, parent_runid, &
      experiment, experiment_id, parent_experiment, parent_experiment_id, &
      forcing, realization, initialization_method, physics_version, source, &
      global_comment, references, model_id, institution, institute_id, contact, &
      calendar, parent_experiment_rip, branch_time, branch_time_YMDH, title, &
      grids_table_file, positive, cmor_dimensions, var_comment, original_name

end module nccrip_params
