      SUBROUTINE FMMM3(FMIN,FMAX,FMEAN,F,LA,SPVAL,NSPV)
C 
C     * NOV 02/04 - S.KHARIN (SKIP "SPVAL" SPECIAL VALUES)
C     * COMPUTES MIN,MAX,MEAN OF FIELD F(LA) (BASED ON FMMM2)
C 
C     * LEVEL 2,F 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL F(LA)
C-------------------------------------------------------------------- 
C 
      FSUM=0.E0 
      FMIN=+1.E+38
      FMAX=-1.E+38
C 
      NSPV=0
      DO 210 I=1,LA 
         IF(F(I).NE.SPVAL) THEN
            IF(F(I).LT.FMIN) FMIN=F(I)
            IF(F(I).GT.FMAX) FMAX=F(I)
            FSUM=FSUM+F(I)
         ELSE
            NSPV=NSPV+1
         ENDIF
 210  CONTINUE
      IF(LA.EQ.NSPV) THEN
        FMEAN=SPVAL
      ELSE
        FMEAN=FSUM/FLOAT(LA-NSPV)
      ENDIF
C 
      RETURN
      END 
