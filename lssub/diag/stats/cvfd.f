      FUNCTION CVFD(P,DF1,DF2)
C
C     * CVFD RETURNS P UPPER TAIL CRITICAL VALUE OF THE 
C     * F-DISTRIBUTION WITH DF1 AND DF2 DEGREES OF FREEDOM.
C
C     * 7/APR 1999
C
C     * AUTHOR: SLAVA KHARIN
C     * (BISECTION METHOD)
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DATA MAXIT/100/,EPS/1.E-6/
C--------------------------------------------------------------------
      T1=1.E-3
      T2=1.E+3
      P1=1.E0-BETAI1(0.5E0*DF2,0.5E0*DF1,DF2/(DF2+DF1*T1))
      P2=1.E0-BETAI1(0.5E0*DF2,0.5E0*DF1,DF2/(DF2+DF1*T2))
      IF ((P1-P)*(P2-P).GT.0.E0) THEN
         WRITE(6,'(A)')
     1        ' *** ERROR IN CVFD: ROOT MUST BE BRACKETED.'
         CALL                                      XIT('CVFD',-1)
      ENDIF
      DO IT=1,MAXIT
         T=0.5E0*(T1+T2)
         P1=1.E0-BETAI1(0.5E0*DF2,0.5E0*DF1,DF2/(DF2+DF1*T))
         IF (P1.GT.P) THEN
            T2=T
         ELSE
            T1=T
         ENDIF
         IF (T2-T1.LT.EPS) GOTO 100
      ENDDO
C
      WRITE(6,'(A)')
     1     ' *** ERROR IN CVFD: TOO MANY BISECTIONS.'
      CALL                                         XIT('CVFD',-2)
C
 100  CVFD=T
      RETURN
      END
