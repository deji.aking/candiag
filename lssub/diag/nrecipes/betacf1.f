      FUNCTION BETACF1(A,B,X)
C
C     * THIS FUNCTION IS TAKEN FROM NUMERICAL RECIPES.
C     * PAUSE STATEMENT IS REPLACED BY A CALL TO "XIT" SUBROUTINE.
C     * THE ORIGINAL NAME "BETACF" IS CHANGED TO "BETACF1".
C
C     JUN 28/96 - SLAVA KHARIN
C
C  (C) COPR. 1986-92 NUMERICAL RECIPES SOFTWARE #=!E=#,)]UBCJ.
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER MAXIT
      REAL BETACF1,A,B,X,EPS,FPMIN
      PARAMETER (MAXIT=100,EPS=3.E-7,FPMIN=1.E-30)
      INTEGER M,M2
      REAL AA,C,D,DEL,H,QAB,QAM,QAP
C--------------------------------------------------------------------
C
      QAB=A+B
      QAP=A+1.E0
      QAM=A-1.E0
      C=1.E0
      D=1.E0-QAB*X/QAP
      IF(ABS(D).LT.FPMIN)D=FPMIN
      D=1.E0/D
      H=D
      DO 10 M=1,MAXIT
        M2=2*M
        AA=M*(B-M)*X/((QAM+M2)*(A+M2))
        D=1.E0+AA*D
        IF(ABS(D).LT.FPMIN)D=FPMIN
        C=1.E0+AA/C
        IF(ABS(C).LT.FPMIN)C=FPMIN
        D=1.E0/D
        H=H*D*C
        AA=-(A+M)*(QAB+M)*X/((A+M2)*(QAP+M2))
        D=1.E0+AA*D
        IF(ABS(D).LT.FPMIN)D=FPMIN
        C=1.E0+AA/C
        IF(ABS(C).LT.FPMIN)C=FPMIN
        D=1.E0/D
        DEL=D*C
        H=H*DEL
        IF(ABS(DEL-1.E0).LT.EPS) GOTO 100
   10 CONTINUE
C
      WRITE (6,'(A)') 
     1  ' *** ERROR IN BETACF1: A OR B TOO BIG, OR MAXIT TOO SMALL.'

      CALL                                         XIT('BETACF1',-1)
C
  100 BETACF1=H

      RETURN
      END
