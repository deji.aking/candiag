      SUBROUTINE FXLRLMT (LRLMT,LR,LM,KTR)
C 
C     * FEB 11/94 - F.MAJAESS 
C 
C     * FORM LRLMT FROM LR, LM AND KTR AS FOLLOWS:
C 
C     * IF (LR.OR.LM) > 99 THEN LRLMT=10000*LR+10*LM+KTR
C     * OTHERWISE               LRLMT= 1000*LR+10*LM+KTR
C
C     *  WHERE  LR = LENGTH OF FIRST SPECTRAL ROW.
C     *         LM = NUMBER OF SPECTRAL ROWS. 
C     *        KTR = TRUNCATION TYPE. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LRLMT,LR,LM,KTR 
C-------------------------------------------------------------------- 
C 
      IF ( LR.GT.99 .OR. LM.GT.99 ) THEN
        LRLMT=10000*LR+10*LM+KTR
      ELSE
        LRLMT= 1000*LR+10*LM+KTR
      ENDIF
C
      RETURN
      END
