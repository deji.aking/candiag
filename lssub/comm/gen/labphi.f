      SUBROUTINE LABPHI (LSPHI,DIALNSH, SH,ILEV)
C 
C     * JAN 19/88 - R.LAPRISE.
C     * CALCULATE THE DIAGNOSTIC LEVELS FOR PHI FROM THE TEMPERATURE
C     * LEVELS IN THE GENERALIZED STAGGERED OR NOT VERSION OF GCM,
C     * SO THAT THE STANDARD DIAGNOSTIC WILL AUTOMATICALLY RECOVER
C     * THE CORRECT TEMPERATURES. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LSPHI(ILEV) 
C 
      REAL  DIALNSH(ILEV), SH(ILEV) 
C------------------------------------------------------------------ 
      SHB=1.0E0 
      DO 100 L=ILEV,1,-1
         LSPHI(L)  =INT( (SH(L)**2/SHB)*1000.E0+0.5E0)
         SHT       =FLOAT(LSPHI(L))*0.001E0 
         DIALNSH(L)=LOG (SHB/SHT)
         SHB       =SHT 
  100 CONTINUE
C 
C     * CHECK REVERSIBILITY.
C 
      SHB=1.0E0 
      DO 200 L=ILEV,1,-1
         SHT       =FLOAT(LSPHI(L))*0.001E0 
         SIG       =SQRT (SHB*SHT)
         LAB1      =INT( SIG  *1000.E0+0.5E0) 
         LAB2      =INT( SH(L)*1000.E0+0.5E0) 
         IF(LAB1.NE.LAB2)THEN 
            WRITE(6,6030)LAB1,LAB2
            CALL XIT('LABPHI',-1) 
         ENDIF
         SHB       =SHT 
  200 CONTINUE
C 
      WRITE(6,6000)LSPHI
      WRITE(6,6010)DIALNSH
      RETURN
C-----------------------------------------------------------------------
 6000 FORMAT(' LAB PHI  (DIAG) = ',10I6)
 6010 FORMAT(' D LN SIG (DIAG) = ',3X,10F7.4) 
 6030 FORMAT(' LAB1, LAB2= ',2I10)
      END
