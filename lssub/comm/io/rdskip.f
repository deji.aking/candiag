      SUBROUTINE RDSKIP(NF,LENGTH,OK)
C 
C     * 2003 MARCH 30 - ALAN IRWIN
C 
C     * SKIP A RECORD FROM FILE NF.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
C---------------------------------------------------------------------
      OK=.FALSE.
      READ(NF,END=910,ERR=900)
      OK=.TRUE.
  900 RETURN
C
  910 LENGTH=0
      RETURN
C---------------------------------------------------------------------
      END 
