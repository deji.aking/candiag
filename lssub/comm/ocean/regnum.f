      SUBROUTINE REGNUM(CHKSTR,NR,NIDREG,NMAX)
C
C     * JUL 19/01 - W.G. LEE
C
C     * SUBROUTINE REGNUM IS PASSED A 4 CHARACTER REGION NAME
C     * AND SENDS BACK THE NUMBER OF SUB-REGIONS ASSOCIATED WITH
C     * THIS NAME.
C
C---------------------------------------------------------------------
C
C     * INPUT PARAMETERS...
C                                                                               
C     * CHKSTR  = REGION NAME (STRING)
C     * NMAX    = MAXIMUM POSSIBLE NUMBER OF SUBREGIONS
C
C     * OUTPUT PARAMETERS...
C
C     * NR      = NUMBER OF SUBREGIONS RELATED TO CHKSTR
C     * NIDREG  = SUBREGION NUMBER ARRAY 
C
C---------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER (NTOTAL=23)
      INTEGER NIDREG(NMAX)
      CHARACTER*37 REGDAT(NTOTAL)
      CHARACTER*4  REGNAM, CHKSTR
C
C---------------------------------------------------------------------
C
      REGDAT(1)  = "NATL  1  1                           "
      REGDAT(2)  = "SATL  1  2                           "
      REGDAT(3)  = "NPAC  1  3                           "
      REGDAT(4)  = "SPAC  1  4                           "
      REGDAT(5)  = "NIND  1  5                           "
      REGDAT(6)  = "SIND  1  6                           "
      REGDAT(7)  = "ARCT  1  7                           "
      REGDAT(8)  = "MEDT  1  8                           "
      REGDAT(9)  = "HBAY  1  9                           "
      REGDAT(10) = "BLKS  1 10                           "
      REGDAT(11) = "CASP  1 11                           "
      REGDAT(12) = "BALT  1 12                           "
      REGDAT(13) = "LAKE  1 21                           "
C      
      REGDAT(14) = "GLOB  9  1  2  3  4  5  6  7  8  9   "
      REGDAT(15) = " ATL  3  1  2  7                     "
      REGDAT(16) = " PAC  2  3  4                        "
      REGDAT(17) = " IND  2  5  6                        "
      REGDAT(18) = "NHEM  6  1  3  5  7  8  9            "
      REGDAT(19) = "SHEM  3  2  4  6                     "
C 
      REGDAT(20) = "LAND  1  0                           "
      REGDAT(21) = "MARS  3 10 11 12                     "
      REGDAT(22) = "NOTO  5  0 21 10 11 12               "
      REGDAT(23) = "PA_I  4  3  4  5  6                  "
C
      DO 200 N = 1,NTOTAL
         READ(REGDAT(N),100) REGNAM,NR,(NIDREG(NN),NN=1,NR)
         IF (REGNAM.EQ.CHKSTR) GOTO 888 
  200 CONTINUE
C
      WRITE(6,6010)
      CALL                                         XIT('REGNUM',-1)
C
  888 RETURN
C
C---------------------------------------------------------------------
C
  100 FORMAT(A4,I3,10I3) 
 6010 FORMAT(' ERROR. REGION NAME DOES NOT MATCH DEFINED REGIONS')
C
C---------------------------------------------------------------------
C
      END 
