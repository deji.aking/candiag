      BLOCK DATA FILLPAT_BD
C 
C SPECIFIES DEFAULT VALUES FOR THE INTERNAL PARAMETERS OF FILL
C 
      IMPLICIT NONE

      INTEGER I

      COMMON /FILLCO/LAN,LSP,LPA,LCH,LDP(8,8) 
      INTEGER LAN,LSP,LPA,LCH,LDP

C LAN IS THE ANGLE AT WHICH FILL LINES ARE TO BE DRAWN--AN INTEGER
C VALUE, IN DEGREES. RESET LAN WITH "CALL FILLOP('AN',IVP)".
      DATA LAN/0/ 

C LSP IS THE DISTANCE BETWEEN EACH FILL LINE AND THE NEXT--AN INTEGER 
C VALUE, IN METACODE UNITS, BETWEEN 0 AND 32767--THE VALUE 0 MEANS 48,
C WHICH GIVES ESSENTIALLY SOLID FILL (ON THE DICOMED, AT LEAST). RESET
C LSP WITH "CALL FILLOP('SP',IVP)". 
      DATA LSP/0/ 

C LPA IS THE PATTERN SELECTOR--THE VALUE 0 MEANS THAT SOLID LINES ARE 
C TO BE USED, THE VALUE 1 THAT DOTTED LINES ARE TO BE USED (THE DOTS TO 
C BE ARRANGED ACCORDING TO THE PATTERN SPECIFIED BY LDP). RESET LPA WITH
C "CALL FILLOP('PA',IVP)".
      DATA LPA/0/ 

C IF LCH IS 0, THE DOTS IN A DOTTED LINE WILL BE ACTUAL DOTS. IF ICH
C IS NON-ZERO, THE CHARACTER SPECIFIED BY ICH WILL BE USED INSTEAD OF 
C THE DOTS. SEE THE PLOT PACKAGE ROUTINE "POINTS".
      DATA LCH /0/

C LDP CONTAINS A SET OF 0S AND 1S DEFINING THE CURRENT DOT PATTERN, IN
C AN 8*8 PIXEL. THE VALUE 1 TURNS A DOT ON, THE VALUE 0 TURNS IT OFF. 
C THE CONTENTS OF LDP CAN BE RESET BY A "CALL FILLPA(IDP)". THE AREA
C OCCUPIED BY ONE MAPPING OF LDP ONTO A PLOT IS 8*LSP UNITS WIDE AND
C 8*LSP UNITS HIGH. EACH FILL LINE IS DRAWN USING DOTS POSITIONED AS
C SPECIFIED BY A PARTICULAR ROW OF LDP, APPLIED REPETITIVELY ALONG THE
C LINE. SUCCEEDING LINES USE THE FOLLOWING ROWS OF LDP, IN A CIRCULAR FASHION.
      DATA LDP/64*1/

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      COMMON /FILLPATTERNS/ FILPAT, FILTYP, FILANG, FILSPC
      INTEGER         FILPAT(0:420), FILTYP(0:420)
      REAL            FILANG(0:420), FILSPC(0:420)
C 
C     * SET THE PATTERNS AND THEIR TYPES FOR SOFTFILL 
C     * NOTE: IF YOU WANT TO MODIFY THE PATTERN/COLOUR-INDEX FOR A
C     *       CERTAIN VALUE, ALL YOU HAVE TO DO IS CHANGE THE VALUES
C     *       STORED IN FILPAT AND FILTYP.  FILTYP TELLS SOFTFILL 
C     *       WHAT TYPE OF PATTERN TO DRAW, AND FILPAT TELLS IT WHICH 
C     *       PATTERN.
C     * 
C     * COLOUR FILLS
C     * 
C     * PATTERN 100 -> 199 : 96 DIFFERENT COLOUR PALETTES DEFINED BY
C     *                      CALLING 'DFCLRS' 
C 
C     * LINE FILLS
C     * 
C     * PATTERN 200 -> 219 : PATTERNS 4 TO 23, FILL TYPE -4 
C     *                      HORIZONTAL, VERTICAL AND DIAGONAL LINES
C     *         220 -> 234 : PATTERNS 4 TO 18, FILL TYPE -3 
C     *                      HORIZONTAL AND DIAGONAL LINES
C     *         240 -> 249 : PATTERNS 4 TO 13, FILL TYPE -2 
C     *                      HORIZONTAL AND VERTICAL LINES
C     *         250 -> 255 : PATTERNS 2 TO  7, FILL TYPE -1, AN=  0 
C     *                      HORIZONTAL LINES 
C     *         260 -> 265 : PATTERNS 2 TO  7, FILL TYPE -1, AN= 45 
C     *                      DIAGONAL LINES (45 DEGREES)
C     *         270 -> 275 : PATTERNS 2 TO  7, FILL TYPE -1, AN= 90 
C     *                      VERTICAL LINES 
C     *         280 -> 285 : PATTERNS 2 TO  7, FILL TYPE -1, AN=135 
C     *                      DIAGONAL LINES (135 DEGREES) 
C 
C     * DOT FILLS 
C     * 
C     *         300 -> 305 : MARKER TYPE 1 - DOTS 
C     *         310 -> 315 : MARKER TYPE 2 - +
C     *         320 -> 325 : MARKER TYPE 3 - *
C     *         330 -> 335 : MARKER TYPE 4 - O
C     *         340 -> 345 : MARKER TYPE 5 - X
C     *         350 -> 355 : MARKER TYPE 6 - TRIANGLE 
C     *         360 -> 365 : MARKER TYPE 7 - BOX
C     * 
C 
C     * NO SHADING
      DATA  FILPAT(0), FILTYP(0), FILANG(0), FILSPC(0)
     1               / 0, -4,   0., 0.002500  /
C     * GREY SCALE (1-16) 
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=1, 16)
     1               / 1,  2,  0.5, 0.0025,    2,  2,  0.5, 0.0025, 
     3                 3,  2,  0.5, 0.0025,    4,  2,  0.5, 0.0025, 
     5                 5,  2,  0.5, 0.0025,    6,  2,  0.5, 0.0025, 
     7                 7,  2,  0.5, 0.0025,    8,  2,  0.5, 0.0025, 
     9                 9,  2,  0.5, 0.0025,   10,  2,  0.5, 0.0025, 
     1                11,  2,  0.5, 0.0025,   12,  2,  0.5, 0.0025, 
     3                13,  2,  0.5, 0.0025,   14,  2,  0.5, 0.0025, 
     5                15,  2,  0.5, 0.0025,   16,  2,  0.5, 0.0025  / 
C     * UNDEFINED (17-21) 
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=17, 21) 
     7               / 0, -4,   0., 0.002500,    0, -4,   0., 0.002500, 
     9                 0, -4,   0., 0.002500,    0, -4,   0., 0.002500, 
     1                 0, -4,   0., 0.002500 /
C     * LINES (45 DEG - 22-24)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=22, 24) 
     1               /                           2, -1,  47., 0.001000, 
     3                3, -1,  47., 0.001000,     4, -1,  47., 0.001000 /
C     * LINES (135 DEG -  25-27)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=25, 27) 
     5               / 2, -1, 137., 0.001000,    3, -1, 137., 0.001000, 
     7                 4, -1, 137., 0.001000 /
C     * LINES (45 DEG CROSS HATCHED - 28-30)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=28, 30) 
     7               /                           4, -2,  47., 0.001000, 
     9                 6, -2,  47., 0.001000,    8, -2,  47., 0.001000 /
C     * LINES (HORIZONTAL - 31-33)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=31, 33) 
     1               / 2, -1,   0., 0.001000,    3, -1,   0., 0.001000, 
     3                 4, -1,   0., 0.001000 /
C     * LINES (VERTICAL - 34-36)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=34, 36) 
     3               /                           2, -1,  90., 0.001000, 
     5                 3, -1,  90., 0.001000,    4, -1,  90., 0.001000 /
C     * LINES (HORIZONTAL CROSS HATCHED - 37-40)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=37, 40) 
     7               / 5, -2,  90., 0.001000,    5, -2,   0., 0.001000, 
     9                 6, -2,   0., 0.001000,    4, -2,   0., 0.001000 /
C     * LINES (HORIZONTAL -41)
      DATA  FILPAT(41), FILTYP(41),FILANG(41),FILSPC(41)
     1               / 3, -1,   0., 0.001000 /
C     * LINES (HORIZONTAL CROSS HATCHED -42)
      DATA  FILPAT(42), FILTYP(42),FILANG(42),FILSPC(42)
     1               /                           8, -2,   0., 0.001000 /
C     * UNDEFINED (43-47) 
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=43, 47) 
     3               / 0, -4,   0., 0.002500,    0, -4,   0., 0.002500, 
     5                 0, -4,   0., 0.002500,    0, -4,   0., 0.002500, 
     7                 0, -4,   0., 0.002500 /
C     * GREY SCALE (LIGHT-DARK - 48-64) 
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=48, 64) 
     7               /                         16,  2,  0.5, 0.0025 , 
     9                16,  2,  0.5, 0.00225,   16,  2,  0.5, 0.002  , 
     1                16,  2,  0.5, 0.00175,   16,  2,  0.5, 0.0015 , 
     3                13,  2,  0.5, 0.0025 ,   13,  2,  0.5, 0.00235, 
     5                13,  2,  0.5, 0.0022 ,   13,  2,  0.5, 0.00205, 
     7                13,  2,  0.5, 0.0019 ,    9,  2,  0.5, 0.0025 , 
     9                 9,  2,  0.5, 0.0023 ,    9,  2,  0.5, 0.00205, 
     1                 9,  2,  0.5, 0.00185,    9,  2,  0.5, 0.0017, 
     3                 9,  2,  0.5, 0.0015 ,    9,  2,  0.5, 0.00125 /

C     * HATCHING (65-78)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=65, 80) 
     1               / 1, -1,  45., 0.00075,  1, -1,  45., 0.00070,
     2                 1, -1,  45., 0.00065,  1, -1,  45., 0.00060,
     3                 1, -1,  45., 0.00055,  1, -1,  45., 0.00050,
     4                 1, -1,  45., 0.00045,  1, -1,  45., 0.00040,
     5                 1, -1,  45., 0.00035,  1, -1,  45., 0.00030,
     6                 1, -1,  45., 0.00025,  1, -1,  45., 0.00020,
     7                 1, -1,  45., 0.000155, 1, -1,  45., 0.00015,
     8                 1, -1,  45., 0.000105, 1, -1,  45., 0.00010 /

C     * HATCHING (79-99)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=81, 99) 
     1               / 1, -1, 135., 0.00075,  1, -1, 135., 0.00070,
     2                 1, -1, 135., 0.00065,  1, -1, 135., 0.00060,
     3                 1, -1, 135., 0.00055,  1, -1, 135., 0.00050,
     4                 1, -1, 135., 0.00045,  1, -1, 135., 0.00040,
     5                 1, -1, 135., 0.00035,  1, -1, 135., 0.00030,
     6                 1, -1, 135., 0.00025,  1, -1, 135., 0.00020,
     7                 1, -1, 135., 0.000155, 1, -1, 135., 0.00015,
     8                 1, -1, 135., 0.000105, 1, -1, 135., 0.00010,
     9                 0, -4, 135., 0.0011,   0, -4, 135., 0.0010,
     A                 0, -4, 135., 0.0009 /

C     * UNDEFINED (65-99) 
C     DATA (FILPAT(I), I=65, 99)
C    +              / 35* 0 / 
C     DATA (FILTYP(I), I=65, 99)
C    +              / 35*-4 / 
C     DATA (FILANG(I), I=65, 99)
C    +              / 35* 0./ 
C     DATA (FILSPC(I), I=65, 99)
C    +              / 35*0.0025 / 
C 
C     * COLOUR FILLS
      DATA (FILPAT(I), I=100, 199)
     1              /  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 
     2                12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 
     3                22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 
     4                32, 33, 34, 35, 36, 37,  0,  0,  0,  0, 
     5                42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 
     6                52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 
     7                62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
     8                72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 
     9                82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 
     A                92, 93, 94, 95, 96, 97, 98, 99, 100, 101 /
C
C     * COLOUR FILLS
      DATA (FILPAT(I), I=350, 419)
     1              /102,103,104,105,106,107,108,109,110,111,
     2               112,113,114,115,116,117,118,119,120,121,
     3               122,123,124,125,126,127,128,129,130,131,
     4               132,133,134,135,136,137,138,139,140,141,
     5               142,143,144,145,146,147,148,149,150,151,
     6               152,153,154,155,156,157,158,159,160,161,
     7               162,163,164,165,166,167,168,169,170,171/
  
      DATA (FILTYP(I), I=100, 199)
     1              / 100* 0 /
      DATA (FILANG(I), I=100, 199)
     1              / 100* 0./
      DATA (FILSPC(I), I=100, 199)
     1              / 100*0.0025 /
      DATA (FILTYP(I), I=350, 419)
     1              / 70* 0 /
      DATA (FILANG(I), I=350, 419)
     1              / 70* 0./
      DATA (FILSPC(I), I=350, 419)
     1              / 70*0.0025 /
  
C     * LINE FILLS (TYPE -4)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=200, 219) 
     1               / 4, -4,   0., 0.0025,      5, -4,   0., 0.0025, 
     2                 6, -4,   0., 0.0025,      7, -4,   0., 0.0025, 
     3                 8, -4,   0., 0.0025,      9, -4,   0., 0.0025, 
     4                10, -4,   0., 0.0025,     11, -4,   0., 0.0025, 
     5                12, -4,   0., 0.0025,     13, -4,   0., 0.0025, 
     6                14, -4,   0., 0.0025,     15, -4,   0., 0.0025, 
     7                16, -4,   0., 0.0025,     17, -4,   0., 0.0025, 
     8                18, -4,   0., 0.0025,     19, -4,   0., 0.0025, 
     9                20, -4,   0., 0.0025,     21, -4,   0., 0.0025, 
     A                22, -4,   0., 0.0025,     23, -4,   0., 0.0025 /
  
C     * LINE FILLS (TYPE -3)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=220, 239) 
     1               / 4, -3,   0., 0.0025,      5, -3,   0., 0.0025, 
     2                 6, -3,   0., 0.0025,      7, -3,   0., 0.0025, 
     3                 8, -3,   0., 0.0025,      9, -3,   0., 0.0025, 
     4                10, -3,   0., 0.0025,     11, -3,   0., 0.0025, 
     5                12, -3,   0., 0.0025,     13, -3,   0., 0.0025, 
     6                14, -3,   0., 0.0025,     15, -3,   0., 0.0025, 
     7                16, -3,   0., 0.0025,     17, -3,   0., 0.0025, 
     8                18, -3,   0., 0.0025,      0, -4,   0., 0.0025, 
     9                 0, -4,   0., 0.0025,      0, -4,   0., 0.0025, 
     A                 0, -4,   0., 0.0025,      0, -4,   0., 0.0025 /
  
C     * LINE FILLS (TYPE -2)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=240, 249) 
     1               / 4, -2,   0., 0.0025,      5, -2,   0., 0.0025, 
     2                 6, -2,   0., 0.0025,      7, -2,   0., 0.0025, 
     3                 8, -2,   0., 0.0025,      9, -2,   0., 0.0025, 
     4                10, -2,   0., 0.0025,     11, -2,   0., 0.0025, 
     5                12, -2,   0., 0.0025,     13, -2,   0., 0.0025 /
  
C     * LINE FILLS (TYPE -1)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=250, 269) 
     1               / 2, -1,   0., 0.0025,      3, -1,   0., 0.0025, 
     2                 4, -1,   0., 0.0025,      5, -1,   0., 0.0025, 
     3                 6, -1,   0., 0.0025,      7, -1,   0., 0.0025, 
     4                17,  1,   0., 0.0018,     17,  1,   0., 0.0017, 
     5                17,  1,   0., 0.0016,     17,  1,   0., 0.0015, 
     6                 2, -1,  45., 0.0025,      3, -1,  45., 0.0025, 
     7                 4, -1,  45., 0.0025,      5, -1,  45., 0.0025, 
     8                 6, -1,  45., 0.0025,      7, -1,  45., 0.0025, 
     9                 18, 1,   0., 0.0014,      18, 1,   0., 0.0013, 
     A                 18, 1,   0., 0.0012,      18, 1,   0., 0.0011 /
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=270, 289) 
     1               / 2, -1,  90., 0.0025,      3, -1,  90., 0.0025,
     2                 4, -1,  90., 0.0025,      5, -1,  90., 0.0025, 
     3                 6, -1,  90., 0.0025,      7, -1,  90., 0.0025, 
     4                 17, 1,   0., 0.0010,      17, 1,   0., 0.0009, 
     5                 17, 1,   0., 0.0008,      17, 1,   0., 0.0007, 
     6                 2, -1, 135., 0.0025,      3, -1, 135., 0.0025, 
     7                 4, -1, 135., 0.0025,      5, -1, 135., 0.0025, 
     8                 6, -1, 135., 0.0025,      7, -1, 135., 0.0025, 
     9                17,  1,   0., 0.0006,     17,  1,   0., 0.00055, 
     A                17,  1,   0., 0.0005,     17,  1,   0., 0.00045 /
  
C     * LINE FILLS (DOTS)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=290, 299) 
     1               / 17, 1,   0., 0.0025,     16,  1,   0., 0.0025, 
     2                 15, 1,   0., 0.0025,     13,  1,   0., 0.0025, 
     3                 9,  1,   0., 0.0025,      1,  1,   0., 0.0025, 
     4                 17, 1,   0., 0.0004,     17,  1,   0., 0.00035,
     5                 17, 1,   0., 0.0003,     17,  1,   0., 0.00025 /
  
C     * DOT FILLS (DOTS)
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=300, 309) 
     1               / 17,  1,   0., 0.0025,    16,  1,   0., 0.0025, 
     2                 15,  1,   0., 0.0025,    13,  1,   0., 0.0025, 
     3                  9,  1,   0., 0.0025,     1,  1,   0., 0.0025, 
     4                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025, 
     5                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025 /
  
C     * DOT FILLS (MARKER TYPE 2) 
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=310, 319) 
     1               / 17,  2,   0., 0.0025,    16,  2,   0., 0.0025, 
     2                 15,  2,   0., 0.0025,    13,  2,   0., 0.0025, 
     3                  9,  2,   0., 0.0025,     1,  2,   0., 0.0025, 
     4                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025, 
     5                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025 /
  
C     * DOT FILLS (MARKER TYPE 3) 
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=320, 329) 
     1               / 17,  3,   0., 0.0025,    16,  3,   0., 0.0025, 
     2                 15,  3,   0., 0.0025,    13,  3,   0., 0.0025, 
     3                  9,  3,   0., 0.0025,     1,  3,   0., 0.0025, 
     4                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025, 
     5                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025 /
  
C     * DOT FILLS (MARKER TYPE 4) 
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=330, 339) 
     1               / 17,  4,   0., 0.0025,    16,  4,   0., 0.0025, 
     2                 15,  4,   0., 0.0025,    13,  4,   0., 0.0025, 
     3                  9,  4,   0., 0.0025,     1,  4,   0., 0.0025, 
     4                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025, 
     5                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025 /
  
C     * DOT FILLS (MARKER TYPE 5) 
      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=340, 349) 
     1               / 17,  5,   0., 0.0025,    16,  5,   0., 0.0025, 
     2                 15,  5,   0., 0.0025,    13,  5,   0., 0.0025, 
     3                  9,  5,   0., 0.0025,     1,  5,   0., 0.0025, 
     4                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025, 
     5                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025 /
  
C     * DOT FILLS (MARKER TYPE 6) 
C     DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=350, 359) 
C    1               / 17,  6,   0., 0.0025,    16,  6,   0., 0.0025, 
C    2                 15,  6,   0., 0.0025,    13,  6,   0., 0.0025, 
C    3                  9,  6,   0., 0.0025,     1,  6,   0., 0.0025, 
C    4                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025, 
C    5                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025 /
C     * DOT FILLS (MARKER TYPE 6) 
C      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=350, 359) 
C     1               / 18,  6,   0., 0.0025,    19,  6,   0., 0.0035, 
C     2                 20,  6,   0., 0.0045,    21,  6,   0., 0.0055, 
C     3                 22,  6,   0., 0.0065,    23,  6,   0., 0.0075, 
C     4                 24,  6,   0., 0.0085,    25,  6,   0., 0.0095, 
C     5                 26,  6,   0., 0.0105,    27,  6,   0., 0.0195 /
C  
C     * DOT FILLS (MARKER TYPE 7) 
C     DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=360, 369) 
C    1               / 17,  7,   0., 0.0025,    16,  7,   0., 0.0025, 
C    2                 15,  7,   0., 0.0025,    13,  7,   0., 0.0025, 
C    3                  9,  7,   0., 0.0025,     1,  7,   0., 0.0025, 
C    4                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025, 
C    5                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025 /
C     * DOT FILLS (MARKER TYPE 7) 
C      DATA (FILPAT(I), FILTYP(I), FILANG(I), FILSPC(I), I=360, 369) 
C     1               / 28,  7,   0., 0.0025,    29,  7,   0., 0.0025, 
C     2                 30,  7,   0., 0.0025,    31,  7,   0., 0.0025, 
C     3                 32,  7,   0., 0.0025,    33,  7,   0., 0.0025, 
C     4                 34,  7,   0., 0.0025,     0, -4,   0., 0.0025, 
C     5                  0, -4,   0., 0.0025,     0, -4,   0., 0.0025 /
C 
C     * DOT FILLS (UNDEFINED) 
C     DATA (FILPAT(I), I=370, 400)
C    1              / 31* 0 / 
C     DATA (FILTYP(I), I=370, 400)
C    1              / 31*-4 / 
C     DATA (FILANG(I), I=370, 400)
C    1              / 31* 0./ 
C     DATA (FILSPC(I), I=370, 400)
C    1              / 31*0.0025 / 

      END
