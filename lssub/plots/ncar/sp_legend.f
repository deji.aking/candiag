      SUBROUTINE SP_LEGEND(CLRPLT)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C Colour LEGEND-plotting routine.

      LOGICAL CLRPLT

      REAL XMAPMN,XMAPMX,YMAPMN,YMAPMX,DUM1,DUM2,DUM3,DUM4
      INTEGER IDUM
      REAL XBARMN, XBARMX, YBARMN, YBARMX, BOXWDT
      INTEGER I
      REAL CLV
      INTEGER NLBLS,NK,NNK,NCLL
      PARAMETER(NLBLS = 35)
      CHARACTER*10 LBLS(NLBLS),TLBL
C For passing colour index array into colour FILL routines through
C Common Block.
      INTEGER NIPAT, NPAT, MAXPAT
      PARAMETER(NIPAT = 28)
      INTEGER IPAT(NIPAT)
      REAL ZLEV(NIPAT)
      COMMON /FILLCB/ IPAT, ZLEV, NPAT, MAXPAT, MAXCLRS
      EXTERNAL LBFILL

      INTEGER LFIN(NIPAT)
      REAL CHSIZE
      INTEGER LSTRBEG,LSTREND
      EXTERNAL LSTRBEG,LSTREND

      NCLL = NPAT - 1
C Duplex font.
      CALL LBSETR('WLB',2.)

C Finish defining the label bar parameters.
      CALL GETSET(XMAPMN, XMAPMX, YMAPMN, YMAPMX,
     +            DUM1,DUM2,DUM3,DUM4,IDUM)
      XBARMX = XMAPMX + 0.08
      XBARMN = XMAPMX - 0.04
      YBARMN = YMAPMN + 0.06
      YBARMX = YMAPMN + 0.50
      BOXWDT = (YBARMX - YBARMN)/MAX(NPAT,7)
C
C
C Define the contour level numeric labels.
C
C      CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', ITMP)
      DO 10 I = 1, NCLL
        CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
        CALL CPGETR('CLV - CONTOUR LEVEL VALUES', CLV)
        WRITE (LBLS(I), '(F10.3)') CLV
        TLBL = LBLS(I)
        LBLS(I)=TLBL(LSTRBEG(LBLS(I)):LSTREND(LBLS(I)))
CDEBUG
C!!!        WRITE(*,90) I,LBLS(I)
C!!! 90     FORMAT('LGND: CLV(',I3,'): "',A,'"')
CDEBUG
 10   CONTINUE

      IF (CLRPLT) THEN
        DO 20 I = 1, NPAT
          LFIN(I) = IPAT(I) - 98
 20     CONTINUE
C Draw and fill a label bar.
        CALL LBSETR('WLB',3.)
        CALL LBLBAR(1, XBARMN, XBARMX,
     +            YBARMN, YBARMN + NPAT * BOXWDT, NCLL+1,
     +            .45, .5, LFIN, 0,
     +            LBLS, NCLL, 2)
      ELSE
        DO 30 I = 1, NPAT
          CALL DFNCLR(IPAT(I), LFIN(I))
 30     CONTINUE
C Draw and fill a label bar.
        CALL LBSETR('WLB',3.)
        CALL LBLBAR(1, XBARMN, XBARMX,
     +            YBARMN, YBARMN + NPAT * BOXWDT, NCLL+1,
     +            .45, .5, LFIN, 2,
     +            LBLS, NCLL, 2)
      ENDIF
C
      RETURN
      END
