      SUBROUTINE ARDBD3 (X1,Y1,X2,Y2) 
 
C     * AUG 23/07 - B.MIVILLE
C
C     * The routine ARDBD3 is to draw an dash-line arrow from the 
C     * point (X1,Y1) to the point (X2,Y2), in the fractional 
C     * coordinate system. In order to prevent too many arrowheads 
C     * from appearing, we keep track  of the cumulative distance 
C     * along edges being drawn (in DT).
C
      COMMON /ARCOM1/ DT
C
C     * Define some temporary X and Y coordinate arrays, to be used 
C     * in marking the head and tail of the arrow.
C
      DIMENSION TX(8),TY(8)
C-----------------------------------------------------------------------------

C
      CALL GSCR(1,0,0.,0.,0.)
C
C     * Compute the length of the arrow.  If it's zero, quit.
C
      DX=X2-X1
      DY=Y2-Y1
      DP=SQRT(DX*DX+DY*DY)
C 
      IF (DP.EQ.0.) RETURN 
C
C     * Draw the body of the arrow.
C
      CALL GSLN(2)
      CALL DASHDB(0)
      CALL LINE(X1,Y1,X2,Y2)
      CALL GSLN(1)
C
C     * If the cumulative length of the edge being drawn is too little,
C     * quit ...
C
      DT=DT+DP
      IF(DT.LE..008) RETURN
C
C     * otherwise, zero the cumulative length ...
C
      DT=0.
C
C     * and draw an solid-filled arrowhead.
C
      B=(DP-.02)/DP
      A=1.0-B
      XT=A*X1+B*X2
      YT=A*Y1+B*Y2
C
      TX(1)=X2
      TY(1)=Y2
      TX(2)=XT-.01*DY/DP
      TY(2)=YT+.01*DX/DP
      TX(3)=A*X1+B*X2
      TY(3)=A*Y1+B*Y2
      TX(4)=XT+.01*DY/DP
      TY(4)=YT-.01*DX/DP
C     
      CALL GSFACI (1)
      CALL GFA    (4,TX,TY)
C
C     * Done.
C
      RETURN
C     
      END
