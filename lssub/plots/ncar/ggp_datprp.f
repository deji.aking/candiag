      SUBROUTINE GGP_DATPRP(LX,LY,LXY,U,GG2,FLIP,FLO,HI,FINC, 
     1           SCAL,SPVAL) 

C     * SEP 18/2006 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C This routine prepares data for ggplot.
      IMPLICIT NONE

      INTEGER LX,LY,LXY
      REAL U(*),GG2(*)
      LOGICAL FLIP
      REAL FLO,HI,FINC,SCAL,SPVAL,SPVALT

      INTEGER ILGH,I,J,JR,NL,NR
C!!!      PRINT*, "DATPRP"

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL
C
C     * EXCHANGE LEFT AND RIGHT HALVES OF THE GLOBAL C-E FIELD
C     * TO SHIFT LON 0 TO THE CENTRE OF THE PLOT.
C
      IF (FLIP) THEN
         IF ( INT(LX/2.)*2 .EQ. LX ) THEN
            ILGH = LX/2
            DO 125 J=1,LY
               JR = (J-1)*LX
               DO 120 I=1,ILGH
                  NL = JR+I
                  NR = NL+ILGH
                  IF(ABS(GG2(NR)-SPVAL).GT.SPVALT) THEN
                   U(NL) = GG2(NR)
                  ELSE
                   U(NL) = SPVAL
                  ENDIF
                  IF(ABS(GG2(NL)-SPVAL).GT.SPVALT) THEN
                   U(NR) = GG2(NL)
                  ELSE
                   U(NR) = SPVAL
                  ENDIF
 120           CONTINUE
 125        CONTINUE
         ELSE
            ILGH = (LX-1)/2
            DO 135 J=1,LY
               JR = (J-1)*LX
               DO 130 I=1,ILGH
                  NL = JR+I
                  NR = NL+ILGH
                  IF(ABS(GG2(NR)-SPVAL).GT.SPVALT) THEN
                   U(NL) = GG2(NR)
                  ELSE
                   U(NL) = SPVAL
                  ENDIF
                  IF(ABS(GG2(NL)-SPVAL).GT.SPVALT) THEN
                   U(NR) = GG2(NL)
                  ELSE
                   U(NR) = SPVAL
                  ENDIF
 130           CONTINUE
               U(JR+LX) = U(JR+1)
 135        CONTINUE
         ENDIF
      ENDIF

      IF (SCAL.EQ.0.) CALL PRECON3(FLO,HI,FINC,SCAL,U,LX,LY,10,SPVAL)
      WRITE(6,6020) FLO,HI,FINC,SCAL

C
C       * SCALE THE FIELD
C
      DO 140 I=1,LXY
         IF(ABS(U(I)-SPVAL).GT.SPVALT) THEN
            U(I)=U(I)*SCAL
         ENDIF
 140  CONTINUE

 6020 FORMAT('0 FLO= ',G12.4,', HI= ',G12.4,', FINC= ',G12.4,
     1                                      ', SCAL= ',G12.4)
      RETURN
      END
