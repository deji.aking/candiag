#deck zxtst
jobname=zxtst ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#                   zxtst               m lazare. dec 2/85 - ml.
#   ----------------------------------- computes the standard t-test for
#                                       significance of the deviation of a
#                                       statistic from its pooled control
#                                       state  and plots out the result.
#                                       the percent area of significance on the
#                                       t-test plots are also calculated.
#                                       this test is done for zonally averaged
#                                       statistics.
#                                       model1 and letter x refer to pooled
#                                       control run  while model2 and letter y
#                                       refer to the experimental run.
#
    access filex ${model1}xp
    access filey ${model2}xp
    libncar plunit=$plunit 
.   plotid.cdk
#   ----------------------------------- make initial null standard deviation file
    xfind filey del 
    xlin del null 
#   ----------------------------------- create a one-level real file containing
#                                       the vertical integral of pressure.
    xlin null g 
    vpint g ip 
    rm g xlin
#   ----------------------------------- find appropriate critical value of t.
    critt tc 
    xlin null tcrit input=tc 
    rm xlin
    xlin null nx 
    xlin null ny 
    xlin nx nx1 
    xlin ny ny1 
    add nx ny b1 
    mlt nx ny b2 
    div b1 b2 b3 
    sqroot b3 fact 
    add nx1 ny1 dof 
    rm tc nx ny b1 b2 b3
#   ----------------------------------- zonally averaged zonal velocity.
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged meridional velocity
#                                       and meridional streamfunction.
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    mlt num del dv 
    zxpsi dv dpsi 
    rm del dv
    zxplot dpsi 
    rm x y num tratio dpsi
#   ----------------------------------- zonally averaged temperature.
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged geopotential.
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged omega.
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged specific humidity.
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged u*u*.
.   ttstxp.cdk
    rm num
    sqroot y ya 
    sqroot x xa 
    sub ya xa num 
    rm xa ya
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged u"u".
.   ttstxp.cdk
    rm num
    sqroot y ya 
    sqroot x xa 
    sub ya xa num 
    rm xa ya
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged v*v*.
.   ttstxp.cdk
    rm num
    sqroot y ya 
    sqroot x xa 
    sub ya xa num 
    rm xa ya
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged v"v".
.   ttstxp.cdk
    rm num
    sqroot y ya 
    sqroot x xa 
    sub ya xa num 
    rm xa ya
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged t*t*.
.   ttstxp.cdk
    rm num
    sqroot y ya 
    sqroot x xa 
    sub ya xa num 
    rm xa ya
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged t"t".
.   ttstxp.cdk
    rm num
    sqroot y ya 
    sqroot x xa 
    sub ya xa num 
    rm xa ya
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged z*z*.
.   ttstxp.cdk
    rm num
    sqroot y ya 
    sqroot x xa 
    sub ya xa num 
    rm xa ya
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged z"z".
.   ttstxp.cdk
    rm num
    sqroot y ya 
    sqroot x xa 
    sub ya xa num 
    rm xa ya
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged v"t".
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged v*t*.
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged u"v".
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
#   ----------------------------------- zonally averaged u*v*.
.   ttstxp.cdk
    zxplot num 
    zxplot tratio 
    rm x y num tratio
    rm nx1 ny1 dof fact tcrit filex filey
    if [ "$splot" = on ] ; then
      zxplot stotal 
      rm stotal
    fi
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         MODEL1    = $model1 
         MODEL2    = $model2 
0 ZXTST  ------------------------------------------------------ ZXTST 
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        ($d) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0      0.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0   9.80616
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRITT.       $alpha     $xyr     $yyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0     $xyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0     $yyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     SUR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E0    -40.E0     40.E0      1.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (U)R. UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (U)R.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (V)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((V)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (V)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((V)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     SVR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E1    -40.E0     40.E0      1.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (V)R. UNITS .1*M/SEC. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (V)R.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp    1.E-10    -2.0E1     2.0E1      0.25$kax$kin
RUN $run.  DAYS $days.  (EXPMT-CONTROL) FOR PSI.  UNITS 1.E10*KG/SEC. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (T)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (T)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     STR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E0    -40.E0     40.E0      1.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (T)R. UNITS DEGK. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (T)R.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (GZ)R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((GZ)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (GZ)R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((GZ)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SGZR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp   1.02E-2   -200.E0    200.E0      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (GZ)R. UNITS DECAMETRES.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (GZ)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (W)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((W)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (W)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((W)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     SWR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E3    -40.E0     40.E0      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (W)R. UNITS .001*PA/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (W)R.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (Q)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((Q)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (Q)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((Q)R) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     SQR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E4    -40.E0     40.E0      1.E0    0$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (Q)R. UNITS .1*G/KG.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0    0$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (Q)R.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (U*U*)R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U*U*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (U*U*)R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (U*U*)R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U*U*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SU*UR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUB    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((U*U*)R)**0.5    UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (U*U*)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (U"U")R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U"U")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (U"U")R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (U"U")R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U"U")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SU"UR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUB    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((U+U+)R)**0.5    UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (U+U+)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (V*V*)R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((V*V*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (V*V*)R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (V*V*)R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((V*V*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SV*VR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUB    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((V*V*)R)**0.5    UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (V*V*)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (V"V")R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((V"V")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (V"V")R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (V"V")R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((V"V")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SV"VR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUB    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((V+V+)R)**0.5    UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (V+V+)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (T*T*)R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T*T*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (T*T*)R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (T*T*)R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T*T*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   ST*TR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUB    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((T*T*)R)**0.5    UNITS DEGK. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (T*T*)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (T"T")R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T"T")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (T"T")R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (T"T")R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T"T")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   ST"TR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUB    0    0$lxp      1.E0     -1.E2      1.E2     5.E-1$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((T+T+)R)**0.5    UNITS DEGK. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (T+T+)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (GZ*GZ*)R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((GZ*GZ*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (GZ*GZ*)R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (GZ*GZ*)R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((GZ*GZ*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SZ*ZR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUB    0    0$lxp   1.02E-2     -1.E2      1.E2      1.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((Z*Z*)R)**0.5    UNITS DMS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (Z*Z*)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (GZ"GZ")R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((GZ"GZ")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (GZ"GZ")R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (GZ"GZ")R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((GZ"GZ")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SZ"ZR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUB    0    0$lxp   1.02E-2     -1.E2      1.E2      1.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR ((Z+Z+)R)**0.5    UNITS DMS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (Z+Z+)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (T"V")R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T"V")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (T"V")R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (T"V")R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T"V")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SV"TR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E0     -2.E2      2.E2      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (V+T+)R. UNITS M*DEGK/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (V+T+)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (T*V*)R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T*V*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (T*V*)R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (T*V*)R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((T*V*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SV*TR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E0     -2.E2      2.E2      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (V*T*)R. UNITS M*DEGK/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (V*T*)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (U"V")R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U"V")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (U"V")R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (U"V")R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U"V")R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SU"VR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E0     -2.E2      2.E2      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (U+V+)R. UNITS (M/SEC)2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (U+V+)R. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        A (U*V*)R 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U*V*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
if [ "$multiexp" != on ] ; then 
XFIND.        (U*V*)R 
fi
if [ "$multiexp" = on ] ; then 
XFIND.        A (U*V*)R 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S((U*V*)R)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SU*VR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXINT.        100.
    THE ABOVE VALUES ACTUALLY REPRESENT THE PERCENT AREA OF SIGNIFICANCE. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     NUM    0    0$lxp      1.E0     -2.E2      2.E2      2.E0$kax$kin
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR (U*V*)R. UNITS (M/SEC)2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    TRAT    0    0$lxp      1.E0      0.E0  1025.E-2      1.E0$kax$kin
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR (U*V*)R. 
if [ "$splot" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     SUR    0    0$lxp      1.E0      0.E0     10.E0     5.E-1$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (U)R. UNITS M/SEC. 
ZXPLOT.     SVR    0    0$lxp      1.E1      0.E0     10.E0     5.E-1$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (V)R. UNITS M/SEC. 
ZXPLOT.     STR    0    0$lxp      1.E0      0.E0     10.E0    25.E-2$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (T)R. UNITS DEGK.
ZXPLOT.    SGZR    0    0$lxp   1.02E-2      0.E0     20.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (GZ)R. UNITS DECAMETRES. 
ZXPLOT.     SWR    0    0$lxp      1.E3      0.E0      1.E1     5.E-1$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (W)R. UNITS .001*PA/SEC. 
ZXPLOT.     SQR    0    0$lxp      1.E4      0.E0      1.E1    25.E-2    0$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (Q)R. UNITS .1*G/KG. 
ZXPLOT.   SU*UR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (U*U*)R. UNITS M2/SEC2.
ZXPLOT.   SU"UR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (U+U+)R. UNITS M2/SEC2.
ZXPLOT.   SV*VR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (V*V*)R. UNITS M2/SEC2.
ZXPLOT.   SV"VR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (V+V+)R. UNITS M2/SEC2.
ZXPLOT.   ST*TR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (T*T*)R. UNITS DEGK2.
ZXPLOT.   ST"TR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (T+T+)R. UNITS DEGK2.
ZXPLOT.   SZ*ZR    0    0$lxp   1.04E-4      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (Z*Z*)R. UNITS DECAMETRES**2.
ZXPLOT.   SZ"ZR    0    0$lxp   1.04E-4      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (Z+Z+)R. UNITS DECAMETRES**2.
ZXPLOT.   SV"TR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (V+T+)R. UNITS M*DEGK/SEC. 
ZXPLOT.   SV*TR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (V*T*)R. UNITS M*DEGK/SEC. 
ZXPLOT.   SU"VR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (U+V+)R. UNITS (M/SEC)2. 
ZXPLOT.   SU*VR    0    0$lxp      1.E0      0.E0     50.E0      1.E0$kax$kin
RUN $run. DAYS $days. COMBINED ST. DEV. OF (U*V*)R. UNITS (M/SEC)2. 
fi
. tailfram.cdk

end_of_data

. endjcl.cdk


