#!/bin/sh
#
#                  cldmic              KvS - Oct 3/2006
#   ---------------------------------- Microphysical sources and sinks.
#
# -------------------------------------------------------------------------
#
.   ggfiles.cdk
#
#  ---------------------------------- select input fields.
#
echo "SELECT          $r1 $r2 $r3     -9001 1000       AGG  AUT  CND  DEP
SELECT      EVP  FRH  FRK  FRS MLTI MLTS RACL SACI SACL  SUB RAIN SNOW" | ccc select npakgg AGG AUT CND DEP  \
                  EVP FRH FRK FRS  \
                  MLTI MLTS RACL SACI \
                  SACL SUB RAIN SNOW
echo "SELECT          $r1 $r2 $r3         1    1      VAGG VAUT VCND VDEP
SELECT     VEVP VFRH VFRK VFRS VMLI VMLS VRCL VSCI VSCL VSUB" | ccc select npakgg VAGG VAUT VCND VDEP VEVP \
                  VFRH VFRK VFRS VMLI VMLS \
                  VRCL VSCI VSCL VSUB
#
#  ----------------------------------- compute and save statistics.
#
    statsav  AGG  AUT  CND  DEP  EVP  FRH  FRK  FRS MLTI MLTS RACL \
            SACI SACL  SUB SNOW RAIN \
            new_gp new_xp $stat2nd
    statsav VAGG VAUT VCND VDEP VEVP VFRH VFRK VFRS \
            VMLI VMLS VRCL VSCI VSCL VSUB \
            new_gp new_xp $stat2nd
#
    rm       AGG  AUT  CND  DEP  EVP  FRH  FRK  FRS  MLTI MLTS RACL \
            SACI SACL  SUB VAGG VAUT VCND VDEP VEVP VFRH VFRK VFRS \
            VMLI VMLS VRCL VSCI VSCL VSUB RAIN SNOW

#  ----------------------------------- save results.
    release oldgp
    access  oldgp ${flabel}gp
    xjoin   oldgp new_gp newgp
    save    newgp ${flabel}gp
    delete  oldgp

    if [ "$rcm" != "on" ] ; then
    release oldxp
    access  oldxp ${flabel}xp
    xjoin   oldxp new_xp newxp
    save    newxp ${flabel}xp
    delete  oldxp
    fi
