#!/bin/sh
#                  cosp4_daily         jcl - Jun 01/15 - jcl ------- cosp4_daily
#  ----------------------------------- update the daily COSP diagnostic deck to 
#                                      match the output produced by the monthly 
#                                      COSP diagnostic deck (cosp4_monthly.dk).
#                                      Also added more comments to better 
#                                      documents the expected inputs and outputs.
# -------------------------------------------------------------------------

    year_offset="0"

#   ---------------------------------- check some variables

    if [ -z "$year_offset" ] ; then
      echo "ERROR in cosp4_daily.dk: undefined variable year_offset"
      exit 1
    fi
    if [ -z "$delt" ] ; then
      echo "ERROR in cosp4_daily.dk: undefined variable delt"
      exit 1
    fi
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`

    if [ -z "$daily_cmip5_tiers" ] ; then
#                                      set default daily tiers, if not defined
      daily_cmip5_tiers="1" # 2-D vars
    fi

#  ----------------------------------- prepare input cards 

    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL       YYYYMMDD" > ic.tstep_model
    echo "C*TSTEP   $deltmin     ${yearoff}010100       ACCMODEL       YYYYMMDD" > ic.tstep_accmodel
    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model_6h
    echo "C*BINSML      4         1" > ic.binsml

# ------------------------------------ access gs file

.   ggfiles.cdk  

# ----------------------------------------------------- Possible input fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# ICTP -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
# CCCP,CCCM -> Cloud fraction detected by lidar but not radar

# CL1C,CL1M -> High liquid cloud fraction
# CL2C,CL2M -> Middle liquid cloud fraction
# CL3C,CL3M -> Low liquid cloud fraction
# CL4C,CL4M -> Total liquid cloud fraction
# CI1C,CI1M -> High ice cloud fraction
# CI2C,CI2M -> Middle ice cloud fraction
# CI3C,CI3M -> Low ice cloud fraction
# CI4C,CI4M -> Total ice cloud fraction
# CU1C,CU1M -> High undefined cloud fraction
# CU2C,CU2M -> Middle undefined cloud fraction
# CU3C,CU3M -> Low undefined cloud fraction
# CU4C,CU4M -> Total undefined cloud fraction
# CLPP,CLPM -> Liquid cloud amount profile
# CIPP,CIPM -> Ice cloud amount profile 
# CUPP,CUPM -> Undefined cloud amount profile
# CT1P,CT1M -> Total cloud temperature profile
# CT2P,CT2M -> Liquid cloud temperature profile
# CT3P,CT3M -> Ice cloud temperature profile
# CT4P,CT4M -> Undefined cloud temperature profile

# SR*       -> Backscattering ratio/height histogram
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CloudSat related fields
#
# ZE*        -> Radar reflectivity/height histogram
# CCTC, CCTM -> Total cloud fraction using lidar and radar
# CHMK       -> Mask of land topography (needed when interpolating data to height grid)
# SMIX, SREF -> Snow mixing ratio and snow effective radius
# RMIX, RREF -> Rain mixing ratio and rain effective radius
#
# CERES-like fields
#
# CCCF -> Cloud fraction (liquid + ice)
# CCNT -> Cloud occurence (liquid + ice)
# CCTP -> Cloud top pressure 
# CCCT -> Cloud visible optical thickness (linear average)
# CCLT -> Cloud visible optical thickness (log average)
# CCFL -> Liquid cloud fraction
# CCFI -> Ice cloud fraction
# CLWP -> Cloud liquid water path in each pressure range
# CIWP -> Cloud ice water path in each pressure range
# CNLC -> Occurence of liquid cloud in each pressure range
# CNIC -> Occurence of ice cloud in each pressure range
# CREL -> Cloud top effective radius for liquid clouds
# CCDD -> Cloud droplet number concentrations for liquid clouds
# CCFM -> Liquid cloud fraction for microphysical parameters
# CNLC -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES-like cloud fields along orbit
#
# CS01 -> Cloud fraction (liquid + ice)
# CS02 -> Cloud occurence (liquid + ice)
# CS03 -> Cloud top pressure 
# CS04 -> Cloud visible optical thickness (linear average)
# CS05 -> Cloud visible optical thickness (log average)
# CS06 -> Liquid cloud fraction
# CS07 -> Ice cloud fraction
# CS08 -> Cloud liquid water path in each pressure range
# CS09 -> Cloud ice water path in each pressure range
# CS10 -> Occurence of liquid cloud in each pressure range
# CS11 -> Occurence of ice cloud in each pressure range
# CS12 -> Cloud top effective radius for liquid clouds
# CS13 -> Cloud droplet number concentrations for liquid clouds
# CS14 -> Liquid cloud fraction for microphysical parameters
# CS15 -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES radiative fluxes by cloud type along orbit
#
# ORBA -> Sum of occurrences of orbit in gridboxes (total orbit)
# ORBS -> Sum of occurrences of orbit in gridboxes (sunlit orbit)
# PWTC -> Precipitable water
# GTC  -> Surface temperature
# GCC  -> Surface type
# CSZC -> Cosine of solar zenith angle
# CDSW -> Upward solar flux at TOA for each cloud type
# CULW -> Upward thermal flux at TOA for each cloud type
# CFSO -> Downward solar flux at the TOA (for each cloud type for convience)
# CLRF -> Occurrence of clear sky
#
# MISR related fields
#
# MSTC -> Total cloud fraction from MISR
# MSMZ -> Cloud-mean cloud top height
# MSDT -> Occurence of model layers in particular MISR height ranges
# MSTZ -> Cloud top height versus optical thickness 
#
# MODIS related fields
#
# MDTC -> Total cloud fraction
# MDWC -> Water cloud fraction
# MDIC -> Ice cloud fraction
# MDHC -> High cloud fraction
# MDMC -> Middle cloud fraction
# MDLC -> Low cloud fraction
# MDTT -> Linear average cloud optical thickness (all clouds)
# MDWT -> Linear average cloud optical thickness (water clouds)
# MDIT -> Linear average cloud optical thickness (ice clouds)
# MDGT -> Log average cloud optical thickness (all clouds)
# MDGW -> Log average cloud optical thickness (water clouds)
# MDGI -> Log average cloud optical thickness (ice clouds)
# MDRE -> Liquid particle effective radius
# MDRI -> Ice particle effective radius
# MDPT -> Total cloud top pressure
# MDWP -> Liquid water path
# MDIP -> Ice water path
# MDTP -> Cloud top pressure versus cloud optical thickness histogram
# MLN1 -> Cloud droplet number coincentration from algorithm
# MLN2 -> Cloud droplet number coincentration from model
# MDLQ -> Warm (>273K) clouds fore which liquid cloud particle retrieved
#         by MODIS simulator
#
# ----------------------------------------------------- Possible output fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# ICTP -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
# CCCP,CCCM -> Cloud fraction detected by lidar but not radar
# SR*       -> Backscattering ratio/height histogram
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CloudSat related fields
#
# ZE*  -> Radar reflectivity/height histogram
# CHMK -> Mask of land topography (needed when interpolating data to height grid)
#
# CERES-like fields
#
# CCCF -> Cloud fraction (liquid + ice)
# CCNT -> Cloud occurence (liquid + ice)
# CCTP -> Cloud top pressure 
# CCCT -> Cloud visible optical thickness (linear average)
# CCLT -> Cloud visible optical thickness (log average)
# CCFL -> Liquid cloud fraction
# CCFI -> Ice cloud fraction
# CLWP -> Cloud liquid water path in each pressure range
# CIWP -> Cloud ice water path in each pressure range
# CNLC -> Occurence of liquid cloud in each pressure range
# CNIC -> Occurence of ice cloud in each pressure range
# CREL -> Cloud top effective radius for liquid clouds
# CCDD -> Cloud droplet number concentrations for liquid clouds
#
# CERES-like cloud fields along orbit
#
# CS01 -> Cloud fraction (liquid + ice)
# CS02 -> Cloud occurence (liquid + ice)
# CS03 -> Cloud top pressure 
# CS04 -> Cloud visible optical thickness (linear average)
# CS05 -> Cloud visible optical thickness (log average)
# CS06 -> Liquid cloud fraction
# CS07 -> Ice cloud fraction
# CS08 -> Cloud liquid water path in each pressure range
# CS09 -> Cloud ice water path in each pressure range
# CS10 -> Occurence of liquid cloud in each pressure range
# CS11 -> Occurence of ice cloud in each pressure range
# CS12 -> Cloud top effective radius for liquid clouds
# CS13 -> Cloud droplet number concentrations for liquid clouds
# CS14 -> Liquid cloud fraction for microphysical parameters
# CS15 -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES radiative fluxes by cloud type along orbit
#
# ORBA -> Sum of occurrences of orbit in gridboxes (total orbit)
# ORBS -> Sum of occurrences of orbit in gridboxes (sunlit orbit)
# PWTC -> Precipitable water
# GTC  -> Surface temperature
# GCC  -> Surface type
# CSZC -> Cosine of solar zenith angle
# CDSW -> Upward solar flux at TOA for each cloud type
# CULW -> Upward thermal flux at TOA for each cloud type
# CFSO -> Downward solar flux at the TOA (for each cloud type for convience)
# CLRF -> Occurrence of clear sky
#
# MISR related fields
#
# MSTC -> Total cloud fraction from MISR
# MSMZ -> Cloud-mean cloud top height
# MSDT -> Occurence of model layers in particular MISR height ranges
# MSTZ -> Cloud top height versus optical thickness 
#      -> This is saved as MST1 and MST2 to avoid problems with statsav
#
# MODIS related fields
#
# MDTC -> Total cloud fraction
# MDWC -> Water cloud fraction
# MDIC -> Ice cloud fraction
# MDHC -> High cloud fraction
# MDMC -> Middle cloud fraction
# MDLC -> Low cloud fraction
# MDTT -> Linear average cloud optical thickness (all clouds)
# MDWT -> Linear average cloud optical thickness (water clouds)
# MDIT -> Linear average cloud optical thickness (ice clouds)
# MDGT -> Log average cloud optical thickness (all clouds)
# MDGW -> Log average cloud optical thickness (water clouds)
# MDGI -> Log average cloud optical thickness (ice clouds)
# MDRE -> Liquid particle effective radius
# MDRI -> Ice particle effective radius
# MDPT -> Total cloud top pressure
# MDWP -> Liquid water path
# MDIP -> Ice water path
# MDTP -> Cloud top pressure versus cloud optical thickness histogram
# MLN1 -> Cloud droplet number coincentration from algorithm
# MLN2 -> Cloud droplet number coincentration from model
# MDLQ -> Warm (>273K) clouds fore which liquid cloud particle retrieved
#         by MODIS simulator
#
# ---------------------------------- Notes
# There are two rather important issues to take note of for this deck
#
# 1. It only processes variables that exist in the "gs" file.  Basically, the error
#    checking is turned off, we attempt to read in the variable.
#    Therefore if the variable is not present,
#    OR the read does not work properly, the script continues onward without the variable.
#    Therefore, if you expected a particular field in the "gp" file and it 
#    is not there check first if it is present in the "gs" file and the correct
#    options were set in the GCM submission job.
#
# 2. Most of the variables need to be weighted to get the proper monthly, seasonal, etc.
#    means.  Since the pooling deck have trouble with missing values the next step is
#    to average all of the weighted mean denominators and numerators.  Potentially adds
#    a lot of extra output to the diagnostic files but is necessary to get the proper 
#    means at averaging times greater than a month.
# 
# 3. When averaging the profile data, and when it is interpolated onto the special height grid, 
#    you will need to account for the points that wind up below the height of the land.
#
# Manipulations needed to get the proper weighted means using the fields from the diagnostic
# output
#
# ICCA = ICCA/ICCF (cloud fraction weighted)
# ICCT = ICCT/ICCF (cloud fraction weighted)
# ICCP = ICCP/ICCF (cloud fraction weighted)
# ICCF = ICCF/SUNL (weighed by "observation" occurrence, 
#                   i.e., solar zenith angle greater than 0.2)
# ICTP = ICTP/SUNL  (weighed by "observation" occurrence, 
#                   i.e., solar zenith angle greater than 0.2)
#
# CLHC = CLHC/CLHM (weighted by "valid observation", i.e., backscatter above threshold)
# CLMC = CLMC/CLMM (weighted by "valid observation", i.e., backscatter above threshold)
# CLLC = CLLC/CLLM (weighted by "valid observation", i.e., backscatter above threshold)
# CLTC = CLTC/CLTM (weighted by "valid observation", i.e., backscatter above threshold)
# CLCP = CLCP/CLCM (weighted by "valid observation", i.e., backscatter above threshold)
# CCCP = CCCP/CCCM (weighted by "valid observation", i.e., backscatter above threshold)
# CL1C = CL1C/CL1M (weighted by "valid observation", i.e., backscatter above threshold)
# CL2C = CL2C/CL2M (weighted by "valid observation", i.e., backscatter above threshold)
# CL3C = CL3C/CL3M (weighted by "valid observation", i.e., backscatter above threshold)
# CL4C = CL4C/CL4M (weighted by "valid observation", i.e., backscatter above threshold)
# CI1C = CI1C/CI1M (weighted by "valid observation", i.e., backscatter above threshold)
# CI2C = CI2C/CI2M (weighted by "valid observation", i.e., backscatter above threshold)
# CI3C = CI3C/CI3M (weighted by "valid observation", i.e., backscatter above threshold)
# CI4C = CI4C/CI4M (weighted by "valid observation", i.e., backscatter above threshold)
# CU1C = CU1C/CU1M (weighted by "valid observation", i.e., backscatter above threshold)
# CU2C = CU2C/CU2M (weighted by "valid observation", i.e., backscatter above threshold)
# CU3C = CU3C/CU3M (weighted by "valid observation", i.e., backscatter above threshold)
# CU4C = CU4C/CU4M (weighted by "valid observation", i.e., backscatter above threshold)
# CLPP = CLPP/CLPM (weighted by "valid observation", i.e., backscatter above threshold)
# CIPP = CIPP/CIPM (weighted by "valid observation", i.e., backscatter above threshold)
# CUPP = CUPP/CUPM (weighted by "valid observation", i.e., backscatter above threshold)
# CT1P = CT1P/CT1M (weighted by "valid observation", i.e., backscatter above threshold)
# CT2P = CT2P/CT2M (weighted by "valid observation", i.e., backscatter above threshold)
# CT3P = CT3P/CT3M (weighted by "valid observation", i.e., backscatter above threshold)
# CT4P = CT4P/CT4M (weighted by "valid observation", i.e., backscatter above threshold)
#
# PL01 = PL01/ML01 (weighted by valid surface type, i.e., not over land)
# PL02 = PL02/ML02 (weighted by valid surface type, i.e., not over land)
# PL03 = PL03/ML03 (weighted by valid surface type, i.e., not over land)
# PL04 = PL04/ML04 (weighted by valid surface type, i.e., not over land)
# PL05 = PL05/ML05 (weighted by valid surface type, i.e., not over land)
#
# The topography mask (CHMK) will be needed when averaging the profiles over regions, 
# i.e., zonally, when the profile has been interpolated onto the heights above mean
# sea level.
#
# For the CERES-like fields we assume that things are properly weighted, i.e., they use
# cloud fraction weighting, as needed.
#
# CCCF = CCCF/CCNT
# CCFL = CCFL/CNLC
# CCFI = CCFI/CNIC
# CCTP = CCTP/CCCF
# CCCT = CCCT/CCCF
# CCLT = CCLT/CCCF
# CLWP = CLWP/CCFL
# CIWP = CIWP/CCFI
# CREL = CREL/CCFM
# CCDD = CCDD/CCFM
# CCFM = CCFM/CNLM
#
# For MISR we need
#
# MSTC = MSTC/SUNL
# MSMZ = MSMZ/MSTC
# MSTZ = (Join up MST1 and MST2) and then MSTZ/SUNL
# MSDT = MSDT/SUNL
#
# For MODIS we need
#
# MDTC = MDTC/SUNL
# MDWC = MDWC/SUNL
# MDIC = MDIC/SUNL
# MDHC = MDHC/SUNL
# MDMC = MDMC/SUNL
# MDLC = MDLC/SUNL
# MDTT = MDTT/MDTC
# MDWT = MDWT/MDWC
# MDIT = MDIT/MDIC
# MDGT = MDGT/MDTC
# MDGW = MDGW/MDWC
# MDGI = MDGI/MDIC
# MDRE = MDRE/MDWC
# MDRI = MDRI/MDIC
# MDPT = MDPT/MDTC
# MDWP = MDWP/MDWC
# MDIP = MDIP/MDIC
# MDTP = MDTP/SUNL
# MLN1 = MLN1/MDLQ
# MLN2 = MLN2/MDLQ
# MDLQ = MDLQ/SUNL

# ------------------------------------ there are several daily data types:
#                                      tier 1 is for (sub)daily 2D variables extracted for all runs/periods
#                                      tier 2 is for (sub)daily 3D variables on pressure levels
#                                             extracted for some runs/periods
#                                      Also, if gssave=on, (sub)daily 3D variables on model levels are selected.
    for tier in $daily_cmip5_tiers ; do

      if [ $tier -eq 1 ] ; then

# ------------------------------------ TIER 1: 2D variables

        vars_all=''

# ----------------------------------- Select all of the ISCCP related fields at once

        vars="icca icct iccf iccp ictp sunl"
        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done


# ----------------------------------- Select CALIPSO and CloudSat cloud fraction fields at once
        vars="clhc clhm clmc clmm cllc cllm cltc cltm cctc cctm chmk"
        vars="${vars} cl1c cl2c cl3c cl4c cl1m cl2m cl3m cl4m"
        vars="${vars} ci1c ci2c ci3c ci4c ci1m ci2m ci3m ci4m"
        vars="${vars} cu1c cu2c cu3c cu4c cu1m cu2m cu3m cu4m"

        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done

# ----------------------------------- Reflectances for PARASOL
        vars="pl01 pl02 pl03 pl04 pl05 ml01 ml02 ml03 ml04 ml05"
        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done


# ----------------------------------- Select all of the CERES related fields at once
        vars="cccf ccnt cctp ccct cclt ccfl ccfi clwp ciwp"
        vars="$vars cnlc cnic crel ccdd ccfm"

        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done


# ----------------------------------- Select all of the MODIS related fields at once
# MODIS
        vars="mdtc mdwc mdic mdhc mdmc mdlc mdtt mdwt mdit mdgt mdgw"
        vars="$vars mdgi mdre mdri mdpt mdwp mdip mdtp mln1 mln2 mdlq"

        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done


# ----------------------------------- Select all of the MISR related fields at once
# MISR
        vars="mstc msmz msdt"
        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done

        set -e

# Special case for MSTZ since number of levels breaks the select code
        echo "C*SELECT   STEP         0 999999999    1     -9001   99 NAME MSTZ" | ccc select npakgg mst1 || true
        echo "C*SELECT   STEP         0 999999999    1       10099999 NAME MSTZ" | ccc select npakgg mst2 || true

        vars="mst1 mst2"
        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u}

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done

        set -e

# ------------------------------------ xsave
        if [ "${vars_all}" != '' ] ; then
          vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
          rm -f old
          while [ -n "$vars0" ] ; do
            vars1=`echo "$vars0" | cut -f1-80 -d' '`
            head -160 ic.xsave > ic.xsave1
            echo vars=$vars1
            cat ic.xsave1
            xsave old $vars1 new input=ic.xsave1
            mv new old
            vars0=`echo "$vars0" | cut -f81- -d' '`
            tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
          done
#   ---------------------------------- save results.
          access olddd ${flabel}dd na
          xjoin  olddd old newdd
          save   newdd ${flabel}dd na
          delete olddd na
          release old newdd
        fi
        
      fi # tier=1

      if [ $tier -eq 1 ] ; then

# ------------------------------------ TIER 1: 3D variables on COSP levels

        vars_all=''

# ------------------------------------ accumulated gs variables 
# CALIPSO/CLOUDSAT
        vars="clcp clcm cccp cccm"
        vars="$vars clpp cipp cupp clpm cipm cupm"
        vars="$vars ct1p ct2p ct3p ct4p ct1m ct2m ct3m ct4m"
        vars="$vars sr01 sr02 sr03 sr04 sr05 sr06 sr07 sr08 sr09 sr10 sr11 sr12 sr13 sr14 sr15"
        vars="$vars ze01 ze02 ze03 ze04 ze05 ze06 ze07 ze08 ze09 ze10 ze11 ze12 ze13 ze14 ze15"


        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done

# ------------------------------------ xsave
        if [ "${vars_all}" != '' ] ; then
          vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
          rm -f old
          while [ -n "$vars0" ] ; do
            vars1=`echo "$vars0" | cut -f1-80 -d' '`
            head -160 ic.xsave > ic.xsave1
            echo vars=$vars1
            cat ic.xsave1
            xsave old $vars1 new input=ic.xsave1
            mv new old
            vars0=`echo "$vars0" | cut -f81- -d' '`
            tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
          done
#   ---------------------------------- save results.

          save old ${flabel}dcosp
        fi 
      fi # tier=1

    done # daily_cmip5_tiers
