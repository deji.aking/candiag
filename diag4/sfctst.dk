#deck sfctst
jobname=sfct ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   remove condef parameter "pooled" and join gb file to gp file.
#                   sfctst              m lazare. jun 22/90 - ec.
#   ----------------------------------- computes the standard t-test for
#                                       significance of the deviation of a
#                                       statistic from its pooled control
#                                       state  and plots out the result.
#                                       the percent area of significance on the
#                                       t-test plots are also calculated.
#                                       this test is done for statistics at
#                                       the surface.
#                                       model1 and letter x refer to pooled
#                                       control run  while model2 and letter y
#                                       refer to the experimental run.
#
    access gpfile ${model1}gp
    access gbfile ${model1}gb na
    joinup filex gpfile gbfile 
    rm gpfile gbfile
# 
    access gpfile ${model2}gp
    access gbfile ${model2}gb na
    joinup filey gpfile gbfile 
    rm gpfile gbfile
# 
    libncar plunit=$plunit 
.   plotid.cdk
#   ----------------------------------- make initial null standard deviation file
    xfind filex alb 
    xlin alb null 
    rm alb xlin
#   ----------------------------------- find appropriate critical value of t.
    critt tc 
    xlin null tcrit input=tc 
    rm xlin
    xlin null nx 
    xlin null ny 
    xlin nx nx1 
    xlin ny ny1 
    add nx ny b1 
    mlt nx ny b2 
    div b1 b2 b3 
    sqroot b3 fact 
    add nx1 ny1 dof 
    rm tc nx ny b1 b2 b3
#   ----------------------------------- mean sea level pressure pmsl .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- precipitation rate pcp .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- surface moisture flux qfs .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- surface heat flux hfs .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
    if [ "$nobeg" != on ] ; then
#     ----------------------------------- surface energy balance beg .
.     ttstgp.cdk
      ggplot num 
      ggplot tratio 
      rm x y num tratio
    fi
#   ----------------------------------- zonal surface wind stress ufs .
.   ttstgp.cdk
    newnam num dufs 
    ggplot tratio 
    cp x conufs 
    cp y expufs 
    rm x y num tratio
#   ----------------------------------- meridional surface wind stress vfs .
.   ttstgp.cdk
    newnam num dvfs 
    cp x convfs 
    cp y expvfs 
    rm x y num
#   ----------------------------------- plot out the vector surface wind stress
#                                       difference and amplitude difference.
    square expufs x2 
    square expvfs y2 
    add x2 y2 amp2 
    sqroot amp2 expamp 
    rm expufs expvfs x2 y2 amp2
    square conufs x2 
    square convfs y2 
    add x2 y2 amp2 
    sqroot amp2 conamp 
    rm conufs convfs x2 y2 amp2
    sub expamp conamp num 
    rm expnum conamp
    ggplot num dufs dvfs 
    rm num dufs dvfs
    ggplot tratio 
    rm tratio
#   ----------------------------------- lowest model level zonal wind lu .
.   ttstgp.cdk
    newnam num du 
    square du du2 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- lowest model level meridional wind lv .
#                                       plot the vector wind difference.
.   ttstgp.cdk
    newnam num dv 
    square dv dv2 
    add du2 dv2 dav2 
    sqroot dav2 dav 
    newnam dav amp 
    rm dav2 du2 dv2 dav
    ggplot amp du dv 
    rm amp du dv
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- lowest model level temperature lt .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- lowest model level specific humidity lq .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
    rm nx1 ny1 dof fact tcrit filex filey
    if [ "$splot" = on ] ; then
      ggplot stotal 
      rm stotal
    fi
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         MODEL1    = $model1 
         MODEL2    = $model2 
0 SFCTST ---------------------------------------------------------------- SFCTST
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        SALB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0      0.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRITT.       $alpha     $xyr     $yyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0     $xyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0     $yyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(PMSL) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(PMSL) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SPMSL 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR PMSL. UNITS MBS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR PMSL.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(PCP)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(PCP)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SPCP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E5    -20.E0     20.E0    25.E-10${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR PCP. UNITS 1.E-5*KG/(M2*SEC). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR PCP. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        QFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(QFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        QFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(QFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SQFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E6    -40.E0     40.E0     10.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR QFS. UNITS 1.E-6*KG/(M2*SEC). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR QFS. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        HFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(HFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        HFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(HFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SHFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0   -100.E0    100.E0     25.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR HFS. UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR HFS. 
if [ "$nobeg" != on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BEG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(BEG)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BEG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(BEG)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SBEG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0   -100.E0    100.E0     25.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR BEG. UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR BEG. 
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        UFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(UFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        UFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(UFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SUFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    DUFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR UFS. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        VFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(VFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        VFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(VFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SVFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    DVFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E2      -80.       80.        4.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR SFC WIND STRESS. UNITS 1.E-2*N/M2.
                1.E2        0.       10.$ncx$ncy
                        VECPLOT OF DIFFERENCE IN (TAUX,TAUY). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR VFS. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LU
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LU) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LU
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LU) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     SLU 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     DLU 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   -1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR LOWEST MODEL LEVEL U.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LV) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LV) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     SLV 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     DLV 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     AMP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  AMP   -1$map        1.        0.       30.        2.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL WIND. UNITS M/SEC
                  1.        0.        5.$ncx$ncy
                        VECPLOT OF (U,V). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   -1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR LOWEST MODEL LEVEL V.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LT) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LT) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     SLT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT   -1$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL T. UNITS DEGK. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   -1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR LOWEST MODEL LEVEL T.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LQ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LQ) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LQ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LQ) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     SLQ 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT   -1$map      1.E3    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL Q. UNITS G/KG. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   -1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR LOWEST MODEL LEVEL Q.
if [ "$splot" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1SPMSL    1$map      1.E0      0.E0     10.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF PMSL. UNITS MBS. 
GGPLOT.           -1 SPCP    1$map      1.E5      0.E0     10.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF PCP. UNITS 1.E-5*KG/(M2*SEC).
GGPLOT.           -1 SQFS    1$map      1.E6      0.E0     20.E0    25.E-10${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF QFS. UNITS 1.E-6*KG/(M2*SEC).
GGPLOT.           -1 SHFS    1$map      1.E0      0.E0     50.E0      5.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF HFS. UNITS W/M2. 
if [ "$nobeg" != on ] ; then
GGPLOT.           -1 SBEG    1$map      1.E0      0.E0     50.E0      5.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF BEG. UNITS W/M2. 
fi
GGPLOT.           -1 SUFS    1$map      1.E2      0.E0     20.E0      2.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF UFS. UNITS 1.E-2*N/M2. 
GGPLOT.           -1 SVFS    1$map      1.E2      0.E0     20.E0      2.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF VFS. UNITS 1.E-2*N/M2. 
GGPLOT.           -1  SLU   -1$map      1.E0      0.E0     10.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF LOWEST MODEL LEVEL U. UNITS M/SEC. 
GGPLOT.           -1  SLV   -1$map      1.E0      0.E0     10.E0     5.E-10${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF LOWEST MODEL LEVEL V. UNITS M/SEC. 
GGPLOT.           -1  SLT   -1$map      1.E0      0.E0     10.E0     5.E-10${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF LOWEST MODEL LEVEL T. UNITS DEGK.
GGPLOT.           -1  SLQ   -1$map      1.E3      0.E0     10.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF LOWEST MODEL LEVEL Q. UNITS G/KG.
fi
. tailfram.cdk

end_of_data

. endjcl.cdk


