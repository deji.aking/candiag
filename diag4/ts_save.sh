#!/bin/sh
#                  ts_save             gjb,ml,sk - Jan 08/2006 - rdr.
#   ---------------------------------- accumulate time series in ${atmos_file}
#                                      Time step is constructed from ${year},
#                                      ${year_offset} and ${mon}.
#
#   sk: re-write for diagnostic files.
#  rdr: Add sea ice concentration.
#                                      * Upper atmosphere:
#
#                                      200 mb u, u'2, v, v'2
#                                      50  mb phi, phi'2
#                                      500 mb phi, phi'2
#                                      1000mb phi, phi'2
#                                      850 mb temp, temp'2
#
#                                      * Energy fluxes:
#
#                                      hfs:   surface sensible heat flux into atm..
#                                      beg:   net downward energy flux at surface
#                                     obeg:   net downward energy flux at ocean top
#
#                                      fsg:   net downward solar flux at surface
#                                      fso:   incident solar flux at TOA
#                                      fss:   incident solar flux at surface
#                                      fsa:   solar flux absorbed in atmosphere
#
#                                      flg:   net downward lw flux at surface
#                                      fdl:   incident lw flux at surface
#                                      fla:   lw flux absorbed in atmosphere
#
#                                     fstc:   net "cloud free" solar at TOA
#                                     fsgc:   net "cloud free" solar at surface
#                                     fltc:   net "cloud free" lw at TOA
#                                     flgc:   net "cloud free" lw at surface
#
#                                      * Moisture fluxes:
#
#                                      qfs:   evaporation rate
#                                      bwg:   net freshwater flux at surface
#                                     obwg:   net freshwater flux at ocean top
#                                      pcp:   mean pcp rate
#                                    pcp'2:   variance of pcp rate
#                                     pcpm:   monthly maximum pcp rate
#
#                                      * Stresses:
#
#                                  ufs,vfs:   surface stresses
#
#                                      * Surface state:
#
#                                       gc:   ground cover
#                                       gt:   ground temperature
#                                      sno:   snow
#                                      sic:   sea ice amount
#                                     sicn:   sea ice concentration
#                                     pmsl:   mean sea level pressure
#                                       ps:   surface pressure
#                                     wgfl:   soil water content (=wgf+wgl)
#                                             (3 layers)
#
#                                      * Screen level:
#
#                                       st:   temperature
#                                     st'2:   variance of screen temperature
#
#                                     stmx:   daily maximum temperature
#                                     stmn:   daily minimum temperature
#                                     stxx:   monthly maximum temperature
#                                     stnn:   monthly minimum temperature
#
#                                       sq:   specific humidity
#                                       su:   zonal wind
#                                       sv:   meridional wind
#                                     swmx:   mean wind-speed
#                                     swxx:   monthly maximum mean wind-speed
#
#                                      * Lowest model level (optional, if lmstat=on):
#
#                                       lu:   zonal wind
#                                       lv:   meridional wind
#                                      lva:   wind speed
#                                       lt:   temperature
#                                       lq:   humidity

#  ----------------------------------- access gp and xp file
      access gp ${flabel}gp
      access xp ${flabel}xp

#   ---------------------------------- select zonal avg. [T], [u], [v]
ccc   xfind xp xt xu xv << CARD
 XFIND        (T)R
 XFIND        (U)R
 XFIND        (V)R
CARD
      rm xp
#  ----------------------------------- save zonal avg.
ccc   xsave old_dat xt xu xv new_dat << CARD
 XSAVE        (T)R
 XSAVE
 XSAVE        (U)R
 XSAVE
 XSAVE        (V)R
 XSAVE
CARD
      mv new_dat old_dat

#  ----------------------------------- select upper atmosphere quantities
ccc   xfind gp u up2 v vp2 gz gzp2 t tp2 << CARD
 XFIND        U
 XFIND        U"U"
 XFIND        V
 XFIND        V"V"
 XFIND        GZ
 XFIND        GZ"GZ"
 XFIND        T
 XFIND        T"T"
CARD

#  ----------------------------------- 200 mb u, u'2, v, v'2
      for x in u v up2 vp2 ; do
        for l in 200 ; do
          LEV=`echo "    $l" | tail -5c`
ccc       select $x $x.$l << CARD
SELECT    STEPS $t1 $t2 $t3 LEVS $LEV $LEV NAME  ALL
CARD
        done
      done

#  ----------------------------------- phi, phi'2
      for x in gz gzp2 ; do
        for l in 50 500 1000 ; do
          LEV=`echo "    $l" | tail -5c`
ccc       select $x $x.$l << CARD
SELECT    STEPS $t1 $t2 $t3 LEVS $LEV $LEV NAME  ALL
CARD
        done
      done

#  ----------------------------------- temp, temp'2
      for x in t tp2 ; do
        for l in 850 ; do
          LEV=`echo "    $l" | tail -5c`
ccc       select $x $x.$l << CARD
SELECT    STEPS $t1 $t2 $t3 LEVS $LEV $LEV NAME  ALL
CARD
        done
      done

#  ----------------------------------- save upper atmosphere quantities
      xsave old_dat \
            u.200 up2.200 v.200 vp2.200 \
            gz.50 gzp2.50 gz.500 gzp2.500 gz.1000 gzp2.1000 \
            t.850 tp2.850 \
            new_dat << CARD
 XSAVE        U (200MB)
 XSAVE
 XSAVE        U"U" (200MB)
 XSAVE
 XSAVE        V (200MB)
 XSAVE
 XSAVE        V"V" (200MB)
 XSAVE
 XSAVE        GZ (50MB)
 XSAVE
 XSAVE        GZ"GZ" (50MB)
 XSAVE
 XSAVE        GZ (500MB)
 XSAVE
 XSAVE        GZ"GZ" (500MB)
 XSAVE
 XSAVE        GZ (1000MB)
 XSAVE
 XSAVE        GZ"GZ" (1000MB)
 XSAVE
 XSAVE        T (850MB)
 XSAVE
 XSAVE        T"T" (850MB)
 XSAVE
CARD
      mv new_dat old_dat

#  ----------------------------------- select sfc/TOA means
      xfind gp hfs beg obeg fsg fso fss fsa flg fdl fla \
               fstc fsgc fltc flgc \
               qfs bwg obwg pcp pcp2 pcpm ufs vfs \
               gc gt sno sic sicn pmsl ps wgf wgl \
               st st2 stmx stmn stxx stnn \
               sq su sv swmx swxx << CARD
 XFIND        HFS
 XFIND        BEG
 XFIND        OBEG
 XFIND        FSG
 XFIND        FSO
 XFIND        FSS
 XFIND        FSA
 XFIND        FLG
 XFIND        FDL
 XFIND        FLA
 XFIND        FSTC
 XFIND        FSGC
 XFIND        FLTC
 XFIND        FLGC
 XFIND        QFS
 XFIND        BWG
 XFIND        OBWG
 XFIND        PCP
 XFIND        PCP"2
 XFIND        TIME MAXIMUM PCP
 XFIND        UFS
 XFIND        VFS
 XFIND        GC
 XFIND        GT
 XFIND        SNO
 XFIND        SIC
 XFIND        SICN
 XFIND        PMSL
 XFIND        PS
 XFIND        WGF
 XFIND        WGL
 XFIND        ST
 XFIND        ST"2
 XFIND        STMX
 XFIND        STMN
 XFIND        TIME MAXIMUM STMX
 XFIND        TIME MINIMUM STMN
 XFIND        SQ
 XFIND        SU
 XFIND        SV
 XFIND        SWMX
 XFIND        TIME MAXIMUM SWMX
CARD

#  ----------------------------------- net soil moisture content
      add    wgf  wgl  wgfl

#  ----------------------------------- save sfc/TOA means
      xsave old_dat\
            hfs beg obeg fsg fso fss fsa flg fdl fla \
            fstc fsgc fltc flgc \
            qfs bwg obwg pcp pcp2 pcpm ufs vfs \
            gc gt sno sic sicn pmsl ps wgfl \
            st st2 stmx stmn stxx stnn \
            sq su sv swmx swxx \
            new_dat << CARD
 XSAVE        HFS
 XSAVE
 XSAVE        BEG
 XSAVE
 XSAVE        OBEG
 XSAVE
 XSAVE        FSG
 XSAVE
 XSAVE        FSO
 XSAVE
 XSAVE        FSS
 XSAVE
 XSAVE        FSA
 XSAVE
 XSAVE        FLG
 XSAVE
 XSAVE        FDL
 XSAVE
 XSAVE        FLA
 XSAVE
 XSAVE        FSTC
 XSAVE
 XSAVE        FSGC
 XSAVE
 XSAVE        FLTC
 XSAVE
 XSAVE        FLGC
 XSAVE
 XSAVE        QFS
 XSAVE
 XSAVE        BWG
 XSAVE
 XSAVE        OBWG
 XSAVE
 XSAVE        PCP
 XSAVE
 XSAVE        PCP"2
 XSAVE
 XSAVE        TIME MAXIMUM PCP
 XSAVE
 XSAVE        UFS
 XSAVE
 XSAVE        VFS
 XSAVE
 XSAVE        GC
 XSAVE
 XSAVE        GT
 XSAVE
 XSAVE        SNO
 XSAVE
 XSAVE        SIC
 XSAVE
 XSAVE        SICN
 XSAVE
 XSAVE        PMSL
 XSAVE
 XSAVE        PS
 XSAVE
 XSAVE        WGFL
 XSAVE     WGFL
 XSAVE        ST
 XSAVE
 XSAVE        ST"2
 XSAVE
 XSAVE        STMX
 XSAVE
 XSAVE        STMN
 XSAVE
 XSAVE        TIME MAXIMUM STMX
 XSAVE
 XSAVE        TIME MINIMUM STMN
 XSAVE
 XSAVE        SQ
 XSAVE
 XSAVE        SU
 XSAVE
 XSAVE        SV
 XSAVE
 XSAVE        SWMX
 XSAVE
 XSAVE        TIME MAXIMUM SWMX
 XSAVE
CARD
      mv new_dat old_dat

      if [ "$lmstat" = "on" ] ; then
#  ----------------------------------- select lowest model level means
ccc   xfind gp lu lv lva lt lq << CARD
 XFIND        LU
 XFIND        LV
 XFIND        LVA
 XFIND        LT
 XFIND        LQ
CARD
#  ----------------------------------- save lowest model level means
ccc   xsave old_dat lu lv lva lt lq new_dat << CARD
 XSAVE        LU
 XSAVE
 XSAVE        LV
 XSAVE
 XSAVE        LVA
 XSAVE
 XSAVE        LT
 XSAVE
 XSAVE        LQ
 XSAVE
CARD
      mv new_dat old_dat
      fi

#   ---------------------------------- adjust labels and save results
#
#                                      make year 4-digit wide.
      yeara=`expr ${year} + ${year_offset} | $AWK '{printf "%4d", $0}'`
#
ccc   relabl  old_dat new_dat << CARD
RELABL
RELABL             $yeara$mon
CARD
#
      access  old  ${atmos_file} na
      xappend old new_dat newa
      save    newa ${atmos_file}
      delete  old na || true
      release newa
