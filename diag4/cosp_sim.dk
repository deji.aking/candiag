#deck cosp_sim
jobname=cosp_sim ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<'end_of_script'

#
#                 cosp_sim             jcl - Nov 02/09
#   ---------------------------------- module to compute monthly mean fields 
#                                      related to output created by COSP
#                                      simulator.
#                                      The output from COSP is rather flexible
#                                      so deck has been set up to check for 
#                                      every possible field from COSP and if 
#                                      they are present generate diagnostics 
#                                      for them.

.   spfiles.cdk
.   ggfiles.cdk

# ----------------------------------------------------- Possible input fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# IT*  -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
# CCCP,CCCM -> Cloud fraction detected by lidar but not radar
# SR*       -> Backscattering ratio/height histogram
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CloudSat related fields
#
# ZE* -> Radar reflectivity/height histogram

# ----------------------------------------------------- Possible output fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# IT*  -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
# CCCP,CCCM -> Cloud fraction detected by lidar but not radar
# SR*       -> Backscattering ratio/height histogram
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CloudSat related fields
#
# ZE* -> Radar reflectivity/height histogram

# ---------------------------------- Notes
# There are two rather important issues to take note of for this deck
#
# 1. It only processes variables that exist in the "gs" file.  Basically, the error
#    checking is turned off, we attempt to read in the variable.
#    Therefore if the variable is not present,
#    OR the read does not work properly, the script continues onward without the variable.
#    Therefore, if you expected a particular field in the "gp" file and it 
#    is not there check first if it is present in the "gs" file and the correct
#    options were set in the GCM submission job.
#
# 2. Most of the variables need to be weighted to get the proper monthly, seasonal, etc.
#    means.  Since the pooling deck have trouble with missing values the next step is
#    to average all of the weighted mean denominators and numerators.  Potentially adds
#    a lot of extra output to the diagnostic files but is necessary to get the proper 
#    means at averaging times greater than a month.
# 
# Manipulations needed to get the proper weighted means using the fields from the diagnostic
# output
#
# ICCA = ICCA/ICCF (cloud fraction weighted)
# ICCT = ICCT/ICCF (cloud fraction weighted)
# ICCP = ICCP/ICCF (cloud fraction weighted)
# ICCF = ICCF/SUNL (weighed by "observation" occurrence, 
#                   i.e., solar zenith angle greater than 0.2)
# IT*  = IT*/SUNL  (weighed by "observation" occurrence, 
#                   i.e., solar zenith angle greater than 0.2)
#
# CLHC = CLHC/CLHM (weighted by "valid observation", i.e., backscatter above threshold)
# CLMC = CLMC/CLMM (weighted by "valid observation", i.e., backscatter above threshold)
# CLLC = CLLC/CLLM (weighted by "valid observation", i.e., backscatter above threshold)
# CLTC = CLTC/CLTM (weighted by "valid observation", i.e., backscatter above threshold)
# CLCP = CLCP/CLCM (weighted by "valid observation", i.e., backscatter above threshold)
# CCCP = CCCP/CCCM (weighted by "valid observation", i.e., backscatter above threshold)
#
# PL01 = PL01/ML01 (weighted by valid surface type, i.e., not over land)
# PL02 = PL02/ML02 (weighted by valid surface type, i.e., not over land)
# PL03 = PL03/ML03 (weighted by valid surface type, i.e., not over land)
# PL04 = PL04/ML04 (weighted by valid surface type, i.e., not over land)
# PL05 = PL05/ML05 (weighted by valid surface type, i.e., not over land)

# Variables to hold fields for output
      vars=""

# ----------------------------------- Select all of the ISCCP related fields at once

ccc select npakgg ICCA ICCT ICCF ICCP SUNL \
           IT01 IT02 IT03 IT04 IT05 IT06 IT07 IT08 IT09 IT10 IT11 IT12 IT13 IT14 \
           IT15 IT16 IT17 IT18 IT19 IT20 IT21 IT22 IT23 IT24 IT25 IT26 IT27 IT28 \
           IT29 IT30 IT31 IT32 IT33 IT34 IT35 IT36 IT37 IT38 IT39 IT40 IT41 IT42 \
           IT43 IT44 IT45 IT46 IT47 IT48 IT49 || true

# ----------------------------------- Cloud albedo

    if [ -s ICCA ]; then
      vars="$vars ICCA"
    fi

# ----------------------------------- Cloud optical thickness

    if [ -s ICCT ]; then
      vars="$vars ICCT"
    fi

# ----------------------------------- Cloud fraction

    if [ -s ICCF ]; then
      vars="$vars ICCF"      
    fi

# ----------------------------------- Cloud top pressure

    if [ -s ICCP ]; then
      vars="$vars ICCP"      
    fi

# ----------------------------------- Sun-lit indicator

    if [ -s SUNL ]; then
      vars="$vars SUNL"      
    fi

# ----------------------------------- Histogram cloud top pressure/cloud optical thickness

    if [ -s IT01 ]; then
      vars="$vars IT01 IT02 IT03 IT04 IT05 IT06 IT07 IT08 IT09 IT10 IT11 IT12 IT13 IT14"      
      vars="$vars IT15 IT16 IT17 IT18 IT19 IT20 IT21 IT22 IT23 IT24 IT25 IT26 IT27 IT28"      
      vars="$vars IT29 IT30 IT31 IT32 IT33 IT34 IT35 IT36 IT37 IT38 IT39 IT40 IT41 IT42"      
      vars="$vars IT43 IT44 IT45 IT46 IT47 IT48 IT49"      
    fi

# ----------------------------------- Select CALIPSO cloud fraction fields at once
ccc select npakgg CLHC CLHM CLMC CLMM CLLC CLLM CLTC CLTM CLCP CLCM CCCP CCCM || true

# ----------------------------------- High cloud fraction from CALIPSO

    if [ -s CLHC ]; then
      vars="$vars CLHC CLHM"      
    fi 

# ----------------------------------- Middle cloud fraction from CALIPSO

    if [ -s CLMC ]; then
      vars="$vars CLMC CLMM"      
    fi 

# ----------------------------------- Low cloud fraction from CALIPSO

    if [ -s CLLC ]; then
      vars="$vars CLLC CLLM"      
    fi 

# ----------------------------------- Total cloud fraction from CALIPSO

    if [ -s CLTC ]; then
      vars="$vars CLTC CLTM"      
    fi 

# ----------------------------------- Cloud amounts from CALIPSO

    if [ -s CLCP ]; then
      vars="$vars CLCP CLCM"      
    fi 

# ----------------------------------- Cloud amounts detected by CALIPSO but not CloudSat

    if [ -s CCCP ]; then
      vars="$vars CCCP CCCM"      
    fi 

# ----------------------------------- Backscatter/height histogram for CALIPSO
ccc select npakgg SR01 SR02 SR03 SR04 SR05 SR06 SR07 SR08 SR09 SR10 SR11 SR12 SR13 SR14 SR15 || true

    if [ -s SR01 ]; then
      vars="$vars SR01 SR02 SR03 SR04 SR05 SR06 SR07 SR08 SR09 SR10 SR11 SR12 SR13 SR14 SR15"      
    fi 

# ----------------------------------- Reflectances for PARASOL
ccc select npakgg PL01 PL02 PL03 PL04 PL05 ML01 ML02 ML03 ML04 ML05 || true

    if [ -s PL01 ]; then
      vars="$vars PL01 PL02 PL03 PL04 PL05 ML01 ML02 ML03 ML04 ML05"
    fi 

# ----------------------------------- Radar reflectivity/height histogram for CloudSat
ccc select npakgg ZE01 ZE02 ZE03 ZE04 ZE05 ZE06 ZE07 ZE08 ZE09 ZE10 ZE11 ZE12 ZE13 ZE14 ZE15 || true

    if [ -s ZE01 ]; then
      vars="$vars ZE01 ZE02 ZE03 ZE04 ZE05 ZE06 ZE07 ZE08 ZE09 ZE10 ZE11 ZE12 ZE13 ZE14 ZE15"
    fi 

# ----------------------------------- Compute the statistics and save the fields to a file

    statsav ${vars} new_gp new_xp off
    rm ${vars}

#  ---------------------------------- save results.

    access oldgp ${flabel}gp
    xjoin oldgp new_gp newgp
    save newgp ${flabel}gp
    delete oldgp

    if [ "$rcm" != "on" ] ; then
      access oldxp ${flabel}xp
      xjoin oldxp new_xp newxp
      save newxp ${flabel}xp
      delete oldxp
    fi

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         T1        = $t1
         T2        = $t2
         T3        =      $t3
0
0 CERES_SIM ---------------------------------------------------------- CERES_SIM
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT    STEPS $t1 $t2 $t3     -999999999 NAME ICCA ICCT ICCF ICCP 
SELECT     SUNL IT01 IT02 IT03 IT04 IT05 IT06 IT07 IT08 IT09 IT10 IT11 IT12 IT13
SELECT     IT14 IT15 IT16 IT17 IT18 IT19 IT20 IT21 IT22 IT23 IT24 IT25 IT26 IT27
SELECT     IT28 IT29 IT30 IT31 IT32 IT33 IT34 IT35 IT36 IT37 IT38 IT39 IT40 IT41
SELECT     IT42 IT43 IT44 IT45 IT46 IT47 IT48 IT49
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT    STEPS $t1 $t2 $t3     -999999999 NAME CLHC CLHM CLMC CLMM
SELECT     CLLC CLLM CLTC CLTM CLCP CLCM CCCP CCCM
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT    STEPS $t1 $t2 $t3     -999999999 NAME SR01 SR02 SR03 SR04
SELECT     SR05 SR06 SR07 SR08 SR09 SR10 SR11 SR12 SR13 SR14 SR15
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT    STEPS $t1 $t2 $t3     -999999999 NAME PL01 PL02 PL03 PL04
SELECT     PL05 ML01 ML02 ML03 ML04 ML05
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT    STEPS $t1 $t2 $t3     -999999999 NAME ZE01 ZE02 ZE03 ZE04
SELECT     ZE05 ZE06 ZE07 ZE08 ZE09 ZE10 ZE11 ZE12 ZE13 ZE14 ZE15
end_of_data

. endjcl.cdk
