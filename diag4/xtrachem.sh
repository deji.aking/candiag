#!/bin/sh
#                  xtrachem            KVS - Oct 13/09.
#   ---------------------------------- Generates extra information about
#                                      tracer sources/sinks
# -------------------------------------------------------------------------
#
.   ggfiles.cdk
#
#  ---------------------------------- select sulphate-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      WDL6 WDD6 WDS6  DD6
SELECT     SLO3 SLHP SDO3 SDHP SSO3 SSHP DOX4" | ccc select npakgg WDL6 WDD6 WDS6 DD6 \
                  SLO3 SLHP SDO3 SDHP \
                  SSO3 SSHP DOX4
#
#  ---------------------------------- select sulphur dioxide-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      WDL4 WDD4 WDS4  DD4
SELECT     ESFS EAIS ESTS EFIS ESVC ESVE NOXD DOXD" | ccc select npakgg WDL4 WDD4 WDS4 DD4 \
                  ESFS EAIS ESTS EFIS \
                  ESVC ESVE NOXD DOXD
#
#  ---------------------------------- select black carbon-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      WDLB WDDB WDSB  DDB
SELECT     ESFB EAIB ESTB EFIB" | ccc select npakgg WDLB WDDB WDSB DDB \
                  ESFB EAIB ESTB EFIB
#
#  ---------------------------------- select organic carbon-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      WDLO WDDO WDSO  DDO
SELECT     ESFO EAIO ESTO EFIO" | ccc select npakgg WDLO WDDO WDSO DDO \
                  ESFO EAIO ESTO EFIO
#
#  ---------------------------------- select sea salt-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      WDLS WDDS WDSS  DDS" | ccc select npakgg WDLS WDDS WDSS DDS
#
#  ---------------------------------- select mineral dust-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      WDLD WDDD WDSD  DDD
SELECT      ESD" | ccc select npakgg WDLD WDDD WDSD DDD ESD
#
#  ---------------------------------- select DMS-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      EDSL EDSO" | ccc select npakgg EDSL EDSO
#
#  ----------------------------------- compute and save statistics.
#
    statsav WDL6 WDD6 WDS6 DD6 \
            SLO3 SLHP SDO3 SDHP \
            SSO3 SSHP DOX4 \
            WDL4 WDD4 WDS4 DD4 \
            ESFS EAIS ESTS EFIS \
            ESVC ESVE NOXD DOXD \
            WDLB WDDB WDSB DDB \
            ESFB EAIB ESTB EFIB \
            WDLO WDDO WDSO DDO \
            ESFO EAIO ESTO EFIO \
            WDLS WDDS WDSS DDS \
            WDLD WDDD WDSD DDD \
            ESD \
            EDSL EDSO \
            new_gp new_xp $stat2nd
#
#  ----------------------------------- save results.
#
    access oldgp ${flabel}gp
    xjoin oldgp new_gp newgp
    save newgp ${flabel}gp
    delete oldgp

    if [ "$rcm" != "on" ] ; then
    access oldxp ${flabel}xp
    xjoin oldxp new_xp newxp
    save newxp ${flabel}xp
    delete oldxp
    fi
