#deck troptst1
jobname=tropt1 ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#                   troptst1            m lazare. apr 01/86.
#   ----------------------------------- computes the difference between an
#                                       experimental and a control run  each
#                                       comprised of one independant
#                                       simulation  and plots out the result.
#                                       this is done for statistics at
#                                       specified pressure levels in the
#                                       troposphere.
#                                       model1 and letter x refer to control
#                                       run  while model2 and letter y refer
#                                       to the experimental run.
#
    access filex ${model1}gp
    access filey ${model2}gp
    libncar plunit=$plunit 
.   plotid.cdk
#   ----------------------------------- zonal wind at 200 and 850 mbs.
.   test1gp.cdk
    newnam num du 
    square du du2 
    rm x y num
#   ----------------------------------- meridional wind at 200 and 850 mbs.
#                                       plot the vector wind difference.
.   test1gp.cdk
    newnam num dv 
    square dv dv2 
    add du2 dv2 dav2 
    sqroot dav2 dav 
    newnam dav amp 
    rm dav2 du2 dv2 dav
    ggplot amp du dv 
    rm amp du dv
    rm x y num
#   ----------------------------------- temperature at 200 and 850 mbs.
.   test1gp.cdk
    ggplot num 
    rm x y num
#   ----------------------------------- geopotential at 500 mbs.
.   test1gp.cdk
    ggplot num 
    rm x y num
#   ----------------------------------- vertical pressure velocity at 500 mbs.
.   test1gp.cdk
    ggplot num 
    rm x y num
#   ----------------------------------- specific humidity at 850 mbs.
.   test1gp.cdk
    ggplot num 
    rm x y num
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         MODEL1    = $model1 
         MODEL2    = $model2 
0 TROPTST1 ---------------------------------------------------- TROPTST1
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.      DU 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.      DV 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     AMP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  AMP  200$map        1.        0.       30.        4.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR MEAN WIND AT 200 MBS. UNITS M/SEC.
                  1.        0.       20.$ncx$ncy
                        VECPLOT OF (U,V). 
GGPLOT.           -1  AMP  850$map        1.        0.       30.        2.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR MEAN WIND AT 850 MBS. UNITS M/SEC.
                  1.        0.        5.$ncx$ncy
                        VECPLOT OF (U,V). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  NUM  200$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR T AT 200 MBS. UNITS DEGK. 
GGPLOT.           -1  NUM  850$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR T AT 850 MBS. UNITS DEGK. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        GZ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        GZ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  NUM  500$map     1.E-2    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR GZ AT 500 MBS. UNITS 100*(M/SEC)2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        W 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        W 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  NUM  500$map      1.E3    -80.E0     80.E0      2.E10${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR W AT 500 MBS. UNITS .001*PA/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        Q 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        Q 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  NUM  850$map      1.E3    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR Q AT 850 MBS. UNITS G/KG. 
. tailfram.cdk

end_of_data

. endjcl.cdk


