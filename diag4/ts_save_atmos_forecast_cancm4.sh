#!/bin/sh
#            ts_save_atmos_bp_subset   ATMOSPHERIC DATA
#
#                                      B. PAL September 2006
#
#                                      SUBSET of ts_save_atmos_bp.dk
#                                      SEPT 7/2006 meeting decision

#   ---------------------------------- accumulate time series in ${atmos_file}
#                                      Time step is constructed from ${year},
#                                      ${year_offset} and ${mon}.
#
#                                      * Upper atmosphere:
#
#                                      (u,v)200,T850,Z1000, Z500
#
#                                      * Moisture fluxes:
#
#                                      pcp:   mean pcp rate
#
#                                      * Surface state:
#
#                                       gt:   ground temperature
#                                      sno:   snow
#                                      sic:   sea ice amount
#                                     pmsl:   mean sea level pressure
#
#                                      * Screen level:
#
#                                       st:   temperature
#
#  ----------------------------------- access gp and xp file
#

      access gp ${flabel}gp
ccc   xfind gp u v gz t <<eor
 XFIND    1   U
 XFIND    1   V
 XFIND    1   GZ
 XFIND    1   T
eor

#  ----------------------------------- 200, 500, 850 1000 mb u, v, phi, t
      for x in u v gz t ; do
        for l in 200 500 850 1000 ; do
          LEV=`echo "    $l" | tail -5c`
ccc       select $x $x.$l <<CARD
SELECT    STEPS $t1 $t2 $t3 LEVS $LEV $LEV NAME  ALL
CARD
          if [ "$x" = "gz" ] ; then
#------------------------------divide by g to convert GZ to Z
ccc           xlin $x.$l  x1 << CARD 
 XLIN      0.1019368
CARD

ccc           relabl  x1  $x.$l <<CARD
RELABL
RELABL                       Z
CARD
              rm   x1
           fi
        done
      done

      rm   u.1000 v.1000 t.1000

#  ----------------------------------- save upper atmosphere quantities
ccc   xsave old_dat u.200 u.500 u.850 v.200 v.500 v.850 t.200 t.500 t.850 \
           gz.200 gz.500 gz.850 gz.1000 new_dat <<CARD
 XSAVE        U (200MB)
 NEWNAM
 XSAVE        U (500MB)
 NEWNAM
 XSAVE        U (850MB)
 NEWNAM
 XSAVE        V (200MB)
 NEWNAM
 XSAVE        V (500MB)
 NEWNAM
 XSAVE        V (850MB)
 NEWNAM
 XSAVE        T (200MB)
 NEWNAM
 XSAVE        T (500MB)
 NEWNAM
 XSAVE        T (850MB)
 NEWNAM
 XSAVE        Z (200MB)
 NEWNAM
 XSAVE        Z (500MB)
 NEWNAM
 XSAVE        Z (850MB)
 NEWNAM
 XSAVE        Z (1000MB)
 NEWNAM
CARD
      mv new_dat old_dat

#  ----------------------------------- select sfc/TOA means
ccc   xfind gp pcp gt sno sic sicn pmsl st fla flg fslo stmn stmx sq wgf wgl  <<CARD
 XFIND        PCP
 XFIND        GT
 XFIND        SNO
 XFIND        SIC
 XFIND        SICN
 XFIND        PMSL
 XFIND        ST
 XFIND        FLA
 XFIND        FLG
 XFIND        FSLO
 XFIND        STMN
 XFIND        STMX
 XFIND        SQ
 XFIND        WGF
 XFIND        WGL
CARD
ccc   xfind gp rof fss cldt tg swmx  <<CARD
 XFIND        ROF
 XFIND        FSS
 XFIND        CLDT
 XFIND        TG
 XFIND        SWA
CARD

# MAKE OLR
add fla flg tmp1
ccc           xlin  tmp1 tmp2 << CARD 
 XLIN           -1.0         0
CARD
add fslo tmp2 tmp3
ccc           relabl  tmp3  olr <<CARD
RELABL
RELABL                     OLR
CARD
release tmp1 tmp2 tmp3

#MAKE VFSL
access dzg t63_dzg
ccc rcopy  dzg  dzg1 << eor
RCOPY              1         1
eor
ccc rcopy  dzg  dzg2 << eor
RCOPY              2         2
eor
ccc rcopy  dzg  dzg3 << eor
RCOPY              3         3
eor
add dzg1  dzg2 dzg12
add dzg12 dzg3 dzgt
release dzg dzg1 dzg2 dzg3 dzg12

access corr t63_dzgt_chfp2d_div_by_chfp2c

# Convert mass_i to vol_i using
# VL_i = (liq kg)_i/(liq kg m**3) + (ice kg)_i/(ice kg m**3) = m**3
# and find vol frac using
# VF = Sum_i( VL_i ) / Sum_i( DZG_i )

levssm="00001 00002 00003"

for lev in $levssm; do

ccc select wgl WGL_${lev} <<eor
C*SELECT.  STEP         1 999999999    1     $lev$lev NAME  WGL
eor

ccc select wgf WGF_${lev} <<eor
C*SELECT.  STEP         1 999999999    1     $lev$lev NAME  WGF
eor

ccc xlin WGL_${lev} WGL_${lev}a <<eor
  XLIN         0.001       0.0
eor
ccc xlin WGF_${lev} WGF_${lev}a <<eor
  XLIN    0.00109051       0.0
eor
add WGL_${lev}a WGF_${lev}a vol_${lev}

rm WGL_${lev}a
rm WGF_${lev}a

done

#MAKE TCSM

add WGF_00001 WGL_00001 tmp1
add WGF_00002 WGL_00002 tmp2
add WGF_00003 WGL_00003 tmp3
add tmp1 tmp2 tmp4
add tmp3 tmp4 t_last

ccc relabl t_last tcsm <<eor
C*         GRID            
           GRID           TCSM    1
eor
rm tmp1 tmp2 tmp3 tmp4 t_last

for lev in $levssm; do
rm WGL_${lev}
rm WGF_${lev}
done

add vol_00001 vol_00002 vol_12
add vol_12 vol_00003 vol_t
gdiv vol_t dzgt tsv
rm vol_00001 vol_00002 vol_12 vol_00003 vol_t

mlt tsv corr aa
mv aa tsv

lev="    1"  
ccc relabl tsv vfsm <<eor
C*         GRID  
           GRID           VFSM
eor

release tsv

#  ----------------------------------- save sfc/TOA means
ccc   xsave old_dat pcp gt sno sic sicn pmsl st olr stmn stmx sq vfsm cldt fss rof tcsm swmx tg wgf wgl \
            new_dat <<CARD
 XSAVE        PCP
 NEWNAM
 XSAVE        GT
 NEWNAM
 XSAVE        SNO
 NEWNAM
 XSAVE        SIC
 NEWNAM
 XSAVE        SICN
 NEWNAM
 XSAVE        PMSL
 NEWNAM
 XSAVE        ST
 NEWNAM
 XSAVE        OLR
 NEWNAM
 XSAVE        STMN
 NEWNAM
 XSAVE        STMX
 NEWNAM
 XSAVE        SQ
 NEWNAM
 XSAVE        VFSM
 NEWNAM
 XSAVE        CLDT
 NEWNAM
 XSAVE        FSS
 NEWNAM
 XSAVE        ROF
 NEWNAM
 XSAVE        TCSM
 NEWNAM
 XSAVE        SWMX
 NEWNAM
 XSAVE        TG
 NEWNAM
 XSAVE        WGF
 NEWNAM
 XSAVE        WGL
 NEWNAM
CARD
      mv new_dat old_dat

#   ---------------------------------- adjust labels and save results
#
      if [ "$mon" -ge "$monr" ] ; then
	yeara=`expr $year + 0 | $AWK '{printf "%4d", $0}'`;
      else
	yeara=`expr $year + 0 | $AWK '{printf "%4d", $0}'`;
      fi

#
ccc   relabl  old_dat new_dat <<CARD
RELABL
RELABL             $yeara$mon
CARD
#
      access  old  ${atmos_file} na
      xappend old new_dat newa
      save    newa ${atmos_file}
      delete  old na
      release newa
