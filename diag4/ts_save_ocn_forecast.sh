#!/bin/sh
#
#                           OCEAN DATA
#
#                           BP September, 2006
#
#                           SUBSET of ts_save_ocn_bp.dk
#                           SEPT 7/2006 meeting decision
#
#                           constructs time series of 
#                           SST - sea surface temp.
#
#
#----------------------------------------------Extract sst, sss, t, u,v
#
    touch xxa.sst

    for  n  in $months ;
    do

       if [ "$n" -ge "$mon_start" ] ; then
            year0=`expr $year + 0 | $AWK '{printf "%4d", $0}'`;
            access  gz mf_${modelh}_${year0}_m${n}_gz
       else
            year0=`expr $year + 1 | $AWK '{printf "%4d", $0}'`;
            access  gz mf_${modelh}_${year0}_m${n}_gz
       fi


ccc     xfind gz t <<eor
 XFIND        OCEAN TEMPERATURE
eor


        for x in sst ; do

            if [ "$x" = "sst" ] ; then

ccc         select   t      x2 <<eor
  SELECT. STEPS         0 999999999         1    0   50      TEMP
eor
            fi

            yymm="    ${year0}$n";

ccc         relabl  x2  x3 <<eor
    RELABL GRID
           GRID$yymm
eor
           cat x3 >> xxa.$x
           rm  $x x2 x3

        done

        release  gz

      done
ccc    relabl  xxa.sst  aa <<CARD
RELABL
RELABL                     SST
CARD
mv aa xxa.sst

ccc   xsave old_dat xxa.sst new_dat <<CARD
 XSAVE        OCEAN SURFACE TEMP (SST)
 XSAVE
CARD
       rm      xxa.sst
#
      access  old  ${atmos_file} na
      xappend old new_dat newa
      save    newa ${atmos_file}
      delete  old na  
      release newa

      rm  old_dat new_dat
