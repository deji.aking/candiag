#deck eng_stat4
jobname=eng_stat4 ; time=$gptime ; memory=$memory3
. comjcl.cdk

cat > Execute_Script <<'end_of_script'
# Add switch for calculating albedos from monthly means,
# (default - calculate albedos from monthly fluxes)
#                  eng_stat4           ML - Nov 17/08 - SK ----------eng_stat4
#   ---------------------------------- Compute energy fluxes and budgets terms.
#                                      This is based on eng_stat2, but has 
#                                      a bugfix to remove FSLO from the
#                                      calculation of the s/w cloud forcing.
#
#  Dictionary for output variables:
#
#  1) Energy fluxes:
#
#      a) Atmospheric energy fluxes
#
#   FSO : solar flux incident on top of atmosphere
#   FSA : solar flux absorbed by atmosphere
#   FLA : net lw absorbed/emitted by atmosphere
#  FSLO : solar/lw overlap (gcm15 onward)
#
#      b) Surface energy fluxes
#
#   FSS : flux received at the surface
#   FSG : solar heat absorbed by ground
#   FLG : net lw at the ground
#   FDL : downward atmospheric lw at sfc
#   FSV : visible flux received at the surface
#   FSD : direct flux received at the surface
#   HFS : surface sensible heat flux
#   HFL : surface latent heat flux
#
#      c) Earth-atmosphere fluxes
#
# FSAG : heat absorbed by earth-atmosphere system,
#        i.e. net solar flux at top of the atmosphere
# FLAG : net lw emitted by earth-atmosphere system,
#        i.e. net lw at the top of atmosphere
#
#   2) Net fluxes
#
# BALG : radiation budget at the ground
# BALT : radiation budget at top of atmosphere
#        i.e. at the highest full sigma level
#  FSR : reflected solar flux at TOA
# FSRC : clear-sky reflected solar flux at TOA
#  OLR : outgoing l/w radiation
# OLRC : clear-sky outgoing l/w radiation
#  BEG : net surface energy flux (at ice-atmosphere interface above sea-ice)
# OBEG : net surface energy flux (at ice-ocean interface below sea-ice)
#  RES : energy residual (used for non-interactive ocean runs)
# CBGO : energy removed at sea-ice/ocean interface to ensure no heat flow into
#        ice covered ocean when sea-ice mass is specifed
#        (used for non-interactive ocean runs)
#
#   3) Near-surface quantities
#
#   ST : screen level (2m) temperature
#  STMX: mean screen level daily maximum temperature
#  STMN: mean screen level daily minimum temperature
#
#   4) Misc
#
#      a) Albedoes - calculated from monthly mean fluxes, by default, unless
#         albedo_monthly=off in which case they are computed from (sub)daily fluxes.
#
#  LPA : planetary albedo
# SALB : surface albedo
#
#      b) Clear-sly fluxes, cloud forcings and albedoes
#
# FSTC : net clear-sky solar flux at toa.
# FSGC : net clear-sky solar flux at ground.
# CFST : solar cloud forcing at toa.
# CFSB : solar cloud forcing at ground.
# FLTC : net clear-sky lw flux at toa.
# FLGC : net clear-sky lw flux at ground.
# CFLT : lw cloud forcing at toa.
# CFLB : lw cloud forcing at ground.
#  CFT : total cloud forcing at toa.
#  CFB : total cloud forcing at ground.
# LPAC : clear-sky planetary albedo
#
# -------------------------------------------------------------------------
#
.   ggfiles.cdk
#  ---------------------------------- select input fields.
#
ccc select npakgg FSO FSA FLA FSS  \
                  FSG FLG FDL FSV  \
                  FSD HFS HFL FSTC \
                  FSGC FLTC FLGC

#  ----------------------------------- try to select FSLO used with gcm15 onward
#                                      in calculation of planetary albedo.
ccc select npakgg FSLO || true

#  ----------------------------------- create a zero field if FSLO does not exist.

    if [ ! -s FSLO ] ; then
     echo ' XLIN             0.        0.' > .xlin_input_card
     xlin FSO FSLO input=.xlin_input_card
     rm .xlin_input_card
    fi
#
ccc select npakgg BEG OBEG

#  ----------------------------------- try to select RES and CBGO
#                                      (for non-interactive ocean coupled runs only)
ccc select npakgg RES CBGO || true

#  ----------------------------------- set the switch if RES is not found

    nonint="on"
    if [ ! -s RES ] ; then
      nonint="off"
    fi
#
ccc select npakgg ST STMX STMN

    rm -f npakgg .npakgg_Link

#
#  ----------------------------------- earth-atmosphere fluxes
#
    add FSA FSG FSAG
    add FLA FLG FLAG

#
#  ----------------------------------- energy budgets
#
    add FSAG FLAG BALT
    add FSG  FLG  BALG

#
#   ---------------------------------- screen temperature and daily maximum/
#                                      minimum screen temperature.

    echo ' XLIN          1.0E0 -2.7316E2' > .xlin_input_card

    for x in ST STMX STMN
    do
      xlin $x .$x input=.xlin_input_card
      mv .$x $x
    done

#
#   ---------------------------------- reflected solar flux at TOA.
#                                      limit FSR to non-negative values to
#                                      avoid numerical differences between
#                                      fsag calculated from model data  i.e.
#                                      fsa derived from vertical integration
#                                      of heating rates  and "precise"
#                                      calculation of fso.

    echo ' FMASK            -1   -1   GT        0.' > .fmask_input_card

    sub    FSO  FSLO fsia
    sub    fsia FSAG fsri
    fmask  fsri msk  input=.fmask_input_card
    mlt    fsri msk  FSR
    rm     fsri msk  fsia

#   ---------------------------------- local planetary albedo.

    if [ "$albedo_monthly" != "off" ] ; then
      timavg FSR .tFSR
      timavg FSO .tFSO
      div  .tFSR .tFSO LPA
    else
      div    FSR   FSO LPA
    fi
#
#   ---------------------------------- reflected clear-sky solar flux at TOA.
#                                      limit FSRC to non-negative values to
#                                      avoid numerical differences between
#                                      fsag calculated from model data  i.e.
#                                      fsa derived from vertical integration
#                                      of heating rates  and "precise"
#                                      calculation of fso.
    sub    FSO  FSLO fsia
    sub    fsia FSTC fsri
    fmask  fsri msk  input=.fmask_input_card
    mlt    fsri msk  FSRC
    rm     fsri msk  fsia

#   ---------------------------------- local clear-sky planetary albedo.

    if [ "$albedo_monthly" != "off" ] ; then
      timavg FSRC .tFSRC
      div  .tFSRC .tFSO LPAC
    else
      div    FSRC   FSO LPAC
    fi

#   ---------------------------------- ground albedo. limit fsrg to
#                                      non-negative values to avoid problems
#                                      with small numerical differences
#                                      when fss is near zero.
    sub    FSS   FSG  fsrgi
    fmask  fsrgi msk  input=.fmask_input_card
    mlt    fsrgi msk  fsrg
    if [ "$albedo_monthly" != "off" ] ; then
      timavg fsrg tfsrg
      timavg FSS  .tFSS
      div   tfsrg .tFSS SALB
    else
      div    fsrg  FSS  SALB
    fi
    rm     fsrgi msk  fsrg

#   ----------------------------------  OLR (all-sky and clear-sky).

    sub    FSLO  FLAG OLR
    sub    FSLO  FLTC OLRC

#
#   ---------------------------------- cloud forcings
#
    sub FSAG FSTC CFST
    sub FSG  FSGC CFSB
    sub FLAG FLTC CFLT
    sub FLG  FLGC CFLB
    add CFST CFLT CFT
    add CFSB CFLB CFB

#
#  ----------------------------------- compute and save statistics.
#
    statsav FSO  FSA  FLA  FSS  FSG  FLG  FDL  FSV  FSD  HFS  HFL \
            FSAG FLAG BALT BALG BEG  OBEG \
            ST   STMX STMN FSR  FSRC OLR  OLRC \
            FSTC FSGC FLTC FLGC CFST CFSB CFLT CFLB CFT  CFB \
            new_gp new_xp $stat2nd
#
#  ----------------------------------- compute albedos only where FSO > 5 W/m2
#                                      (~1% of max FSO) to avoid areas where
#                                      albedos are unreliable.
    rm -f .fmask_input_card
    echo ' FMASK            -1   -1   GT        5.' > .fmask_input_card
    if [ "$albedo_monthly" != "off" ] ; then
      fmask .tFSO  MFSO input=.fmask_input_card
      statsav mask=MFSO LPA LPAC new_gp new_xp off
      fmask .tFSS  MFSS input=.fmask_input_card
      statsav mask=MFSS SALB     new_gp new_xp off
    else
      fmask   FSO  MFSO input=.fmask_input_card
      statsav mask=MFSO LPA LPAC new_gp new_xp $stat2nd
      fmask   FSS  MFSS input=.fmask_input_card
      statsav mask=MFSS SALB     new_gp new_xp $stat2nd
    fi

    statsav STMX new_gp new_xp timmax
    statsav STMN new_gp new_xp timmin
#
    if [ "$nonint" = "on" ] ; then
      statsav RES CBGO new_gp new_xp $stat2nd
    fi
#
    rm      FSO  FSA  FLA  FSS  FSG  FLG  FDL  FSV  FSD  HFS  HFL \
            FSAG FLAG BALT BALG BEG  OBEG \
            ST   STMX STMN LPA  LPAC SALB FSR  FSRC OLR  OLRC \
            FSTC FSGC FLTC FLGC CFST CFSB CFLT CFLB CFT  CFB \
            RES  CBGO

#  ----------------------------------- save results.

    access oldgp ${flabel}gp
    xjoin oldgp new_gp newgp
    save newgp ${flabel}gp
    delete oldgp

    if [ "$rcm" != "on" ] ; then
    access oldxp ${flabel}xp
    xjoin oldxp new_xp newxp
    save newxp ${flabel}xp
    delete oldxp
    fi

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         MODEL1    = $model1
         R1        = $r1
         R2        = $r2
         R3        =      $r3
0 ENG_STAT4 ---------------------------------------------------------- ENG_STAT4
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FSO  FSA  FLA  FSS
SELECT      FSG  FLG  FDL  FSV  FSD  HFS  HFL FSTC FSGC FLTC FLGC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      FSLO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $g1 $g2 $g3         1    1       BEG OBEG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $g1 $g2 $g3         1    1       RES CBGO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $t1 $t2 $t3         1    1        ST STMX STMN
end_of_data

. endjcl.cdk
