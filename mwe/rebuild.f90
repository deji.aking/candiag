module rebuild
  ! TODO:
  ! - allow ccc_data_array to grow dynamically (e.g. to rebuild unknown # of fields).
  ! - rebuild all fields, not just selected ones
  ! - Remove vestigule f66 code; improve some naming etc.
  
  use ccc_data, only: ccc_data_array
  use diag_sizes
  use utilities, only: timer_start, timer_stop
  
  implicit none
  private
  public rebuild_gcm
  
contains

  subroutine rebuild_gcm(file_prefix, num_tiles, variables, cccdata)
    implicit none
    character (len=*),    intent(in)                 :: file_prefix  ! Prefix of input files
    character (len=*),    intent(in),    dimension(:) :: variables    ! Vars to process
    integer,              intent(in)                  :: num_tiles    ! Number of tiles 
    type(ccc_data_array), intent(inout), dimension(:) :: cccdata
    call joinpio_inline(file_prefix, num_tiles, variables, cccdata)
    call pakgcm_inline(cccdata)
  end subroutine rebuild_gcm

  subroutine joinpio_inline(file_prefix, num_tiles, variables, cccdata)
! NCS 2020, heavily modified from original joinpio.f program
!           Many simplifications for a minimal working example
!           Not necessarily functional for all fields and cases.
!
!      IMPLICIT REAL (A-H,O-Z)  !< BAD DOG!
!      IMPLICIT INTEGER (I-N)   !< NO!
    
      ! So easy, why not do it?
      integer maxx, maxxr
      integer ibuf, jbuf
      integer i, k, n
      integer idat, jdat
      integer idummy
      integer intsize
      integer nc4to8
      integer me32o64, machine
      integer inteflt
      integer num_infiles
      integer nwds, nrecs, nr, nf
      integer :: noffst
      
      PARAMETER (MAXX = SIZES_BLONP1xBLATxNWORDIO , MAXXR = SIZES_BLONP1xBLAT )
      REAL A(MAXXR)
      LOGICAL OK,SPEC, record_loop, parse_tiles
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
      COMMON/JCOM/JBUF(8),JDAT(MAXX)

      COMMON /MACHTYP/ MACHINE,INTSIZE ! Needed to avoid calling jclpnt

      character (len=*)  , intent (in) :: file_prefix    ! Input file prefix
      integer            , intent (in) :: num_tiles      ! Number of tiles / input files
      character (len=999)              :: input_filename ! Full names of files
      integer                          :: io_status      ! Status of file opens
      integer                          :: loop_count     !
      character (len=*),    intent(in),    dimension(:) :: variables ! Vars to process      
      type(ccc_data_array), dimension(:), intent(inout) :: cccdata   ! cccdata array to fill
      type(ccc_data_array)             :: cccdata_instance 
      integer                          :: ivar           ! 
      logical                          :: include_var    ! 
      integer                          :: cccdat_index   ! index in the array
      character (len=4)                :: current_var
      integer                          :: current_var2
      INTEGER                          :: LEV(49)          ! model levels.
                                                           ! should come from namelist.
      integer                          :: nsl              ! number of model levels

      call timer_start('joinpio')
      ! NCS 2020.
      ! The basic task here is to open the input files for reading
      ! and possibly to open an output file for writing to.
      ! This should be improved, but is much simpler than jclpnt and 30x shorter!

      ! output file in 10. Should use a name for the output unit.
      !open(unit=10, file='GCM', form='UNFORMATTED')
      !rewind 10
      
      ! Loop over the input tiles / files and open on units from 11 up,
      ! following the existing convention (again should use named variables).
      NF = num_tiles ! only for legavy use below
      do i=1, num_tiles
         ! format the input filename
         write (input_filename, "(A,I0.2)") trim(trim(file_prefix)//'_'), i-1

         ! Open input file for reading
         open(unit=10+i, file=input_filename, &
              form='UNFORMATTED', action='READ', &
              iostat=io_status)
         rewind(10+i)
         
         ! If opening any input file fails, then hard exit.
         if ( io_status /= 0 )  CALL XIT('JOINPIO',-2)
      end do
      open(unit=10, file='OUTGCM_TEST', form='UNFORMATTED')
      rewind (10)

      
      ! DETERMINE THE MACHINE TYPE. This is used below for determing
      ! lengths / sizes of input. In calls to recget/rdbuf.
      MACHINE = ME32O64(IDUMMY)
      INTSIZE = INTEFLT(IDUMMY)
!--------------------------------------------------------------------
      NRECS=0
      NR=0

!     * READ DATA RECORDS FROM FIRST INPUT FILE.
      record_loop=.TRUE.
      loop_count = 0

      ! Allocate cccdata objects before parsing the full files
      ! This requires querying the file.
      
!       write(6,*) "About to try allocation"
!       do ivar = 1, size(variables)
! !         write(6,*) "Working on ", variables(ivar), ivar
!          rewind(11)
!          CALL GETFLD2(11,A, -1 ,-1,variables(ivar),-1,IBUF,MAXX,OK)
! !         write(6,*) ok
         
!          IF(OK)THEN
!           SPEC=(IBUF(1).EQ.NC4TO8("SPEC"))
!           NWDS=IBUF(5)*IBUF(6)*NF
!           IF (SPEC) THEN
!               NWDS=NWDS*2
!               IBUF(5)=NF*IBUF(5)
!           ELSE
!               IBUF(6)=NF*IBUF(6)
!           ENDIF
      
!           current_var = variables(ivar)          
! !          write(6,*) 'nwds:', current_var, nwds 
!           ! Initialize the cccdata object. Could just use dynamic grow below, but
!           ! this is much faster. Must be generalized moving forward. This is a
!           ! brutal hack for efficiency in allocation.
!               if (IBUF(4) < 7 .AND. IBUF(4) > 0) then
! !                write(6,*) "Allocating 2D"
!                 call cccdata(ivar)%alloc(ADJUSTR(current_var), nwds, &
!                              1, 124, IBUF)
!              else
! !                write(6,*) "Allocating 3D"                
!                 call cccdata(ivar)%alloc(ADJUSTR(current_var), nwds, &
!                              1, 124*49, IBUF)
!               end if            
!            endif
!        end do
!        rewind(11)
!        write(6,*) "Done allocating"
       
      do  while (record_loop)
         
          CALL GETFLD2(11,A, -1 ,0,0,0,IBUF,MAXX,OK)
          IF(.NOT.OK)THEN
              IF(NR.EQ.0) CALL                           XIT('JOINPIO',-1)
              record_loop=.FALSE.
              exit ! This means "success" - goto the end
          ENDIF
               
          include_var=.FALSE.
          do ivar = 1, size(variables)
             if (IBUF(3).EQ.NC4to8(variables(ivar))) then
                include_var=.TRUE.
                current_var = variables(ivar)
                cccdat_index = ivar
!                write(*,*) 'joinpio: ', current_var, cccdat_index, cccdata(ivar)%name
                exit
             end if   
          end do
          if (.not. include_var) then
              !write(6,*) "Excluding record from joinpio:"
              !call prtlab(IBUF)
              cycle ! Skip record, cycle outer loop
          end if
          !include_var=.TRUE.
          !current_var2 = IBUF(3)
          !write(current_var, "(A)") IBUF(3)
          !write(6,*) "Joining paralell:", current_var
          
          !IF(NR.EQ.0) CALL PRTLAB(IBUF)
          NR=NR+1
          
          ! ABORT ON OTHER THAN "GRID" AND "SPEC" DATA RECORDS.
          IF (IBUF(1).NE.NC4TO8("GRID").AND.IBUF(1).NE.NC4TO8("SPEC")) &
          CALL                                       XIT('JOINPIO',-2)

          SPEC=(IBUF(1).EQ.NC4TO8("SPEC"))
          NWDS=IBUF(5)*IBUF(6)
          IF(SPEC) NWDS=NWDS*2

          ! WRITE OUT "SCALAR AND FULL GRIDS IF PRESENT & cycle back
          IF(IBUF(1).EQ.NC4TO8("GRID").AND. &
            (NWDS.EQ.1.OR.NWDS.EQ.IBUF(5)*(IBUF(5)-1)/2)) THEN
              !CALL PUTFLD2(10,A,IBUF,MAXX)
              IF(NR.EQ.1) CALL PRTLAB(IBUF)
              cycle ! Cycle outer loop
          ENDIF

          ! ABORT IF EXPECTED OUTPUT ARRAY SIZE IS NOT LARGE ENOUGH.
          IF (NF*NWDS.GT.MAXXR) CALL                   XIT('JOINPIO',-3)

          ! SKIP TO WRITE THE RECORD READ IF ONLY ONE INPUT FILE
          ! IS GETTING PROCESSED!.
          if (NF.EQ.1) then
             parse_tiles=.FALSE.
          else
             parse_tiles=.TRUE.
          end if
          ! LOOP OVER AND READ THE REMAINING ELEMENTS FOR THE CURRENT
          ! RECORD PROCESSED FROM THE REST OF THE INPUT FILES
          if (parse_tiles) then
              do N=12,10+NF
                  NOFFST=(N-11)*NWDS+1
                  !B=A
!                  write(6,*) 'om:' N, NOFFST
                  do ! Loop until we find the desired record
                      CALL GETFLD2(N,A(NOFFST),-1 ,0,0,0,JBUF,MAXX,OK)
                      if (JBUF(3).EQ.NC4to8(current_var)) then
                         ! exit inner-inner do loop
                          exit
                      end if
                  end do
                  !A=B
                  IF(.NOT.OK) CALL                            XIT('JOINPIO',-4)
                 
                  ! ABORT IF ANY 2 CORRESPONDING LABEL ELEMENTS DIFFER.
                  do K=1,8
                      if (IBUF(K).NE.JBUF(K)) THEN
                          call PRTLAB(IBUF)
                          call PRTLAB(JBUF)
                          call                                XIT('JOINPIO',-5)
                      end if
                  end do
               end do
          end if     
  
          ! ADJUST THE DIMENSION BASED ON THE RECORD KIND.
          IF (SPEC) THEN
              IBUF(5)=NF*IBUF(5)
          ELSE
              IBUF(6)=NF*IBUF(6)
          ENDIF

          ! WRITE OUT THE JOINED ELEMENT OF THE CURRENT RECORD PROCESSED
          ! AND LOOP BACK FOR THE REST.
          !CALL PUTFLD2(10,A,IBUF,MAXX)
          !IF(NR.EQ.1) CALL PRTLAB(IBUF)
          
          ! Reading the data into A, then later copying it into the cccdata object is very slow.
          ! It would be much faster to pre-allocate the cccdata array, and read the data directly into it.
          ! joinpio takes about 20s on a 7GB 1 month GCM tile set. As written this inline version takes much longer,
          ! But all the excess time is associated with the memory copy below. 
          
          call cccdata(cccdat_index)%add_data(ADJUSTR(current_var), A, IBUF(1:8))
          loop_count = loop_count + 1
      end do ! end of out do while

! !--------------------------------------------------------------------
      ! close opened files
      write(*,*) "Done joinpio..."
      close(10)
      do i=1, num_infiles
         close(10+i)
      end do
      write(*,*) 'Completed joining of tiles.'
      call timer_stop('joinpio')
      
  end subroutine joinpio_inline

!==========================================================================

  subroutine pakgcm_inline(cccdata)
    ! NCS 2020 - significantly modified from pakgcm9.f
    !          - radically simplified for MWE. Might not support some fields
    !             and packing densities etc. Tested with packing density 0.
    !-------------------------------------------------------------------------
    implicit none
    
    integer LSR(2,SIZES_LMTP1+1)
    logical OK
    integer KT(3),NREC(3),NPAK(3),IBUFS(8,3)
    integer i, iloop, jloop, kloop
    integer nc4to8
    integer nrecs
    integer nsp
    integer name
    integer npack, nkind
    integer la, lr, lm, ktr, n
    integer ibuf, idat
    real F, G

    integer, parameter :: MAXX = &
         (SIZES_LONP1+1)*SIZES_MAXLEV*(4+SIZES_NTRAC)*SIZES_NWORDIO

    COMMON/BLANCK/ F((SIZES_LONP1+1)*SIZES_MAXLEV*(4+SIZES_NTRAC)), &
         G((SIZES_LONP1+1)*SIZES_MAXLEV*(4+SIZES_NTRAC))
    COMMON/BUFCOM/ IBUF(8), IDAT(MAXX)

    type(ccc_data_array), intent(inout), dimension(:) :: cccdata
    type(ccc_data_array) :: cccdata_instance
    !----------------------------------------------------------------------
    call timer_start('pakgcm')
    write(*,*) "Starting re-packing"
    
    KT(1)=NC4TO8("SPEC")
    KT(2)=NC4TO8("GRID")
    KT(3)=NC4TO8("ENRG") ! probably obsolete?

    !      NCS 2020, simple file open replacement for jclpnt
    !--------------------------------------------------------------------
    !      CALL JCLPNT (NFIL,1,21,22,23,6)
    ! Assume GCM file name for now.
    NKIND = 2    ! number of output files / types (ss and gs)
    NPAK(1) = 21 ! output file 1 unit
    NPAK(2) = 22 ! output file 2 unit
    NREC(:) = 0  ! number of records in each 
    !--------------------------------------------------------------------
    NRECS = 0
    NSP=0
    ! PROCESS INPUT FIELDS AND WRITE SELECTED ONES TO THE APPROPRIATE
    ! OUTPUT FILE.
    open(unit=NPAK(1), file='ss', form='UNFORMATTED')
    rewind NPAK(1)
    open(unit=NPAK(2), file='gs', form='UNFORMATTED')
    rewind NPAK(2)

    ! iloop is the loop over variables in the ccc datat structure
    ! jloop is the loop over time steps for the given variable
    
    do iloop = 1, size(cccdata)
       cccdata_instance = cccdata(iloop)
       IBUF = cccdata_instance%IBUF(1:8)

       ! SKIP "DATA" NAMED RECORDS.
       IF(IBUF(3).EQ.NC4TO8("DATA")) cycle

       if (cccdata_instance%name == "    ") then
          write(*,*) "Warning: Empty record in pakgcm_inline"
          CALL PRTLAB(IBUF)
          cycle
       end if   
       do jloop = 1, cccdata_instance%nt
          IBUF(2) = cccdata_instance%time(jloop)
          do kloop = 1, cccdata_instance%nlev
              NRECS=NRECS+1
              IBUF(4) = cccdata_instance%level(kloop) 

              ! NCS 2020 remove support ofr "ENRG" type of records

              if(IBUF(1).eq.KT(1)) then
                 ! "SPEC" KIND RECORDS...
                 N=1
                 NREC(N)=NREC(N)+1

                 ! SET DEFAULT PACKING DENSITY FOR MATCHING RECORDS.
                 ! CALCULATE SPECTRAL TRIANGLE INFORMATION (INVARIANT
                 ! SO ONLY NEED TO DO THIS ONCE).
                 NPACK=2
                 IF(NSP.EQ.0) CALL DIMGT(LSR,LA,LR,LM,KTR,IBUF(7))
                 NSP=1

                 ! CONVERT ORDERED LOW-HIGH ZONAL WAVENUMBER PAIRS TO USUAL
                 ! SPECTRAL TRIANGLE ORDER, IE (1,LM,2,LM-1...) -> (1,2,...LM-1,LM).

                 ! NCS 2020, hard assume packing density of 0.
                 ! However, also note RECUP2 should have been called during rebuild above.
                 !CALL RECUP2(G,IBUF)
                 G(:)=0
                 do i=1, cccdata_instance%np
                    G(i) = cccdata_instance%data(i, kloop, jloop)
                 end do

                 IBUF(8)=NPACK
                 CALL REC2TRI (F,G,LA,LSR,LM)

                 IF(IBUF(3).EQ.NC4TO8("LNSP")) THEN
                    ! CONVERT LOG OF SURFACE PRESSURE LNSP FROM PA TO MB.
                    IBUF(4)=1
                    F(1)=F(1)-LOG(100.E0)*SQRT(2.E0)
                 ENDIF
              else
                 ! OTHERS RECORDS, INCLUDING "GRID" KIND, NOT MATCHING ANY
                 ! OF THE ABOVE TYPES...
                 N=2
                 NREC(N)=NREC(N)+1

                 ! SET DEFAULT PACKING DENSITY FOR MATCHING RECORDS.
                 NPACK=2
                 NAME=IBUF(3)

                 IF(NAME.EQ.NC4TO8("PVEG") .OR. NAME.EQ.NC4TO8("SVEG") .OR. &
                    NAME.EQ.NC4TO8("SOIL") .OR. NAME.EQ.NC4TO8("PBLT") .OR. &
                    NAME.EQ.NC4TO8("  GC") .OR. NAME.EQ.NC4TO8(" TCV") .OR. &
                    NAME.EQ.NC4TO8("WTAB") .OR. NAME.EQ.NC4TO8("SUNL")) NPACK=1

                 IF(NAME.EQ.NC4TO8("CF01") .OR. NAME.EQ.NC4TO8("CF02") .OR. &
                    NAME.EQ.NC4TO8("CF03") .OR. NAME.EQ.NC4TO8("CF04")) NPACK=1

                 IF(NAME.EQ.NC4TO8("OT01") .OR. NAME.EQ.NC4TO8("OT02") .OR. &
                    NAME.EQ.NC4TO8("OT03") .OR. NAME.EQ.NC4TO8("OT04") .OR. &
                    NAME.EQ.NC4TO8("OT05") .OR. NAME.EQ.NC4TO8("OT06") .OR. &
                    NAME.EQ.NC4TO8("OT07") .OR. NAME.EQ.NC4TO8("OT08") .OR. &
                    NAME.EQ.NC4TO8("OT09") .OR. NAME.EQ.NC4TO8("OT10") .OR. &
                    NAME.EQ.NC4TO8("OT11") .OR. NAME.EQ.NC4TO8("OT12") .OR. &
                    NAME.EQ.NC4TO8("OT13") .OR. NAME.EQ.NC4TO8("OT14") .OR. &
                    NAME.EQ.NC4TO8("OT15") .OR. NAME.EQ.NC4TO8("OT16") .OR. &
                    NAME.EQ.NC4TO8("OT17") .OR. NAME.EQ.NC4TO8("OT18") .OR. &
                    NAME.EQ.NC4TO8("OT19") .OR. NAME.EQ.NC4TO8("OT20")) NPACK=1

                 IF(NAME.EQ.NC4TO8("OT21") .OR. NAME.EQ.NC4TO8("OT22") .OR. &
                    NAME.EQ.NC4TO8("OT23") .OR. NAME.EQ.NC4TO8("OT24") .OR. &
                    NAME.EQ.NC4TO8("OT25") .OR. NAME.EQ.NC4TO8("OT26") .OR. &
                    NAME.EQ.NC4TO8("OT27") .OR. NAME.EQ.NC4TO8("OT28") .OR. &
                    NAME.EQ.NC4TO8("OT29") .OR. NAME.EQ.NC4TO8("OT30") .OR. &
                    NAME.EQ.NC4TO8("OT31") .OR. NAME.EQ.NC4TO8("OT32") .OR. &
                    NAME.EQ.NC4TO8("OT33") .OR. NAME.EQ.NC4TO8("OT34") .OR. &
                    NAME.EQ.NC4TO8("OT35") .OR. NAME.EQ.NC4TO8("OT36") .OR. &
                    NAME.EQ.NC4TO8("OT37") .OR. NAME.EQ.NC4TO8("OT38") .OR. &
                    NAME.EQ.NC4TO8("OT39") .OR. NAME.EQ.NC4TO8("OT40")) NPACK=1

                 IF(NAME.EQ.NC4TO8("OT41") .OR. NAME.EQ.NC4TO8("OT42") .OR. &
                    NAME.EQ.NC4TO8("OT43") .OR. NAME.EQ.NC4TO8("OT44") .OR. &
                    NAME.EQ.NC4TO8("OT45") .OR. NAME.EQ.NC4TO8("OT46") .OR. &
                    NAME.EQ.NC4TO8("OT47") .OR. NAME.EQ.NC4TO8("OT48") .OR. &
                    NAME.EQ.NC4TO8("OT49")) NPACK=1

                 IF(NAME.EQ.NC4TO8(" PCP") .OR. NAME.EQ.NC4TO8("  GT") .OR. &
                    NAME.EQ.NC4TO8(" SIC") .OR. NAME.EQ.NC4TO8(" SNO") .OR. &
                    NAME.EQ.NC4TO8(" HFS") .OR. NAME.EQ.NC4TO8(" QFS") .OR. &
                    NAME.EQ.NC4TO8(" UFS") .OR. NAME.EQ.NC4TO8("OUFS") .OR. &
                    NAME.EQ.NC4TO8(" VFS") .OR. NAME.EQ.NC4TO8("OVFS") .OR. &
                    NAME.EQ.NC4TO8(" BEG") .OR. NAME.EQ.NC4TO8("OBEG") .OR. &
                    NAME.EQ.NC4TO8(" BWG") .OR. NAME.EQ.NC4TO8("OBWG") .OR. &
                    NAME.EQ.NC4TO8("  FN") .OR. NAME.EQ.NC4TO8("  ZN")) NPACK=2

                 IF(NAME.EQ.NC4TO8("VOAE") .OR. NAME.EQ.NC4TO8("VBCE") .OR. &
                    NAME.EQ.NC4TO8("VASE") .OR. NAME.EQ.NC4TO8("VMDE") .OR. &
                    NAME.EQ.NC4TO8("VSSE") .OR. NAME.EQ.NC4TO8("VOAW") .OR. &
                    NAME.EQ.NC4TO8("VBCW") .OR. NAME.EQ.NC4TO8("VASW") .OR. &
                    NAME.EQ.NC4TO8("VMDW") .OR. NAME.EQ.NC4TO8("VSSW") .OR. &
                    NAME.EQ.NC4TO8("VOAD") .OR. NAME.EQ.NC4TO8("VBCD") .OR. &
                    NAME.EQ.NC4TO8("VASD") .OR. NAME.EQ.NC4TO8("VMDD") .OR. &
                    NAME.EQ.NC4TO8("VSSD") .OR. NAME.EQ.NC4TO8("VOAG") .OR. &
                    NAME.EQ.NC4TO8("VBCG") .OR. NAME.EQ.NC4TO8("VASG") .OR. &
                    NAME.EQ.NC4TO8("VMDG") .OR. NAME.EQ.NC4TO8("VSSG") .OR. &
                    NAME.EQ.NC4TO8("VOAC") .OR. NAME.EQ.NC4TO8("VBCC") .OR. &
                    NAME.EQ.NC4TO8("VASC") .OR. NAME.EQ.NC4TO8("VMDC") .OR. &
                    NAME.EQ.NC4TO8("VSSC") .OR. NAME.EQ.NC4TO8("VASI") .OR. &
                    NAME.EQ.NC4TO8("VAS1") .OR. NAME.EQ.NC4TO8("VAS2") .OR. &
                    NAME.EQ.NC4TO8("VAS3")) NPACK=1

                 ! "UNPACK" THE FIELD...
                 !CALL RECUP2(G,IBUF)
                 ! NCS 2020: making the hard assumption that packing density is 0
                 ! i.e. unpacked. This is the case now, but might fail otherwise!
                 G(:)=0
                 do i=1, cccdata_instance%np
                    G(i) = cccdata_instance%data(i, kloop, jloop)
                 end do

                 ! CONVERT FROM MODEL INTERNAL FORMAT TO "REGULAR" GRID.
                 ! NOTE THAT NO GRID CONVERSION IS DONE IF IBUF(7).NE.0 SINCE THIS
                 ! SPECIAL FLAG IS SET ONLY FOR S/L FIELDS OR RCM.
                 ! RESET IBUF(7) TO ZERO IF S/L FIELD SO THAT DIAGNOSTICS WILL WORK
                 ! (FOR INSTANCE, "CMPLBL" OF TRANSFORMED TEMPERATURE FROM SPECTRAL
                 ! VERSUS S/L MOISTURE). THIS IS NOT DONE FOR THE RCM CASES.

                 IF(IBUF(7).EQ.0) THEN
                    CALL CGTORG (F,G,IBUF(5),IBUF(6))
                 ELSE
                    DO I=1,IBUF(5)*IBUF(6)
                       F(I)=G(I)
                    ENDDO
                    IF(IBUF(7).EQ.10) IBUF(7)=0
                 ENDIF
                 IBUF(8)=NPACK
              endif

              ! PRESERVE THE LABEL OF THE FIRST MATCHING RECORD
              ! TO BE DISPLAYED AT THE END TO THE STANDARD OUTPUT
              ! FILE.
              IF(NREC(N).EQ.1 ) THEN
                 DO I=1,8
                    IBUFS(I,N)=IBUF(I)
                 ENDDO
              ENDIF

              ! Write to CCC file
              CALL PUTFLD2 (NPAK(N),F(1:cccdata_instance%np),IBUF,MAXX)

              ! update the cccdata object with repacked data
              cccdata_instance%data(:,kloop,jloop) = F(1:cccdata_instance%np)
          end do ! k-loop (levels)    
       end do ! j-loop (time)
       cccdata(iloop) = cccdata_instance   ! update the array
    end do ! i-loop (variables)
    write(*,*) 'Completed repacking'
    call timer_stop('pakgcm')
    
  end subroutine pakgcm_inline
!============================================================================
end module rebuild
