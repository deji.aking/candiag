      PROGRAM ZXPLOT
C     PROGRAM ZXPLOT (ZXIN,      ZXV,      ZXW,      ZXIN2,      INPUT,         A2
C    1                                                           OUTPUT,)       A2
C    2          TAPE1=ZXIN,TAPE2=ZXV,TAPE13=ZXW,TAPE4=ZXIN2,TAPE5=INPUT,
C    3                                                     TAPE6=OUTPUT)
C     ------------------------------------------------------------------        A2
C
C     AUG 23/07 - B.MIVILLE (ADDED COLOR LEGEND,FIX FONT AND LABELS FORMAT,     A2
C                             FIX MANY INTERNAL BUGS)                           A2
C     SEP 18/06 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)                   
C     MAY 13/04 - F.MAJAESS (ENSURE "ITYPE" CHECK IS DONE AS INTEGER)
C     MAR 11/04 - M.BERKLEY (MODIFIED for NCAR V3.2)
C     JUL 27/00 - F.MAJAESS (Added "EXTERNAL" block.)                           
C     NOV 01/96 - F.MAJAESS (FIX FRAME/AXES DRAWING WITH NO-ZERO CONTOUR LINE)
C     DEC 31/97 - R. TAYLOR (CLEAN UP GGPLOT UPGRADE, FIX BUGS,TEST,TEST,TEST)
C     AUG 31/96 - T.GUI (UPDATE TO NCAR GRAPHICS V3.2)
C     AUG 13/96 - F.MAJAESS (ENSURE THE INPUT FIELD IS ONE DIMENSIONAL AND,
C                            IN THE GAUSSIAN LATITUDE CASE,"ILATH" IS EVEN)
C     JUL 09/96 - TAO,FM (ENABLE SKIPPING "SPVAL" VALUES IN HAFTNP)
C     MAY 31/96 - W.G. LEE  (FIX FLAG INITIALIZATION AND HAFTNP CALLS)
C     MAY 07/96 - F.MAJAESS (IMPLEMENT OVERRIDING X_AXIS DEFAULT LABELING       
C                            AND REVISE DFCLRS CALL)                            
C     APR 03/96 - W.G. LEE  (REVISE TO SKIP OVER MISSING DATA "SPVAL=1.E38")
C     MAR 11/96 - F.MAJAESS (ISSUE WARNING WHEN PACKING DENSITY <= 0)
C     OCT  4/95 - M. BERKLEY (Added options for GRID BOX shading and for
C                         an ocean depth vertical axis [KAX=2])
C     JAN 12/94 - D. RAMSDEN (label log(p) correctly when p<1 )                 
C     JAN 12/94 - F.MAJAESS (CORRECT "SKIP" OPTION )                            
C     SEP 21/93 - M. BERKLEY (changed Holleriths to strings)                    
C     SEP 10/93 - M. BERKLEY (changed ZXPLOT to save data files instead of      
C                             generating meta code.)                           
C     SEP 10/93 - M. BERKLEY (changed MOD to MODEZX - MOD is an intrinsic)    
C     AUG 20/93 - F.MAJAESS (corrected the plotting of vector fields)        
C     JAN 14/93 - E. CHAN  (Decode levels in 8-work label)
C     OCT 19/92 - T. JANG  (changed unit 3 to unit 13 due to possible conflict
C                           with "gmrta" file)
C     SEP  1/92 - T. JANG  (add format to write to unit 44)
C     JUL 20/92 - T. JANG  (changed variable "MAX" to "MAXX" so not to
C                           conflict with the generic function "MAX")
C     JUL 24/91 - S.WILSON (CHANGE FOR NEW COLOUR PALETTES AND ADD
C                           BLACK/WHITE CONOUR LINE OPTION)
C     DEC 12/90 - S.WILSON (CONVERT TO USE COLOUR VERSION OF HAFTON)
C     OCT 23/90 - S.WILSON (MAKE LABELS BIGGER & NON-OVERLAPPING.  ADD OPTION
C                           FOR LABELLING CONTOUR LINES.)
C     JAN 16/90 - F.MAJAESS (ALLOW PROCESSING OF DATA ON LAT-LON GRID)
C     SEP 01/89 - H.REYNOLDS (UPDATE TO NCAR GRAPHICS V2.0)
C     JAN 19/88 - F.MAJAESS (IMPLEMENT ALLOWING VARIOUS PLOT FRAMES ACCORDING
C                            TO "OVRLY" SWITCH WHICH CAN BE READ FROM THE
C                            FIRST INPUT CARD. ALSO, ALLOW FINE AND COARSE
C                            GRID INTERPOLATION).
C     DEC 22/87 - F.MAJAESS (CORRECT THE PROGRAM SO THAT DASHED CONTOUR
C                            LINES APPEAR FOR NEGATIVE VALUES IF POSSIBLE).
C     SEP 11/87 - M.SUTCLIFFE (ADD SHADING, SECOND SCALAR FIELD,
C                              AND SUBAREA PLOT OPTIONS)
C     MAY 02/86 - F.ZWIERS (TAKE AWAY UNWANTED TICK MARKS)
C     APR 24/85 - B.DUGAS. (ADD PUBLICATION QUALITY OPTION)
C     NOV 14/84  - M.LAZARE (ADD VECTOR PLOT)
C     MAR 31/81 - J.D.HENDERSON
C                                                                               A2
CZXPLOT  - CREATES NCAR PLOT VECTORS FOR ONE ZONAL CROSS-SECTION        4  1 C  A1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON                                                        A3
C                                                                               A3
CPURPOSE - PLOTS CROSS-SECTIONS FROM FILE ZXIN, AND POSSIBLY ZXIN2,             A3
C          AND/OR ZONALLY AVERAGED VECTOR COMPONENTS ZXV AND ZXW.               A3
C          NOTE - CROSS-SECTIONS CAN BE GLOBAL OR NORTHERN HEMISPHERE.          A3
C                 ONE LEVEL CROSS-SECTIONS ARE ILLEGAL - PROGRAM STOPS.         A3
C                 2 OR 3 LEVEL PLOTS USE LINEAR INTERPOLATION.                  A3
C                 4 OR MORE LEVELS ALLOWS OPTION OF CUBIC INTERPOLATION.        A3
C                 IF TWO SCALAR FIELDS ARE PLOTTED, THE SECOND FIELD WILL       A3
C                 BE  CONTOURED WITH A DASHED LINE, AND A '+' WILL MARK         A3
C                 THE HIGHS AND LOWS. THE PLOT EXTENDS FROM PR(NLEV) TO         A3
C                 PR(1) AT THE TOP, OR FROM DPR1 TO DPR2 IF PLOTTING A          A3
C                 SUBAREA, (SEE DOCUMENTATION BELOW). THE VERTICAL AXIS         A3
C                 CAN BE PRESSURE OR LOG(PRESSURE).                             A3
C                 MAXIMUM INPUT LEVELS IS 20 AND MAXIMUM LATITUDES IS 200.      A3
C                                                                               A3
CINPUT FILES...                                                                 A3
C                                                                               A3
C      ZXIN  = FILE CONTAINING CROSS-SECTIONS TO BE PLOTTED.                    A3
C      ZXV   = MERIDIONNAL COMPONENT OF VECTOR FIELD.                           A3
C      ZXW   = VERTICAL COMPONENT OF VECTOR FIELD.                              A3
C      ZXIN2 = OPTIONAL FILE CONTAINING A SECOND CROSS-SECTIONS SCALAR          A3
C              FIELD TO BE PLOTTED.                                             A3
C                                                                               A3
C                                                                               
CCARDS READ...                                                                  A5
C                                                                               A5
C      2 TO 11 CARDS ARE READ-IN.                                               A5
C                                                                               A5
C      FOR CROSS-SECTION MAP...                                                 A5
C      =====================                                                    A5
C      CARD 1-                                                                  A5
C      ------                                                                   A5
C      READ(5,5010)NAME,MODEZX,ICOSH1,ICOSH2,MS,LLAB,LX,SCAL,                   A5
C     1            FLO,HI,FINC,KAX,IREV,IOVRLY,KIND                             A5
C 5010 FORMAT(10X,1X,A4,I5,2I1,I3,I2,I3,4E10.0,I5,I1,2I2)                       A5
C                                                                               A5
C      NAME       NAME OF THE VARIABLE TO BE CONTOURED.                         A5
C      ABS(MODEZX)=0, ONLY CONTOUR MAP (2 CARDS PER PLOT).                      A5
C                 =1, ONLY VECTOR PLOT (CARD 1 USED ONLY TO DEFINE MODEZX,      A5
C                     AND LX, CARD 2 IS IGNORED, AND CARDS 3 AND 4 USED         A5
C                     FOR VECTOR PLOT DIRECTIVES).                              A5
C                 =2, BOTH FUNCTIONS ARE PERFORMED (4 CARDS PER PLOT).          A5
C                 =3, ONLY CONTOUR MAP (HIGHS/LOWS LABELLED).                   A5
C                 =4, TWO SCALAR FIELDS.                                        A5
C                 =5, TWO SCALAR FIELDS AND A VECTOR FIELD.                     A5
C      MODEZX.GE.0   HIGHS/LOWS NOT LABELLED                                    A5
C          LT.0   HIGHS/LOWS     LABELLED                                       A5
C      ICOSH1     CONTOUR/SHADING CONTROL FOR FIRST  SCALAR FIELD               A5
C      ICOSH2     CONTOUR/SHADING CONTROL FOR SECOND SCALAR FIELD               A5
C                 =0, CONTOUR LINES                                             A5
C                 =1, CONTOUR LINES, NO ZERO LINE                               A5
C                 =2, CONTOUR LINES AND SHADING                                 A5
C                 =3, CONTOUR LINE AND SHADING, NO ZERO LINE                    A5
C                 =4, SHADING ONLY                                              A5
C                 =5, CONTOURS & GRID BOX SHADING,                              A5
C                 =6, CONTOURS & GRID BOX SHADING,NO ZERO LINE                  A5
C                 =7, CONTOURS & GRID BOX SHADING,NO ZERO LINE,BOX SIDES DRAWN  A5
C                 =8, GRID BOX SHADING ONLY                                     A5
C                 =9, GRID BOX SHADING WITH BOX SIDES DRAWN                     A5
C                                                                               A5
C                 NOTE : FOR THE NO ZERO LINE OPTION IT IS ASSUMED THAT         A5
C                        (0.-FLO)/FINC RESULTS IN AN INTEGER DIVISIBLE BY       A5
C                        TWO, I.E. THE ZERO LINE WOULD HAVE BEEN CONTOURED      A5
C                        AS A THICK, LABELLED LINE. ALSO, NEITHER HI OR         A5
C                        FLO = 0.                                               A5
C      MS         PUBLICATION QUALITY OPTIONS:                                  A5
C                                                                               A5
C                 IF MS.LT.0, THEN PUBLICATION QUALITY OPTION IS SET, AND       A5
C                 PLOTS HAVE: TITLE, THICK LINES, NO INFORMATION LABEL, NO      A5
C                 LEGEND, AND ALL CONTOUR LINES ARE SAME WIDTH.  LHI IS SET     A5
C                 TO -1, SO NO HIGH/LOW LABELS ARE DISPLAYED, AND MAP IS        A5
C                 CENTRED ON LAT 0 FOR GLOBAL FIELD.                            A5
C                 IF MS.GE.0, THEN NORMAL QUALITY OPTION IS SET, AND PLOTS      A5
C                 HAVE THICK/THIN LINES, AND HIGH/LOW LABELS (DEPENDING UPON    A5
C                 LHI SETTING.  IN ADDITION, IF MS                              A5
C                  =8,  PUBLICATION QUALITY (LIKE MS<0) BUT WITH LEGEND         A5
C                  =7,  NO INFO LABEL (2) BUT WITH LEGEND                       A5
C                  =6,  NO TITLE (1) BUT WITH LEGEND                            A5
C                  =5,  NORMAL (0) WITH LEGEND                                  A5
C                  =4,  NO INFO LABEL, NO TITLE, NO LEGEND (BACK COMPATIBLE)    A5
C                  =3,  NO INFO LABEL, NO TITLE, NO LEGEND                      A5
C                  =2,  NO INFO LABEL, NO LEGEND                                A5
C                  =1,  NO TITLE, NO LEGEND                                     A5
C                  =0   NORMAL INFO LABEL AND TITLE, NO LEGEND                  A5
C                                                                               A5
C                                                                               A5
C      LLAB       =0, CONTOUR LINES ARE LABELLED.                               A5
C                 OTHERWISE, CONTOUR LINES ARE NOT LABELLED.                    A5
C      ABS(LX)    <= 3 FINE GRID INTERPOLATION IS INDICATED WITH THE            A5
C                      NUMBER OF LEVELS IN THE DISPLAY PLOT = 2*NLAT/3.         A5
C                      (NLAT IS THE NUMBER OF LATIDUNAL POINTS).                A5
C                 OTHERWISE, A COARSE GRID INTERPOLATION IS INDICATED           A5
C                            WITH ABS(LX) LEVELS IN THE DISPLAY PLOT.           A5
C                 LX < 0, SUBAREA OR OVERRIDING THE STANDARD LATITUDE X-AXIS    A5
C                         LABELING IS REQUESTED (SEE "IXFLG" AND "KIND"         A5
C                         SWITCHES BELOW), IN WHICH CASE THE REQUIRED           A5
C                         PARAMETERS ARE READ FROM CARD 3.                      A5
C      SCAL       SCALING FACTOR                                                A5
C      FLO        LOWEST  VALUE TO BE CONTOURED                                 A5
C      HI         HIGHEST VALUE TO BE CONTOURED                                 A5
C      FINC       => 0 CONTOUR INTERVAL (A MAX. OF 40 CONTOURS IS PERMITTED)    A5
C                 <  0 WILL DO ARBITRARY CONTOUR LINES (SCALE .NE. 0) AND       A5
C                      ABS(FINC) WILL EQUAL THE NUMBER OF CONTOURS. AN EXTRA    A5
C                      INPUT CARD WILL BE READ AFTER THE SHADING CONTOUR VALUES A5
C                      IF PRESENT.                                              A5
C      KAX        =0, FOR VERTICAL AXIS OF PRESSURE.                            A5
C                 =1, FOR VERTICAL AXIS OF LOG(PRESSURE).                       A5
C                 =2, FOR VERTICAL AXIS OF OCEAN DEPTH.                         A5
C      IREV       = 1,REVERSED DATA AND X AXIS LABELS OTHERWISE NO CHANGE.      A5
C                     IGNORED IF IXFLG.NE.0.                                    A5
C      OVRLY      THIS FIELD IS IGNORED IF LX < 0; (SUBAREA INDICATED).         A5
C                 =-1,PLOTTED AREA IS DEFINED BY THE FILE RANGE AND PLOTTED     A5
C                     ON THE STANDARD LEVEL (1000,10) AND STANDARD LATITUDE     A5
C                     (90,-90) SCALES. SCREEN IS FILLED.                        A5
C                 = 0,PLOTTED AREA IS DEFINED BY THE FILE RANGE AND PLOTTED     A5
C                     ON THE STANDARD LATITUDE SCALE (90,-90) AND ON A LEVEL    A5
C                     SCALE WHOSE RANGE IS THAT OF THE FILE LEVELS RANGE.       A5
C                     SCREEN IS FILLED. USE THIS OPTION FOR OCEAN DATA.         A5
C                 = 1,PLOTTED AREA IS DEFINED BY THE FILE RANGE AND PLOTTED     A5
C                     ON THE STANDARD LEVEL (1000,10) AND STANDARD LATITUDE     A5
C                     (90,-90) SCALES. WHAT APPEARS ON THE SCREEN IS JUST A     A5
C                     PLOT HAVING THE SAME SIZE AS THAT SPANNED BY THE FILE     A5
C                     RANGES ON THE STANDARD PLOT SCALE. SCREEN MAY OR MAY      A5
C                     NOT BE FILLED.                                            A5
C      KIND       <  0 DATA IS ON A LAT-LON  GRID;                              A5
C                  &  <  -4 LATITUDINAL RANGE DOES NOT INCLUDE THE POLE(S)      A5
C                  &  >= -4 LATITUDINAL RANGE DOES     INCLUDE THE POLE(S),     A5
C                           (DEFAULT).                                          A5
C                 >= 0 DATA IS ON A GAUSSIAN GRID, (DEFAULT).                   A5
C                 ABS(KIND)=1, FOR LINEAR VERTICAL INTERPOLATION FROM DATA      A5
C                              TO DISPLAY,                                      A5
C                 OTHERWISE    CUBIC (DISABLED).                                A5
C                 (A LINE DEFAULTING KIND TO 1 IS INSERTED IN THE CODE JUST     A5
C                 AFTER KIND IS READ, THIS FORCES THE PROGRAM TO PERFORM        A5
C                 LINEAR VERTICAL INTERPOLATION ONLY. THE USER MUST MODIFY      A5
C                 THAT LINE TO SPECIFY A VALUE OF 3 IF HE/SHE WISHES TO         A5
C                 APPLY CUBIC VERTICAL INTERPOLATION).                          A5
C                                                                               A5
C                                                                               A5
C      CARD 2-                                                                  A5
C      ------                                                                   A5
C      READ(5,5012) (LABEL(I),I=1,80)                                           A5
C 5012 FORMAT(80A1)                                                             A5
C                                                                               A5
C      LABEL      80 CHARACTER LABEL FOR CONTOUR MAP.                           A5
C                 ABSENT IF NAME=NC4TO8('SKIP').                                A5
C                                                                               A5
C      X-AXIS LABELING/SUBAREA SPECIFICATION CARD                               A5
C      ==========================================                               A5
C      CARD 3-                                                                  A5
C      -------                                                                  A5
C                                                                               A5
C      READ(5,5018) DLAT1,DLAT2,DPR1,DPR2,IFLAG,IXFLG                           A5
C 5018 FORMAT(10X,4E10.0,2I5)                                                   A5
C                                                                               A5
C      DLAT1      LOWER X-AXIS LABEL IF "IXFLG" .NE. 0                          A5
C                 LEFT HAND LATITUDE ( OF SUBAREA IF "IXFLG"=0, IT MUST BE IN   A5
C                 THE RANGE +90 TO -90 FOR GLOBAL FIELDS KHEM=0,                A5
C                           +90 TO   0 FOR NORTHERN HEMISPHERE FIELDS, KHEM=1,  A5
C                             0 TO -90 FOR SOUTHERN HEMISPHERE FIELDS, KHEM=2). A5
C      DLAT2      UPPER X-AXIS LABEL IF "IXFLG" .NE. 0                          A5
C                 RIGHT HAND LATITUDE ( OF SUBAREA IF "IXFLG"=0).               A5
C                 IMPORTANT : IF "IXFLG" .EQ. 0 THEN:                           A5
C                             DLAT1 > DLAT2  ELSE  DLAT1 < DLAT2                A5
C      DPR1       TOP PRESSURE LEVEL OF SUBAREA (LOWEST PRESSURE).              A5
C                 (IGNORED IF "IXFLG" .NE. 0)                                   A5
C      DPR2       BOTTOM PRESSURE LEVEL OF SUBAREA (HIGHEST PRESSURE)           A5
C                 (IGNORED IF "IXFLG" .NE. 0)                                   A5
C      IFLAG      IGNORED IF "IXFLG" .NE. 0;                                    A5
C                 =0,1,SUBAREA IS PLOTTED INSIDE A FRAME DEFINED                A5
C                      BY THE SUBAREA COORDINATES, ON STANDARD PLOT SCALE.      A5
C                      SCREEN IS FILLED.                                        A5
C                 = 2, SUBAREA IS PLOTTED INSIDE A FRAME DEFINED BY             A5
C                      THE SUBAREA COORDINATES, TO FILL THE SCREEN.             A5
C                 = 3, THE SUBAREA IS PLOTTED INSIDE A FRAME DEFINED BY         A5
C                      THE SUBAREA COORDINATES, THE PLOT LOOKS EXACTLY          A5
C                      THE SAME AS IF IT WAS PLOTTED ON THE STANDARD PLOT       A5
C                      SCALE. SCREEN MAY OR MAY NOT BE FILLED.                  A5
C      IXFLG      THIS FLAG IS VALID ONLY WITH DATA ON LAT/LON GRID ("KIND" < 0)A5
C                 IT'S USED TO OVERRIDE THE STANDARD X-AXIS LATITUDE LABELING   A5
C                 BY READING DLAT1/DLAT2                                        A5
C                 =0,      STANDARD X-AXIS LATITUDE LABELING OR SUBAREA, WORKS  A5
C                          ON LAT/LON AND GAUSSIAN GRIDS. (DEFAULT)             A5
C                          USE KHEM TO KNOW WHERE THE INPUT DATA IS AND USE     A5
C                          DLAT1/DLAT2 AND DPR1/DPR2 FOR EXTRACTING THE SUBAREA A5
C                          AND LABELLING THE X AND Y AXIS.                      A5
C                 ELSE, NO-STANDARD X-AXIS LATITUDE LABELING,                   A5
C                          DLAT1/DLAT2 REFERS TO THE LOWER/UPPER X-AXIS LABELS. A5
C                          ONLY WORKS ON LAT/LON GRID. EXITS IF KIND >= 0 .     A5
C                          THIS LABELS THE SOUTHERN MOST LATITUDE ON THE LEFT   A5
C                          SIDE OF THE GRAPHICS. SO INPUT DATA HAVE TO BE IN    A5
C                          THE SAME ORIENTATION. TYPICAL MODEL OUTPUT FILE WILL A5
C                          WILL BE PLOTTED WITH  THE NORTHERN MOST DATA ON THE  A5
C                          LEFT SIDE OF THE GRAPHICS UNLESS DATA IS REVERSED.   A5
C                                                                               A5
C      SHADING CONTROL CARDS FOR FIRST SCALAR FIELD                             A5
C      ============================================                             A5
C      CARD 4-                                                                  A5
C      -------                                                                  A5
C      IF NPAT <= 7, CARD 4 will read one line.                                 A5
C                                                                               A5
C      READ(5,5014) WHITFG,NPAT,(IPAT(I),I=1,7)                                 A5
C 5014 FORMAT(10X,I1,I4,7I5)                                                    A5
C                                                                               A5
C      WHITFG     .NE. 0 IF COLOUR PLOTS ARE TO HAVE WHITE CONTOURS             A5
C      NPAT       NUMBER OF DIFFERENT RANGES TO SHADE                           A5
C      IPAT(NPAT) NPAT SHADING PATTERN CODES                                    A5
C                                                                               A5
C      IF 8 <= NPAT <= 14,  CARD 4 will read two lines. (Etc. up to 28 colors)  A5
C                                                                               A5
C      READ(5,5014) WHITFG, NPAT, (IPAT(I),I=1,7)                               A5
C 5014 FORMAT(10X,I1,I4,7I5)                                                    A5
C      READ(5,5015) (IPAT(I),I=8,NPAT)                                          A5
C 5015 FORMAT(15X,7I5)                                                          A5
C                                                                               A5
C      CARD 5-                                                                  A5
C      -------                                                                  A5
C      IF NPAT <= 7, CARD 5 will read one line.                                 A5
C      IF ZLEV(NPAT-1) = 1.0E+38 THEN IF SHADING WAS CHOOSEN IT WILL SHADE THAT A5
C         VALUE WITH THE COLOR IPAT(NPAT). VALUE WILL NOT SHOW IN LEGEND        A5
C                                                                               A5
C      READ(5,5016) (ZLEV(I),I=1,NPAT-1)                                        A5
C 5016 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      ZLEV(I) =    NPAT-1 DATA VALUES AT WHICH TO CHANGE SHADING PATTERNS.     A5
C                                                                               A5
C      IF 8 <= NPAT <= 14,  CARD 5 will read two lines. (Etc. up to 28 colors)  A5
C                                                                               A5
C      READ(5,5016) (ZLEV(I),I=1,7)                                             A5
C      READ(5,5016) (ZLEV(I),I=8,NPAT-1)                                        A5
C 5016 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      FOR ARBITRARY CONTOUR LINES AND SHADING CONTOUR LEVELS OF FIRST SCALAR   A5
C      FIELD                                                                    A5
C      ================================================                         A5
C                                                                               A5
C     IF FINC < 0                                                               A5
C                                                                               A5
C      NPATS=-FINC                                                              A5
C                                                                               A5
C      NOTE: SCAL CAN NOT BE EQUAL TO ZERO IF YOU ARE REQUESTIONG ARBITRARY     A5
C            LINE CONTOURS.                                                     A5
C                                                                               A5
C      CARD 6-                                                                  A5
C      -------                                                                  A5
C                                                                               A5
C      DO K=1,NPATS,7                                                           A5
C         READ(5,5016) (ZLEVS(I),I=K,K+6)                                       A5
C      ENDDO                                                                    A5
C                                                                               A5
C 5016 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      NPATS       EXACT NUMBER OF CONTOUR LINES (NPATS=-FINC)                  A5
C      ZLEVS(I) =  NPATS DATA VALUES OF THE CONTOUR LINES.                      A5
C                                                                               A5
C     ELSE                                                                      A5
C                                                                               A5
C      CARDS FOR SECOND SCALAR FIELD                                            A5
C      =============================                                            A5
C      CARD 6-                                                                  A5
C      ------                                                                   A5
C      READ(5,5011)NAME,II,JJ,KK,MS,N,SCAL,FLO,HI,FINC,NN,III,KIND              A5
C 5011 FORMAT(10X,1X,A4,I5,2I1,I3,I5,4E10.0,I5,1X,2I2)                          A5
C                                                                               A5
C      VARIABLES IN () ARE NOT USED BUT MERELY KEEP PLACE.                      A5
C      NAME       NAME OF THE VARIABLE TO BE CONTOURED.                         A5
C II - MODEZX        = 0,  HIGHS,LOWS NOT LABELLED.                             A5
C                 =-1,  HIGHS,LOWS     LABELLED.                                A5
C JJ - (ICOSH1)   NOT USED, READ FROM CARD 1.                                   A5
C KK - (ICOSH2)   NOT USED, READ FROM CARD 1.                                   A5
C      MS         USE SECOND SCALAR ZLEV TO PLOT LEGEND OR NOT                  A5
C                 0 NO LEGEND (DEFAULT) DEPENDING ON THE VALUE OF MS FOR THE    A5
C                            FIRST SCALAR  THERE MIGHT STILL BE A LEGEND        A5
C                 1 LEGEND                                                      A5
C N  - (LX)       NOT USED, READ FROM CARD 1.                                   A5
C      SCAL       SCALING FACTOR                                                A5
C      FLO        LOWEST  VALUE TO BE CONTOURED                                 A5
C      HI         HIGHEST VALUE TO BE CONTOURED                                 A5
C      FINC       CONTOUR INTERVAL (A MAX. OF 40 CONTOURS IS PERMITTED)         A5
C NN - (KAX)      NOT USED, READ FROM CARD 1.                                   A5
C III- (OVRLY)    NOT USED, READ FROM CARD 1.                                   A5
C      KIND       <  0 DATA IS ON A LAT-LON  GRID;                              A5
C                  &  <  -4 LATITUDINAL RANGE DOES NOT INCLUDE THE POLE(S)      A5
C                  &  >= -4 LATITUDINAL RANGE DOES     INCLUDE THE POLE(S),     A5
C                           (DEFAULT).                                          A5
C                 >= 0 DATA IS ON A GAUSSIAN GRID, (DEFAULT).                   A5
C                 ABS(KIND)=1, FOR LINEAR VERTICAL INTERPOLATION FROM DATA      A5
C                              TO DISPLAY,                                      A5
C                 OTHERWISE    CUBIC (DISABLED).                                A5
C                 (A LINE DEFAULTING KIND TO 1 IS INSERTED IN THE CODE JUST     A5
C                 AFTER KIND IS READ, THIS FORCES THE PROGRAM TO PERFORM        A5
C                 LINEAR VERTICAL INTERPOLATION ONLY. THE USER MUST MODIFY      A5
C                 THAT LINE TO SPECIFY A VALUE OF 3 IF HE/SHE WISHES TO         A5
C                 APPLY CUBIC VERTICAL INTERPOLATION).                          A5
C                                                                               A5
C                                                                               A5
C      CARD 7-                                                                  A5
C      ------                                                                   A5
C      READ(5,5012) (LABEL(I),I=1,80)                                           A5
C 5012 FORMAT(80A1)                                                             A5
C                                                                               A5
C      LABEL      80 CHARACTER LABEL.                                           A5
C                                                                               A5
C      SHADING CONTROL CARDS FOR SECOND SCALAR FIELD                            A5
C      ============================================                             A5
C      CARD 8-                                                                  A5
C      -------                                                                  A5
C      IF NPAT <= 7, CARD 8 will read one line.                                 A5
C                                                                               A5
C      READ(5,5014) WHITFG,NPAT,(IPAT(I),I=1,7)                                 A5
C 5014 FORMAT(10X,I1,I4,7I5)                                                    A5
C                                                                               A5
C      WHITFG     .NE. 0 IF COLOUR PLOTS ARE TO HAVE WHITE CONTOURS             A5
C      NPAT       NUMBER OF DIFFERENT RANGES TO SHADE                           A5
C      IPAT(NPAT) NPAT SHADING PATTERN CODES                                    A5
C                                                                               A5
C      IF 8 <= NPAT <= 14,  CARD 8 will read two lines. (Etc. up to 28 colors)  A5
C                                                                               A5
C      READ(5,5014) WHITFG, NPAT, (IPAT(I),I=1,7)                               A5
C 5014 FORMAT(10X,I1,I4,7I5)                                                    A5
C      READ(5,5015) (IPAT(I),I=8,NPAT)                                          A5
C 5015 FORMAT(15X,7I5)                                                          A5
C                                                                               A5
C      CARD 9-                                                                  A5
C      -------                                                                  A5
C      IF NPAT <= 7, CARD 9 will read one line.                                 A5
C                                                                               A5
C      READ(5,5016) (ZLEV(I),I=1,NPAT-1)                                        A5
C 5016 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      ZLEV(I) =    NPAT-1 DATA VALUES AT WHICH TO CHANGE SHADING PATTERNS.     A5
C                                                                               A5
C      IF 8 <= NPAT <= 14,  CARD 9 will read two lines. (Etc. up to 28 colors)  A5
C                                                                               A5
C      READ(5,5016) (ZLEV(I),I=1,7)                                             A5
C      READ(5,5016) (ZLEV(I),I=8,NPAT-1)                                        A5
C 5016 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C     IF FINC < 0                                                               A5
C                                                                               A5
C      NPATS=-FINC                                                              A5
C                                                                               A5
C      NOTE: SCAL CAN NOT BE EQUAL TO ZERO IF YOU ARE REQUESTIONG ARBITRARY     A5
C            LINE CONTOURS.                                                     A5
C                                                                               A5
C      CARD 10-                                                                 A5
C      -------                                                                  A5
C                                                                               A5
C      DO K=1,NPATS,7                                                           A5
C         READ(5,5016) (ZLEVS(I),I=K,K+6)                                       A5
C      ENDDO                                                                    A5
C                                                                               A5
C 5016 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      NPATS       EXACT NUMBER OF CONTOUR LINES (NPATS=-FINC)                  A5
C      ZLEVS(I) =  NPATS DATA VALUES OF THE CONTOUR LINES.                      A5
C                                                                               A5
C     ELSE                                                                      A5
C                                                                               A5
C      FOR VECTOR PLOT...                                                       A5
C      ===============                                                          A5
C      CARD 10-                                                                 A5
C      --------                                                                 A5
C  310 READ(5,5020) VSCAL,VLO,VI,INCY,INCZ                                      A5
C 5020 FORMAT(10X,3E10.0,2I5)                                                   A5
C                                                                               A5
C      VSCAL      SCALING FACTOR FOR VECTOR MAGNITUDE                           A5
C      VLO        LOWEST VECTOR MAGNITUDE TO BE PLOTTED                         A5
C      VI         VECTOR MAGNITUDE TO BE DRAWN AS ARROW OF LENGTH  DX           A5
C      INCY       EVERY INCY GRID POINT VECTOR IS PLOTTED ALONG Y AXIS          A5
C      INCZ       EVERY INCZ GRID POINT VECTOR IS PLOTTED ALONG Z AXIS          A5
C                                                                               A5
C      CARD 11-                                                                 A5
C      --------                                                                 A5
C      READ(5,5012) (LABEL(I),I=1,80)                                           A5
C 5012 FORMAT(80A1)                                                             A5
C                                                                               A5
C      LABEL      80 CHARACTER LABEL FOR VECTOR PLOT.                           A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C*  ZXPLOT TEMP    544 22   -1     1.E-1      1.E1      4.E1      1.E0  0 0     A5
C* JUNE 12-1       FIRST SCALAR FIELD TEMPERATURE CROSS-SECTION (DEG K)/10.     A5
C*               90.      -90.       10.     1000.    0                         A5
C*            7   52    0   48    0   48    0   52                              A5
C*                0.       10.       15.       20.       25.       30.          A5
C*  ZXPLOT TEMP    0   22   -1     1.E-1      1.E1      4.E1      1.E0  0 0     A5
C* JUNE 12-1       SECOND SCALAR FIELD TEMPERATURE CROSS-SECTION (DEG K)/10.    A5
C*            7   16    0   15    0    9    0   63                              A5
C*                0.       10.       15.       20.       25.       30.          A5
C*  ZXPLOT        1.     1.E-1      5.E0    2    2                              A5
C*   MERIDIONAL MASS STREAM FUNCTION VECTOR PLOT                                A5
C*                                                                              A5
C* AND FOR OVERRIDING X-AXIS LABELS:                                            A5
C*                                                                              A5
C*  ZXPLOT NEXT         0   -1        1.      -60.       60.       0.5    0   -1A5
C* LABEL LOWER/UPPER X-AXIS WITH 1 AND 32; LOG10  SPECTRUM VS WAVENUMBER N      A5
C*                1.       32.        0.        0.    0    1                    A5
C-----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_MAXBLONP1BLAT,
     &                       SIZES_MAXLEV,
     &                       SIZES_NWORDIO
      integer, parameter :: 
     & MAXX = SIZES_MAXLEV*max(SIZES_BLONP1,SIZES_BLAT)*SIZES_NWORDIO

      PARAMETER (LXPNLTP=SIZES_MAXBLONP1BLAT*SIZES_MAXBLONP1BLAT)
      LOGICAL OK,REW,SECOND,SUBAREA,LOVRLY,LLFLD,SHFT,LEAD,TRAIL
      LOGICAL LGND,CONT1,SHAD1,BOXSHD1,BOXDRW1,CLRPLT,PATPLT,NZERO1
      LOGICAL CONT2,SHAD2,BOXSHD2,BOXDRW2,NZERO2
      LOGICAL CONT,BOXSHD,BOXDRW,NZERO
      CHARACTER*6 IPNT
      CHARACTER*7 IPNT2
      CHARACTER*1 IPRT(7)
      INTEGER LEV(SIZES_MAXLEV), WHITFG
      CHARACTER*1 LABEL(83), XLABEL(83)
      CHARACTER*83 ALABEL, AXLABEL
      INTEGER MAJLW,MINLW,LOWMKR,HIMKR,INZERO 
      INTEGER IPAT(28), ITSIZ, ISIZ, IHGT
      INTEGER ISHADSPVAL,IREV
      REAL PRX(SIZES_MAXBLONP1BLAT),VX(SIZES_MAXBLONP1BLAT),
     &     WX(SIZES_MAXBLONP1BLAT),YX(SIZES_MAXBLONP1BLAT)
      REAL DEN(4,17),PR(SIZES_MAXLEV),V(SIZES_MAXLEV),
     &     W(SIZES_MAXLEV),Y(SIZES_MAXLEV)
      REAL ZLEV(28),SPV(2),OLDY
      REAL SLATP(SIZES_MAXBLONP1BLAT),SLAT(SIZES_MAXBLONP1BLAT)
      REAL HSVV(3, 16)
      REAL CHRATIO
      REAL SPVAL
      CHARACTER*10 LI1,LI2,LI3
      CHARACTER*15 LI4
      REAL PT
      REAL*8 SL,CL,WL,WOSSL,RAD
      REAL*8 ATEMP,RADADEG 
      EQUIVALENCE(LABEL,ALABEL),(IPRT, IPNT2),(XLABEL,AXLABEL)
      CHARACTER*140 INFOLABEL
C     * LEGEND BAR
      CHARACTER*10 LLBS(28),LLBST
      DIMENSION LIND(28)
      DATA LIND / 2,3,4,5,6,7,8,9,10,11,12,13,14,15,
     1           16,17,18,19,20,21,22,23,24,25,26,27,28,29/
C     * Quality options
      INTEGER MS
      LOGICAL PUB, DOTITLE, DOINFOLABEL, DOLGND, DOLABEL, SHAD
C     * Publication common block
      COMMON /PPPP/ PUB, SHAD, DOTITLE, DOINFOLABEL, DOLGND, DOLABEL

      INTEGER LUNCARD,UNIQNUM,LUNDATAFILES(4)
      PARAMETER (LUNOFFSET=10,
     1     LUNFIELD1=LUNOFFSET+1,LUNFIELD2=LUNOFFSET+2,
     2     LUNFIELD3=LUNOFFSET+3,LUNFIELD4=LUNOFFSET+4)

C     * Workspace arrays for NCARG routines.
      INTEGER LRWK, LIWK
      PARAMETER (LRWK=15000, LIWK=7000)
      REAL RWRK(LRWK)
      INTEGER IWRK(LIWK)
C     * Arbitrary contours lines
      INTEGER NIPATS,NPATS,IARBI
      PARAMETER(NIPATS = 28)
      REAL ZLEVS(NIPATS), CWM
      COMMON /ARBI/ ZLEVS,NPATS,IARBI, CWM
C     * COLour CONtour plotting routine.
      INTEGER ZX_MAP_SIZE
      PARAMETER (ZX_MAP_SIZE=2000000)
      INTEGER ZX_MAP(ZX_MAP_SIZE)
      COMMON /ZX_AREA/ZX_MAP
      INTEGER NWRK, NOGRPS
      PARAMETER (NWRK=15000,NOGRPS=5)
      REAL XWRK(NWRK), YWRK(NWRK)
      INTEGER IAREA(NOGRPS), IGRP(NOGRPS)

      COMMON/ICOM/IBUF(8),IDAT(MAXX)
      COMMON/JCOM/JBUF(8),JDAT(MAXX)
C     * DEBUG
      CHARACTER*15 DPAT,LLT
      REAL DPV,DPS
      INTEGER IDPU,LLP
C     * DEBUG
      CHARACTER*4 ATYPE, CZXPL
      INTEGER*4 ITYPE, IZXPL
      CHARACTER*8 ALABLAT
      INTEGER XLAB

      CHARACTER*20 ALABY
      CHARACTER*1 NORTH,SOUTH
      CHARACTER*2 EQ
C     * ITYPE is used by all plotting programs.
      COMMON /CCCPLOT/ ITYPE
      REAL SABR(28)
      COMMON /RGB/ SABR
      COMMON /FILLCB/ IPAT, ZLEV, NPAT, MAXPAT, MAXCLRS
      COMMON /CONRE4/ ISIZEL     ,ISIZEM     ,ISIZEP     ,NREP       ,
     1                NCRT       ,ILAB       ,NULBLL     ,IOFFD      ,
     2                EXT        ,IOFFM      ,ISOLID     ,NLA        ,
     3                NLM        ,XLT        ,YBT        ,SIDE

      COMMON/GAUS/SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT)
      COMMON/GAUS/WOSSL(SIZES_BLAT),RAD(SIZES_BLAT)
C     COMMON /BLANCK/ XS($XBIBJ$,$L$),FXS($LXBIBJ$),F($LXBIBJ$),
C    1                ZXV($LXBIBJ$),ZXW($LXBIBJ$)
C     COMMON /WINDS/ VS($XBIBJ$,$L$),WS($XBIBJ$,$L$),VXS($LXBIBJ$),WXS($LXBIBJ$)
      COMMON /BLANCK/ XS(SIZES_MAXBLONP1BLAT,SIZES_MAXLEV),FXS(LXPNLTP),
     &                F(LXPNLTP),ZXV(LXPNLTP),ZXW(LXPNLTP)
      COMMON /WINDS/ VS(SIZES_MAXBLONP1BLAT,SIZES_MAXLEV),
     &               WS(SIZES_MAXBLONP1BLAT,SIZES_MAXLEV),
     &               VXS(LXPNLTP),WXS(LXPNLTP)
      COMMON /CONCCC/MAJLW,MINLW,LOWMKR,HIMKR,INZERO
C--------------------------------------------------------------------
C     DASHLINE Common Block
      COMMON /INTPR/
     1     IPAU,FPART,TENSN,NP,SMALL,L1,ADDLR,ADDTB,MLLINE,ICLOSE
      INTEGER IPAU,NP,L1,MLLINE,ICLOSE
      REAL FPART,TENSN,SMALL,ADDLR,ADDTB
C--------------------------------------------------------------------
      COMMON /PUSER/ MODE
      COMMON /FILLCO/ LAN,LSP,LPA,LCH,LDP(8,8)


      EQUIVALENCE (CZXPL,IZXPL)

C     * DATA PERTAINING TO CHARACTER SIZES FOR PCHIQU.
C     * NOTE:  THE FONT PCHIQU USES IS DIGITIZED TO BE 32 PLOTTER
C     * COORDINATE UNITS (PLU) HIGH - THIS INCLUDES SPACE ABOVE THE
C     * CHARACTER.  THE ACTUAL CHARACTER HEIGHT IS 21 PLU.  THEREFORE,
C     * THE ACTUAL HEGHT FOR A GIVEN CHARACTER SIZE CAN BE DETERMINED
C     * BY MULTIPLYING BY THE RATIO 32/21 = 1.52381
      DATA ITSIZ /18/, ISIZ /14/, CHRATIO /1.52381/

      DATA SPV/0.0,0.0/
      DATA FSCAL/0.87/
      DATA REW/.TRUE./,LEAD/.TRUE./,TRAIL/.TRUE./

C
C     * DECLARE NCAR PLUGINS EXTERNAL, SO CORRECT VERSION  THE LOADER
C     * USES THE REVISED NCAR ROUTINES INSTEAD OF THE DEFAULT ONES...

      EXTERNAL AGAXIS,AGCHCU,AGCHNL,AGPWRT,ASIGNA,CFVLD,CONTDF,CPCHHL,
     1         CPCHIL,CPDRPL,CONBD,CONBND,CONBSP,CONREC,CURVED,CUTUP,
     2         CYLEQ,DASHBD,DASHDB,DASHDC,DFCLRS,DFNCLR,DRAWPV,DRLINE,
     3         DRWVEC,FDVDLD,FILLBD,FRSTD,GGP_COLSHD,GGP_COLSMSHD,
     4         GGP_CONLS,GGP_ILDEF,GGP_LEGEND,GGP_PATTERN,GRIDAL,
     5         HAFLVS,HAFTNP,HAFTON,HOV_LEGEND,HSVRGB,INTMSK,KURV1S,
     6         KURV2S,LASTD,LBFILL,LINEB,LINED,MAKLAB,MAPIT,MAPVP,
     7         MARKL,MINMAX,NEW_HAFTNP,PATTERN,PERIM,PERIML,PLOTLGD,
     8         POINTN,POLSTR,PSTART,PWRTM,PXIT,REMOVE,REORD,RESET,
     9         SAMPLE,SETPAT,SFBLDA,SFGETC,SFGETI,SFGETP,SFGETR,SFNORM,
     A         SFSETC,SFSETI,SFSETP,SFSETR,SFSGFA,SFSORT,SFWRLD,SHORTN,
     B         SIGPLC,SPINTR,SP_LEGEND,STLINE,STRINDEX,VECTD,XYPLOT

C
C     * DECLARE BLOCK DATAS EXTERNAL, SO INITIALIZATION HAPPENS
C
      EXTERNAL AGCHCU_BD
      EXTERNAL FILLCBBD
      EXTERNAL FILLPAT_BD

C-----------------------------------------------------------------------
      DATA ALABEL/" "/
      DATA AXLABEL/" "/
      CZXPL='ZXPL'
      MLLINE=35
C---------------------------------------------------------------------
      NFF=6
      CALL JCLPNT(NFF,LUNFIELD1,LUNFIELD2,LUNFIELD3,LUNFIELD4,5,6)
      CALL PSTART
C     * MINIMUM POSITIONS FOR LEFT AND BOTTOM OF FRAME
      YBOT  = 0.175
C      YBOT = 0.075
      FLMIN = 0.033
C
      SPVAL=1.0E38

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL

      TOL=1.D-6
      TOLA=5.D-6
C     ITYPE = 'ZXPL'
      ITYPE = IZXPL
      INZERO=-1
      IOFFP=1
      NULBLL=1
      ISIZEM=10
      CONT1   = .FALSE.
      SHAD1   = .FALSE.
      BOXSHD1 = .FALSE.
      BOXDRW1 = .FALSE.
      ISHADSPVAL = 1
      IREV = 0
C
C     * LINE WIDTH PARAMETERS
C
      LWBG=2000
      LWMN=1000
      LWMJ=2000
      LWBGP=4000
      LWMNP=2000
      LWMJP=2000

C     * CHANGE TO DUPLEX FONT FOR PCHIQU
      CALL PCSETI('FN', 12)
      CALL PCSETI('OF', 1)
      CALL PCSETI('OL', 2)
C
C     * READ THE CONTROL CARDS.
C     * CARD 1.
C
      ICOUNT = 0
  150 READ(5,5010,END=900)NAME,MODEZX,ICOSH1,ICOSH2,MS,LLAB,LX,SCAL,            
     1                    FLO,HI,FINC,KAX,IREV,IOVRLY,KIND                      
      ICOUNT=ICOUNT+1
C
C     * Check for arbitrary line contours
C
      IARBI=0
C
      IF(FINC.LT.0.) THEN
         IF(SCAL.EQ.0.) THEN
            WRITE(6,*) 'CAN NOT HAVE BOTH SCALE = 0 AND FINC < 0'
            CALL                                   PXIT('ZXPLOT',-1)
         ENDIF
         FINC=-FINC
         IARBI=INT(FINC)
      ENDIF      
C
C     * CHECK FOR USER DEFINED X-AXIS LABEL OPTION
C
      XLAB=0
      IF(MS.GE.10) THEN
         MS=MS-10
         XLAB=1
      ENDIF
C
C     * ALLOW ONLY LINEAR INTERPOLATION TO BE PERFORMED BY SETTING
C     * KIND TO 1. ALSO, CHECK IF DATA IS ON LAT-LON GRID (TO BE
C     * SHIFTED FROM THE POLE(S) OR NOT).
C
      IF(KIND.LT.0) THEN
        LLFLD=.TRUE.
        IF(KIND.LT.-4)THEN
          SHFT=.TRUE.
        ELSE
          SHFT=.FALSE.
        ENDIF
        KIND=ABS(KIND)
      ELSE
        LLFLD=.FALSE.
      ENDIF
C
      NZERO1 = .FALSE.
      CONT1 = .FALSE.
      SHAD1 = .FALSE.
      BOXSHD1 = .FALSE.
      BOXDRW1 = .FALSE.  
      NZERO2 = .FALSE. 
      CONT2 = .FALSE.
      SHAD2 = .FALSE.
      BOXSHD2 = .FALSE.
      BOXDRW2 = .FALSE.
C
      KIND = 1
C
      IMODEZX = ABS(MODEZX)
      IF (IMODEZX.NE.1.AND.(ICOSH1.LT.8.AND.ICOSH1.NE.4))CONT1 =  .TRUE.
      IF (IMODEZX.NE.1.AND.ICOSH1.GT.1)                  SHAD1 =  .TRUE. 
      IF (SHAD1.AND.(ICOSH1.GT.4))                      BOXSHD1 = .TRUE.
      IF (BOXSHD1.AND.((ICOSH1.EQ.7).OR.(ICOSH1.EQ.9))) BOXDRW1 = .TRUE.
      IF ((ICOSH1.EQ.1).OR.(ICOSH1.EQ.3).OR.(ICOSH1.EQ.6)
     1   .OR.(ICOSH1.EQ.7)) NZERO1 = .TRUE. 

      IF (MODEZX.NE.1.AND.(ICOSH2.LT.8.AND.ICOSH2.NE.4))CONT2 =  .TRUE.
      IF (MODEZX.NE.1.AND.ICOSH2.GT.1)                  SHAD2 =  .TRUE.
      IF (SHAD2.AND.(ICOSH2.GT.4))                      BOXSHD2 = .TRUE.
      IF (BOXSHD2.AND.((ICOSH2.EQ.7).OR.(ICOSH2.EQ.9))) BOXDRW2 = .TRUE.
      IF ((ICOSH2.EQ.1).OR.(ICOSH2.EQ.3).OR.(ICOSH2.EQ.6)
     1   .OR.(ICOSH2.EQ.7)) NZERO2 = .TRUE. 

      NZERO = NZERO1
      CONT = CONT1
      SHAD = SHAD1
      BOXSHD = BOXSHD1
      BOXDRW = BOXDRW1  

C
C     * CHECK FOR PUBLICATION QUALITY OPTION.
C
      IF (MS .LT. 0) THEN
         CALL GSLWSC(2.0)
         PUB=.TRUE.
         DOTITLE=.TRUE.
         DOINFOLABEL=.FALSE.
         DOLGND=.FALSE.
         DOLABEL=.FALSE.
         LHI=-1
      ELSE
         PUB=.FALSE.
         DOTITLE=.TRUE.
         DOINFOLABEL=.TRUE.
         DOLGND=.FALSE.
         IF(MS.EQ.1) THEN
            DOTITLE=.FALSE.
         ELSE IF(MS.EQ.2) THEN
            DOINFOLABEL=.FALSE.
         ELSE IF(MS.EQ.3) THEN
            DOTITLE=.FALSE.
            DOINFOLABEL=.FALSE.
         ELSE IF(MS.EQ.4) THEN
            DOTITLE=.FALSE.
            DOINFOLABEL=.FALSE.
         ELSE IF(MS.EQ.5) THEN
            DOLGND=.TRUE.
         ELSE IF(MS.EQ.6) THEN
            DOTITLE=.FALSE.
            DOLGND=.TRUE.
         ELSE IF(MS.EQ.7)THEN
            DOINFOLABEL=.FALSE.
            DOLGND=.TRUE.  
         ELSE IF(MS.EQ.8)THEN
            CALL GSLWSC(2.0)
            PUB=.TRUE.
            DOTITLE=.TRUE.
            DOINFOLABEL=.FALSE.
            DOLGND=.TRUE.
            DOLABEL=.FALSE.
            LHI=-1
         ENDIF
      ENDIF
C
C     * IF DOING ARBITRARY LINES WILL NOT PLOT INFOLABEL
C
      IF(IARBI.NE.0) DOINFOLABEL=.FALSE.
C
C     * LLAB overrides MS
C
      IF(LLAB.EQ.0) THEN
         DOLABEL=.TRUE.
      ELSE
         DOLABEL=.FALSE.
      ENDIF
C
C     * REWIND NECESSARY FILES (ONLY ONCE).
C
      IF (REW) THEN
        IF (IMODEZX.NE.1) REWIND LUNFIELD1
        IF (IMODEZX.EQ.1.OR.IMODEZX.EQ.2.OR.IMODEZX.EQ.5) THEN
          REWIND LUNFIELD2
          REWIND LUNFIELD3
        ENDIF
        IF (IMODEZX.GE.4) REWIND LUNFIELD4
        REW=.FALSE.
      ENDIF
      IF(NAME.EQ.NC4TO8('SKIP'))THEN
C       READ(LUNFIELD1,END=901)
        CALL FBUFFIN(LUNFIELD1,IBUF,MAXX,KKK,LLEN)
        IF (KKK.GE.0) GO TO 901
        GO TO 150
      ENDIF
      WRITE(6,6020)NAME,MODEZX,ICOSH1,ICOSH2,MS,LX,
     1             SCAL,FLO,HI,FINC,KAX,IOVRLY,KIND
      SECOND = .FALSE.
      IF(KIND.NE.1) KIND=3
      LHI = 0
      IF(MODEZX.GE.0.AND.MODEZX.NE.3) LHI=-1
C
C     * CARD 2.
C
      READ(5,5012,END=902) (LABEL(I),I=1,80)                                     
      IF(XLAB.EQ.1) THEN
         READ(5,5012,END=902) (XLABEL(I),I=1,80)                                     
      ENDIF
C
C     * READ SUBAREA CARD 3.
C
      DLAT1 = 90.
      DLAT2 =-90.
      DPR1 = 10.
      DPR2 = 1000.
      IFLAG = 0
      IXFLG = 0
      SUBAREA = .FALSE.
      LOVRLY = .TRUE.
      IF (LX.LT.0) THEN
       READ(5,5018,END=903) DLAT1,DLAT2,DTPR1,DTPR2,ITFLAG,IXFLG                
C       IF((IXFLG.NE.0).AND.(KIND.GE.0)) THEN
C          WRITE(6,*) 'IF DATA IS GAUSSIAN YOU MUST HAVE IXFLG=0'
C          CALL                                    PXIT('ZXPLOT',-101)
C       ENDIF
       IF ( IXFLG.EQ.0) THEN
        IF (DPR1.GE.DPR2.OR.DLAT1.LE.DLAT2) THEN
          CALL                                     PXIT('ZXPLOT',-102)
        ENDIF
        SUBAREA = .TRUE.
        LOVRLY = .FALSE.
        DPR1=DTPR1
        DPR2=DTPR2
        IFLAG=ITFLAG
        IF (IFLAG.EQ.0) IFLAG = 1
       ELSE
        IF (DLAT1.GE.DLAT2 .OR. .NOT. LLFLD ) THEN
          CALL                                     PXIT('ZXPLOT',-103)
        ENDIF
        WRITE(6,6060) DLAT1,DLAT2,IXFLG
       ENDIF
       LX = ABS(LX)
      ENDIF
C
      IF(IMODEZX.EQ.1) GO TO 310
C
C-----------------------------------------------------------------------
C     * READ-IN SCALAR FIELD TO BE CONTOURED.

C     * GET THE NEXT CROSS-SECTION.
C
  180 LU = LUNFIELD1
      IF (SECOND) LU = LUNFIELD4
      CALL GETSET2(LU,FXS,LEV,NLEV,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        WRITE(6,6010) NAME
        CALL                                       PXIT('ZXPLOT',-104)
      ENDIF
      IF(NLEV.EQ.1.OR.IBUF(6).NE.1) THEN
        CALL                                       PXIT('ZXPLOT',-105)
      ENDIF
      IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)
C
C     * KEEP IF NAME EQUALS IBUF(3) OR NC4TO8('NEXT').
C
      IF(NAME.EQ.NC4TO8('NEXT')) GO TO 190
      IF(IBUF(3).NE.NAME) GO TO 180
C
C     * PUT CROSS-SECTION INTO XS WITH LEVELS AND LATITUDES REVERSED.
C
  190 IF (SECOND) THEN
        IF(NLAT.NE.IBUF(5).OR.KHEM.NE.IBUF(7))CALL PXIT('ZXPLOT',-2)
      ELSE
        NLAT=IBUF(5)
        KHEM=IBUF(7)
      ENDIF
      CALL LVDCODE(PR,LEV,NLEV)
      DO 210 L=1,NLEV
        K=NLEV+1-L
        IF(KAX.EQ.1) THEN
C          LOG PRESSURE
           PR(L)=LOG(PR(L))
        ELSE IF(KAX.EQ.2) THEN
C          OCEAN DEPTH
           PR(L)=PR(L)/10.0
        ENDIF
C        IF(IREV.EQ.1)THEN
C           DO 208 J=1,NLAT
C              N=L*NLAT+1-J
C              JJ=NLAT-J+1
C              XS(JJ,K)=FXS(N)
C 208       CONTINUE
C        ELSE
           DO 209 J=1,NLAT
              N=L*NLAT+1-J
              XS(J,K)=FXS(N)
 209       CONTINUE
C        ENDIF
  210 CONTINUE
C
C
      IF (.NOT.SECOND) THEN
C
C       * CALCULATE THE LATITUDE GRID OF THE INPUT FILE.
C
        IF(LLFLD)THEN
C
C         * LAT-LON GRID CASE.
C
          IF(SHFT)THEN
            NDT=NLAT
          ELSE
            NDT=NLAT-1
          ENDIF

          IF (IXFLG.EQ.0) THEN
           IF((KHEM.EQ.0).OR.(KHEM.EQ.3))THEN
            ADEG= 180.0/NDT
            ASHFT=-90.0
           ELSE IF(KHEM.EQ.1)THEN
            ADEG=  90.0/NDT
            ASHFT=  -90.0
           ELSE IF(KHEM.EQ.2)THEN
            ADEG=  90.0/NDT
            ASHFT=  0.0
           ELSE
             CALL                                  PXIT('ZXPLOT',-106)
           ENDIF
          ELSE
           ASHFT=DLAT1
           ADEG=(DLAT2-DLAT1)/NDT
          ENDIF

          IF(SHFT) ASHFT=ASHFT+(0.5*ADEG)
          DO 211 I=1,NLAT
             SLAT(I) = ASHFT+(I-1)*ADEG
 211      CONTINUE

        ELSE
C
C         * GAUSSIAN GRID CASE.
C
          ILATH = NLAT/2
          IF (KHEM.EQ.1.OR.KHEM.EQ.2) ILATH = NLAT
          IF (MOD(NLAT,2).NE.0) CALL               PXIT('ZXPLOT',-107)
          CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)
          IF (KHEM.EQ.0) CALL TRIGL (ILATH,SL,WL,CL,RAD,WOSSL)
          RADADEG = 180./3.14159
          IF (KHEM.EQ.1) THEN
             RADADEG = -180./3.14159
             DO 212 I=1,NLAT/2
                ATEMP = RAD(NLAT+1-I)
                RAD(NLAT+1-I) = RAD(I)
                RAD(I) = ATEMP
 212         CONTINUE
          ENDIF
C
          DO 213 I=1,NLAT
             SLAT(I) =  RAD(I)*RADADEG
 213      CONTINUE
C
       ENDIF
C
        IF (LOVRLY) THEN
          CALL LVDCODE(DPR1,LEV(1),1)
          CALL LVDCODE(DPR2,LEV(NLEV),1)
          IF(IXFLG.EQ.0) THEN
           DLAT1=-1.*SLAT(1)
           DLAT2=-1.*SLAT(NLAT)
          ELSE
           DLAT1=SLAT(1)
           DLAT2=SLAT(NLAT)
          ENDIF
          IF (IOVRLY.LT.0) IFLAG=1
          IF (IOVRLY.EQ.0) IFLAG=2
          IF (IOVRLY.GT.0) IFLAG=3
          SUBAREA=.TRUE.
        ENDIF
C
C       * RESTRICT SUBAREA TO PHYSICAL LIMITS (90,-90), (.001,1100)
C
        IF (IXFLG.EQ.0) THEN
         IF (KHEM.EQ.1.AND..NOT.SUBAREA) DLAT2 = 0.
         IF (DLAT1.GT.90.) DLAT1 = 90.
         IF (KHEM.EQ.1.AND.DLAT2.LT.0) DLAT2 = 0.
         IF (KHEM.EQ.0.AND.DLAT2.LT.-90.) DLAT2 = -90.
         IF (KHEM.EQ.2.AND..NOT.SUBAREA) DLAT2 = -90.
         IF (KHEM.EQ.2.AND.DLAT1.GT.0) DLAT1 = 0.
        ENDIF
        IF (KAX .NE. 2) THEN
           IF (DPR1.LT..001) DPR1 = .001
           IF (DPR2.GT.1100.) DPR2 = 1100.
        ENDIF
        IF (KAX.EQ.1)THEN
           DPR1 = LOG(DPR1)
           DPR2 = LOG(DPR2)
        ELSE IF (KAX.EQ.2)THEN
C          OCEAN DEPTH
           DPR1 = DPR1/10.0
           DPR2 = DPR2/10.0
        ENDIF
C
C       * SET THE COORDINATES OF THE FRAME
C
        IF (IFLAG.NE.2) THEN
          IF(KAX.EQ.1)THEN
            FYT = LOG(10.)
            FYB = LOG(1000.)
          ELSE IF(KAX.EQ.2)THEN
            FYT = DPR1
            FYB = DPR2
          ELSE
            FYT = 10.
            FYB = 1000.
          ENDIF
        ELSE
          FYT = DPR1
          FYB = DPR2
        ENDIF
        IF (IXFLG.EQ.0 .AND.((IFLAG.NE.2).OR.(LOVRLY))) THEN
          FXL = 90.
          FXR = -90.
          IF (KHEM.EQ.1) FXR = 0.
          IF(KHEM.EQ.2) THEN
             FXR = -90.
             FXL =  0.
          ENDIF
        ELSE
          FXL = DLAT1
          FXR = DLAT2
        ENDIF
C
C       * MAKE SURE THE COORDINATES ARE BOUNDED BY THE INPUT FILE RANGE.
C
        IF(IXFLG.EQ.0) THEN
         IF(DLAT1.GT.-1.*SLAT(1)) DLAT1 = -1.*SLAT(1)
         IF(DLAT2.LT.-1.*SLAT(NLAT)) DLAT2 = -1.*SLAT(NLAT)
        ENDIF
        IF (DPR1.LT.PR(1)) DPR1 = PR(1)
        IF (DPR2.GT.PR(NLEV)) DPR2 = PR(NLEV)
C
C       * SET THE COORDINATES OF THE PLOTTED AREA.
C
        PYT = DPR1
        PYB = DPR2
        PXL = DLAT1
        PXR = DLAT2
C
C       * SET THE DIMENSIONS OF THE INTERPOLATED FIELD.
C
        IF (LX.GT.99) LX=99
        IF(IXFLG.EQ.0) THEN
         NLATP = INT(FLOAT(NLAT)*(PXL-PXR)/(SLAT(NLAT)-SLAT(1)) + .5)
        ELSE
         NLATP = INT(FLOAT(NLAT)*(PXR-PXL)/(SLAT(NLAT)-SLAT(1)) + .5)
        ENDIF
        IF (NLATP.LT.6) NLATP = 6
        ISCLAT=1
        LXP = LX
        ISCLX=1
        IF (LX .LT.4) THEN
          IF (NLATP.LE.24) ISCLAT=4
          IF (NLATP.LE.33) ISCLAT=3
          IF (NLATP.LE.48) ISCLAT=2
          NLATP=ISCLAT*NLATP
          LX = 2*NLAT/3
          LXP = 2*NLATP/3
          ISCLX = ISCLAT
        ENDIF
C
C       * SET THE POSITION OF THE FRAME ON THE PLOTTER PAGE.
C
        FLMIN = 0.025
C
        IF (IFLAG.EQ.2) THEN
        FHGT=FSCAL*(FLOAT(LXP-1)*(FYB-FYT)/(FLOAT(NLATP-1)*(PYB-PYT)))
        ELSE
          IF (KAX.EQ.1) THEN
             DENOM = LOG(1000.)-LOG(10.)
          ELSE IF (KAX.EQ.2) THEN
             DENOM = DPR2-DPR1
          ELSE
             DENOM = 1000.-10.
          ENDIF
          FHGT=FSCAL*FLOAT(LX-1)*(FYB-FYT)/(DENOM*FLOAT(NLAT-1))
        ENDIF
        IF (IXFLG.EQ.0 .AND. ((IFLAG.NE.2).OR.(LOVRLY))) THEN
          FWDTH =FSCAL*(FXL-FXR)/(SLAT(NLAT)-SLAT(1))
          FLEFT = (1.-FWDTH)/2. + FLMIN
        ELSE
          FWDTH = FSCAL
C          FLEFT = (1-FWDTH)/2. + FLMIN
          FLEFT = 0.075
        ENDIF
C
C       * SET THE POSITION OF THE PLOTTED AREA ON THE PLOTTER PAGE.
C
C        IF(DOLGND)THEN
C           YBOT = .175   
C        ELSE
C           YBOT=0.075
C        ENDIF
        PHGT = FHGT*(PYB-PYT)/(FYB-FYT)
        PWDTH = FWDTH*(PXL-PXR)/(FXL-FXR)
        PLEFT = FLEFT + PWDTH*(FXL-PXL)/(PXL-PXR)
        PBOT = YBOT + PHGT*(FYB-PYB)/(PYB-PYT)
        FBOT = YBOT
        IF (IFLAG.EQ.2) FBOT = PBOT
C
C     * SET THE FRAMED AREA EQUAL TO THE PLOTTED AREA
C
        IF (IFLAG.EQ.3) THEN
          IF (.NOT.LOVRLY) THEN
            FXL = PXL
            FXR = PXR
            FWDTH = PWDTH
            FLEFT = PLEFT
          ENDIF
          FYB = PYB
          FYT = PYT
          FHGT = PHGT
          FBOT = PBOT
        ENDIF
C
C       * IF THE PLOT IS TOO TALL, SCALE IT DOWN TO FIT THE PLOTTER PAGE.
C
        IF ((FHGT+FBOT).GT.FSCAL.AND.IFLAG.EQ.2) THEN
          SCALE = FSCAL/(FHGT+FBOT)
          PHGT = SCALE*PHGT
          PWDTH = SCALE*PWDTH
          FWDTH = SCALE*FWDTH
          FHGT = FSCAL-FBOT
          PLEFT = SCALE*(PLEFT-FLEFT) + FLEFT
          PBOT = SCALE*(PBOT-FBOT) + FBOT
        ENDIF
C
C       * COMPUTE THE EQUALLY SPACED PRESSURE LEVELS AND LATITUDES FOR
C       * INTERPOLATION TO THE DISPLAY GRID.
C
        IF (IXFLG.NE.0) THEN
         DY = (PXR-PXL)/FLOAT(NLATP-1)
         DO 214 I=1,NLATP
            SLATP(I) = PXL + DY*FLOAT(I-1)
 214     CONTINUE
        ELSE
         DY = (PXL-PXR)/FLOAT(NLATP-1)
         DO 215 I=1,NLATP
            SLATP(I) = DY*FLOAT(I-1) - PXL
 215     CONTINUE
        ENDIF
        DP = (DPR2-DPR1)/FLOAT(LXP-1)
        DO 216 L=1,LXP
  216   PRX(L)=(L-1)*DP+DPR1
C
      ENDIF
C
C     * PRECOMPUTE THE LAGRANGIAN DENOMINATORS IN DEN.
C
      IF(NLEV.LE.3) KIND=1
      IF(KIND.EQ.3) CALL LGRDC(DEN,PR,NLEV)
C
C     * INTERPOLATE THE DISPLAY LEVELS ONE COLUMN AT A TIME.
C
      DO 250 J=1,NLAT
        DO 230 L=1,NLEV
          K=NLEV+1-L
  230     Y(L)=XS(J,K)
        IF(KIND.EQ.1) CALL LINIL(YX,PRX,LXP,Y,PR,NLEV,0.,0.)
        IF(KIND.EQ.3) CALL LGRIC(YX,PRX,LXP,Y,PR,DEN,NLEV,0.,0.)
        DO 240 L=1,LXP
          N=(L-1)*NLAT+J
          K=LXP+1-L
  240     FXS(N)=YX(K)
  250 CONTINUE
C
C     * INTERPOLATE HORIZONTALLY ONE LEVEL AT A TIME.
C
      DO 252 L = 1,LXP
         DO 254 I = 1,NLAT
            WX(I) = FXS( (L-1)*NLAT + I)
 254     CONTINUE
         CALL LINIL(YX,SLATP,NLATP,WX,SLAT,NLAT,0.,0.)
         IF(IREV.NE.1) THEN
            DO 256 I = 1,NLATP
               VXS( (L-1)*NLATP + I) = YX(I)
 256        CONTINUE
         ELSE
            DO 257 I = 1,NLATP
               VXS( (L-1)*NLATP + NLATP-I+1 ) = YX(I)
 257        CONTINUE            
         ENDIF
 252  CONTINUE
      NWDS=NLATP*LXP
      DO 259 I = 1,NWDS
         FXS(I) = VXS(I)
 259  CONTINUE      
C
C       * CALCULATE SCALE FACTOR AUTOMATICALLY IF DESIRED
C
        IF (SCAL.EQ.0.) THEN
          ARIMEAN = AMEAN2(FXS,NLATP,LXP,1,0)
          SMALL   = FXS(ISMIN(NWDS,FXS,1))
          IF (SMALL.GT.0.) THEN
            GEOMEAN = GMEAN2(FXS,NLATP,LXP,1,0)
          ENDIF
          CALL PRECON3(FLO,HI,FINC,SCAL,FXS,NLATP,LXP,10,SPVAL)
        ENDIF
C
C     * SCALE THE CROSS-SECTION FIELD.
C
      DO 260 I=1,NWDS
         IF (ABS(FXS(I)-SPVAL).GT.SPVALT) THEN
            F(I)=SCAL*FXS(I)
         ELSE
            F(I)=SPVAL
         ENDIF
  260 CONTINUE
      IF (PBOT+PHGT.GT.1.) THEN
        CALL                                       PXIT('ZXPLOT',-108)
      ENDIF
      XPOS = FLOAT(NLATP)
      YPOS = FLOAT(LXP)
      CALL SET(PLEFT,PLEFT+PWDTH,PBOT,PBOT+PHGT,1.,XPOS,1.,YPOS,1)
C
      PT = F(NLATP*2+3)
      ICOSH = ICOSH1
      IF (SECOND) THEN 
         ICOSH = ICOSH2
         NZERO = NZERO2
         CONT = CONT2
         SHAD = SHAD2
         BOXSHD = BOXSHD2
         BOXDRW = BOXDRW2
      ENDIF
      WHITFG = 0 
      CALL BFCRDF(0)
C     * IF LHI = 0, HIGHS AND LOWS LABELLED, 
C              =-1, .. NOT LABELLED.
C     
      IF (LHI.EQ.0)  CALL CPSETC('HLT',
     1                 'H:B:$ZDV$:E:''L:B:$ZDV$:E:')
      IF (LHI.EQ.-1) CALL CPSETC('HLT',' '' ')
C     * Set special value SPVAL=1.0E38
      CALL CPSETR('SPV', SPVAL)
C     
      IF (ICOSH.GT.1) THEN
C
C       * READ SHADING CARD 4 (OR 8 FOR SECOND FIELD).
        LGND=.FALSE.
        READ(5,5014,END=904) WHITFG, NPAT, (IPAT(I),I=1,7)                    
C      If 8 <= NPAT <= 14,  CARD 4 will read two lines.
        IF (NPAT .GT. 7) THEN
          READ(5,5015,END=904) (IPAT(I),I=8,14)
        ENDIF
        IF (NPAT .GT. 14) THEN
          READ(5,5015,END=904) (IPAT(I),I=15,21)
        ENDIF
        IF (NPAT .GT. 21) THEN
          READ(5,5015,END=904) (IPAT(I),I=22,NPAT)
        ENDIF

C       * READ LEVELS CARDS 5 (OR 9 FOR SECOND FIELD)
        IF(NPAT.LE.7) THEN
           READ(5,5016,END=905) (ZLEV(I),I=1,NPAT-1)                         
        ELSE
           READ(5,5016,END=905) (ZLEV(I),I=1,7) 
        ENDIF
C      If 8 <= NPAT <= 14,  CARD 5 will read two lines.
        IF (NPAT .GT. 7.AND.NPAT.LE.14) THEN
          READ(5,5016) (ZLEV(I),I=8,13)
        ENDIF
        IF (NPAT .GT. 14.AND.NPAT.LE.21) THEN
          READ(5,5016) (ZLEV(I),I=8,14)
          READ(5,5016) (ZLEV(I),I=15,NPAT-1)
        ENDIF
        IF (NPAT .GT. 21.AND.NPAT.LE.28) THEN
          READ(5,5016) (ZLEV(I),I=8,14)
          READ(5,5016) (ZLEV(I),I=15,21)
          READ(5,5016) (ZLEV(I),I=22,NPAT-1)
        ENDIF

C
C

C       * CHECK LEVEL VALUES FOR SPVAL. ONLY SHADE SPVAL IF SPVAL
C       * IS SPECIFIED AS A VALUE.
         ISHADSPVAL=1
         DO I=1,NPAT-1
            IF(ZLEV(I).GE.1E37) THEN
               ISHADSPVAL=0
            ENDIF
CDEBUG
CCCC           WRITE(*,*)'ISHADVAL: ',ZLEV(I),SPVAL,ZLEV(I)-SPVAL,ISHADSPVAL
         ENDDO

C       * IF NOT DOING SPVAL, THEN SET A MAXIMUM
         ZLEV(NPAT) = 1E35

C     Changed definition of colour plot to mean CLRPLT is true if 
C     there is at least one level of colour shading.
         CLRPLT = .FALSE.
         PATPLT = .FALSE.
         DO 395 I = 1, NPAT
            IF (IPAT(I) .GE. 100 .AND. IPAT(I) .LE. 199) THEN
               CLRPLT = .TRUE.
            ELSEIF(IPAT(I) .GE. 350 .AND. IPAT(I) .LE. 420)THEN
               CLRPLT = .TRUE.
            ELSE
               PATPLT = .TRUE.
            ENDIF
 395      CONTINUE
C
          IF(CLRPLT) THEN
             IF(NPAT.GT.0) THEN
                NCLRS=-NPAT
             ELSE
                NCLRS=0
             ENDIF
             CALL DFCLRS(NCLRS, HSVV,IPAT)
          ENDIF
C
C    * SET UP BACKGROUND
C
C        LXXX = INT((LX-1)/5)
C        LYYY = INT((LY-1)/5)
C        CALL GRIDAL(LXXX,5,LYYY,5,0,0,5,0.,0.)
C
C       * COLOR LEGEND
C
        IF(DOLGND)THEN
           IF(ISHADSPVAL.EQ.0) THEN
              NL1=NPAT-2
              NPATM=NPAT-1
              NPATM2=NPAT-2
           ELSE
              NL1=NPAT-1
              NPATM=NPAT
              NPATM2=NPAT-1
           ENDIF
C
           DO I=1,NPATM2
              IZZ=0
              FLVAL=ZLEV(I)
C             IF(FLVAL.EQ.0.0.OR.
C    1             (FLVAL.GE.-TOL.AND.FLVAL.LE.TOL)) THEN
              IF (ABS(FLVAL).LE.TOL) THEN
                 WRITE(LLBST,2002)
              ELSEIF(ABS(FLVAL).GT.9999..OR.ABS(FLVAL).LT.0.001) 
     1                THEN
                 WRITE(LLBST,2000) FLVAL            
              ELSE
                 FVAL=ABS(1.0D0*FLVAL)+1.D0*TOLA
                 FREST=10.D0*(1.D0*FVAL-AINT(1.D0*FVAL))
                 DO J=1,3
                    IFINT=AINT(FREST)
                    IF(IFINT.GT.0) IZZ=J
                    FREST=(10.D0*(1.D0*FREST-AINT(1.D0*FREST)))
                 ENDDO
                 IF(IZZ.EQ.0) THEN
                    IF(FLVAL.LT.0.) THEN
                       IFLVAL=AINT(FLVAL-TOLA)
                    ELSE
                       IFLVAL=AINT(FLVAL+TOLA)
                    ENDIF
                    WRITE(LLBST,992) IFLVAL
                 ELSEIF(IZZ.EQ.1) THEN
                    WRITE(LLBST,2030) FLVAL
                 ELSEIF(IZZ.EQ.2) THEN
                    WRITE(LLBST,2031) FLVAL
                 ELSEIF(IZZ.EQ.3) THEN
                    WRITE(LLBST,2032) FLVAL
                 ELSE
                    WRITE(LLBST,2000) FLVAL
                 ENDIF
              ENDIF
C
              LLBS(I)=LLBST(LSTRBEG(LLBST):LSTREND(LLBST))
           ENDDO
C     
           DO I = 1, NPATM
              IF(IPAT(I).GE.350) THEN
                 LIND(I) = IPAT(I) - 248
              ELSEIF(IPAT(I).GE.100.AND.IPAT(I).LE.199) THEN
                 LIND(I) = IPAT(I) - 98
              ELSE
               CALL DFNCLR(IPAT(I), LIND(I))
              ENDIF
           ENDDO
C     
           PERC = 0.93
           XLMAX = FLEFT + PERC * FWDTH
           XLMIN = FLEFT + FWDTH * (1.0 - PERC)
           IF(NPAT.GT.7)THEN
              RMIN=0.
              RBOX=.35
           ELSE
              RMIN=0.02
              RBOX=.45
           ENDIF
C
           CALL LBLBAR (0,XLMIN,XLMAX,RMIN,0.07,NPATM,1.,RBOX,LIND,2,
     1                  LLBS,NL1,1)
C
        ENDIF
C
      ENDIF
C
C     * READ ARBITRARY LINE CONTOURS
C
      IF (IARBI.NE.0) THEN
         NPATS=IARBI
C     READ LEVELS
         IF(NPATS.GT.NIPATS) THEN
            WRITE(6,*) 'TOO MANY CONTOUR LEVELS, NPATS=',NPATS
            CALL                                   PXIT('ZXPLOT',-3)
         ENDIF
         DO K=1,NPATS,7
            READ(5,5016) (ZLEVS(I),I=K,K+6)
         ENDDO
C
      ENDIF
C
      


      CALL CONTDF(PUB)

      IF (SHAD) THEN
         CALL ARINAM(ZX_MAP,ZX_MAP_SIZE)
         CALL CPRECT(F,NLATP,NLATP,LXP,RWRK,LRWK,IWRK,LIWK)
         CALL CPCLAM(F,RWRK,IWRK,ZX_MAP)
         IF (.NOT. BOXSHD) THEN
            CALL GSLWSC(1.)
            CALL ARSCAM(ZX_MAP,XWRK,YWRK,NWRK,IAREA,IGRP,
     1           NOGRPS,PATTERN)
            CALL CPSETI('LBC',0)       
            IF (PATPLT) THEN 
               CALL CPSETI('LLB',2)
               CALL CPSETI('HLB',2)
            ENDIF
         ELSE
            CALL NEW_HAFTNP(F,NLATP,NLATP,LXP,
     1           ISHADSPVAL,SPVAL,BOXDRW)
         ENDIF
      ENDIF 
      CALL GSLWSC(1.5)

C     
C     * SET CONTOURING PARAMETERS
C     
      IF (CONT) THEN 
         CALL ARINAM(ZX_MAP,ZX_MAP_SIZE)
C CPRECT is called by INITCL
         CALL INITCL(FLO, HI, FINC, FINCN,
     1        F,NLATP,LXP,RWRK,LRWK,IWRK,LIWK)
C Set label parameters, dashed/solid
         CALL CONLS(FLO, HI, FINC, NZERO, PUB, FHIGHN, FLOWN)    

C Set thick/thin contours
         CALL CONCN(FHIGHN, FLOWN, FINC, PUB, DOLABEL)
         IF ((INZERO .GT. 0).AND.(NZERO)) THEN
            CALL CPSETI('PAI - PARAMETER ARRAY INDEX', INZERO)
            CALL CPSETI('CLU', 0)
            CALL CPSETR('CLL', 0)
         ENDIF

C     
C     * SETUP INFO LABEL
C     
         IF (DOINFOLABEL) THEN
            CALL CPGETR('CMN - CONTOUR LEVEL MINIMUM', CMN)
            CALL CPGETR('CMX - CONTOUR LEVEL MAXIMUM', CMX)
            CALL CPGETR('CIU - CONTOUR INTERVAL USED', CIU)

            WRITE(INFOLABEL,90) CMN,CMX,CIU,SCAL,PT
 90         FORMAT('CONTOUR FROM ',G11.5,' TO ',G11.5,
     1           ' INTERVAL = ',G11.5,
     2           ' SCALE = ',G11.5,' PT(3,3) =  ',G11.5)
        
            CALL CPSETR('ILS - INFORMATION LABEL SIZE', 0.007)
            CALL CPSETC('ILT - INFORMATION LABEL TEXT', INFOLABEL)
            CALL CPSETI('ILC - INFORMATION LABEL COLOUR INDEX', 1)
            CALL CPSETI('ILP - INFORMATION LABEL POSITION FLAG', -3)
            CALL CPSETR('ILX - INFORMATION LABEL X COORDINATE',0.5)
C            IF(DOLGND)THEN
            CALL CPSETR('ILY - INFORMATION LABEL Y COORDINATE',
     1                     -0.165)
C            ELSE
C               CALL CPSETR('ILY - INFORMATION LABEL Y COORDINATE',-0.1)  
C            ENDIF
         ENDIF
C
C          * DRAWING CONTOURING LINES
C     
         CALL CPLBAM(F,RWRK,IWRK,ZX_MAP)
         CALL CPCLDM(F,RWRK,IWRK,ZX_MAP,CPDRPL)
         CALL CPLBDR(F,RWRK,IWRK)
         
CDEBUG
CCCC          CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
CCCC          DO I = 1, NCL
CCCC             CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
CCCC             CALL CPGETR('CLV - CONTOUR LEVEL VALUES',FLVAL)
CCCC             CALL CPGETC('CLD',DPAT)
CCCC             CALL CPGETR('CLL',CLL)
CCCC             CALL CPGETI('CLU',ICLU)
CCCC             CALL CPGETI('DPU',IDPU)
CCCC             CALL CPGETI('LLP',LLP)
CCCC             CALL CPGETC('LLT',LLT)
CCCC             CALL CPGETR('DPS',DPS)
CCCC             CALL CPGETR('DPV',DPV)
CCCC CCCC          WRITE(*,*)'ZXPLOT2',I,FLVAL,PUB,DPAT,ICLU,IDPU,LLP
CCCC             WRITE(*,*)'ZXPLOT2',I,FLVAL,PUB,CLL,ICLU,
CCCC     1            IDPU,LLP,''''//LLT//''''
CCCC             IF(FLVAL.LT.0) THEN
CCCC                CALL CPSETC('CLD',"$$$$$$$$''''''''$$$$$$$$") 
CCCC             ENDIF
CCCC          ENDDO
CDEBUG


      ENDIF

C
C     * MAP THE CROSS-SECTION ON LINE PRINTER (NO MAP IF MS=0).
C
CCCC        CALL FCONW2(FXS,FINC,SCAL,NLATP,LXP,1,1,NLATP,LXP,MS)

C     * RESET PICTURE MODEL IF FRAME ALREADY CONTAINS SHADING AND CONTOURS
C     * ALSO RESET AFTER SECOND FILE IS CONTOURED AND/OR SHADED.

        IF(.NOT.SECOND)THEN
           IF(CONT1 .AND. SHAD1) THEN
              CALL RESET
           ENDIF
        ELSE
           CALL RESET
        ENDIF
C
C     * WRITE THE LABEL ABOVE THE CROSS-SECTION.
C
C      CALL SET(.01,.99,.01,.99,.01,.99,.01,.99, 1)
      CALL SET(0.,1.,0.,1.,0.,1.,0.,1., 1)

C     * FIGURE OUT HOW FAR ABOVE THE GRAPH THE LOWEST LABEL SHOULD GO.
C     * FHGT + FBOT ==> TOP OF THE GRAPH
C     * CPFY(ISIZ/2) ==> THE TOPMOST PRESSURE LABEL WILL STICK UP THIS
C     *                  MUCH ABOVE THE GRAPH
C     * (CHRATIO - 1)*CPFY(ISIZ) ==> THE AMOUNT OF WHITE SPACE NEEDED ABOVE
C     *                              THE TOPMOST PRESSURE LABEL
C
      YPOS = FHGT + FBOT + CPFY(ISIZ/2) + (CHRATIO - 1)*CPFY(ISIZ)
C
      IF (SECOND) THEN
C
C          * POSITION THE TITLE AND LABELS FOR THE SECOND SCALAR FIELD.
C
           YPOS = YPOS + CPFY(ITSIZ/2)
      ELSE
C
C          * POSITION THE TITLE AND LABELS FOR THE FIRST SCALAR FIELD.
C
           YPOS = YPOS + CPFY(ITSIZ/2) + CHRATIO*CPFY(2*ITSIZ)
      ENDIF
C
      WRITE(ALABLAT,6099)
      IF(KAX.EQ.0)THEN
         WRITE(ALABY,6100)
      ELSEIF(KAX.EQ.1)THEN
         WRITE(ALABY,6100)
      ELSEIF(KAX.EQ.2)THEN
         WRITE(ALABY,6102)         
      ENDIF
      WRITE(6,6041) (LABEL(I),I=1,80)
      CALL GETUSV('LW',IOLDLW)
      CALL SETUSV('LW',MAJLW)
      CHSIZE=1.0/85.0
      XTITLE = FWDTH / 2.+ FLEFT
      YLABLAT = FBOT - 2.5* CPFX(ISIZ + 8)
C
C     MAIN TITLE
C
      IF(DOTITLE) THEN
         CALL PCHIQU(XTITLE,YPOS,
     1        ALABEL(LSTRBEG(ALABEL):LSTREND(ALABEL)),CHSIZE,0.,0.)
      ENDIF
C
C     X LABEL TEXT
C
      IF(XLAB.EQ.0) THEN
         CALL PCHIQU(XTITLE,YLABLAT,ALABLAT,CHSIZE,0.,0.)
      ELSE
         CALL PCHIQU(XTITLE,YLABLAT,
     1   AXLABEL(LSTRBEG(AXLABEL):LSTREND(AXLABEL)),CHSIZE,0.,0.)
      ENDIF
C
C     Y LABEL TEXT (NO SPACE TO WRITE ONE IF WE WANT TO BE 
C     BACKWARD COMPATIBLE)
C

      YLABY =  FBOT + FHGT / 2.
      YTITLE = FLEFT - 0.068
      CALL PCHIQU(YTITLE,YLABY,
     1        ALABY(LSTRBEG(ALABY):LSTREND(ALABY)),CHSIZE,90.,0.)
C
      CALL SETUSV('LW',IOLDLW)

C
C     * READ CARDS 6 AND 7 FOR SECOND SCALAR FIELD, THEN PLOT.
C
      IF (IMODEZX.GE.4.AND..NOT.SECOND) THEN
        SECOND = .TRUE.
        READ(5,5011,END=906)NAME,II,JJ,KK,MS,N,SCAL,FLO,HI,FINC,NN,             
     1                      III,KIND                                            
        READ(5,5012,END=907)(LABEL(I),I=1,80)                                   
C
        IF(MS.EQ.0) THEN 
           DOLGND=.FALSE.
        ELSE
           DOLGND=.TRUE.
        ENDIF
C



C
C       * ALLOW ONLY LINEAR INTERPOLATION TO BE PERFORMED BY SETTING
C       * KIND TO 1. ALSO, CHECK IF DATA IS ON LAT-LON GRID (TO BE
C       * SHIFTED FROM THE POLE(S) OR NOT).
C
        IF(KIND.LT.0) THEN
          LLFLD=.TRUE.
          IF(KIND.LT.-4)THEN
            SHFT=.TRUE.
          ELSE
            SHFT=.FALSE.
          ENDIF
          KIND=ABS(KIND)
        ELSE
          LLFLD=.FALSE.
        ENDIF
C
        KIND = 1
C       IF (KIND.NE.1) KIND = 3
        LHI = 0
        IF (II.GE.0) LHI = -1
        IF (MS.LT.0) MS = ABS(MS+1)
        GO TO 180
      ENDIF

      IF(MODEZX.EQ.0.OR.MODEZX.EQ.3.OR.IMODEZX.EQ.4) GO TO 700
C-----------------------------------------------------------------------
C     * BRING THE VECTOR FIELDS.
C     * READ IN ZONALLY AVERAGED V FIELD.
C
C     * VECTOR CARDS 10 AND 11.
C
  310 READ(5,5020,END=908) VSCAL,VLO,VI,INCY,INCZ                               
      READ(5,5012,END=909) (LABEL(I),I=1,80)                                    


      IF(VSCAL.EQ.0.) VSCAL=1.

C
      CALL GETSET2(LUNFIELD2,VXS,LEV,NLEV,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        WRITE(6,6010) NAME
        CALL                                       PXIT('ZXPLOT',-109)
      ENDIF
      IF(NLEV.EQ.1.OR.IBUF(6).NE.1) THEN
        CALL                                       PXIT('ZXPLOT',-110)
      ENDIF
      IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)
C
C     * READ IN ZONALLY AVERAGED W FIELD.
C
      CALL GETSET2(LUNFIELD3,WXS,LEV,NLEV,JBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        WRITE(6,6010) NAME
        CALL                                       PXIT('ZXPLOT',-111)
      ENDIF
      IF(NLEV.EQ.1.OR.JBUF(6).NE.1) THEN
        CALL                                       PXIT('ZXPLOT',-112)
      ENDIF
      IF(JBUF(8).LE.0) WRITE(6,6090) JBUF(8)

C
C     * MAKE SURE THE 2 VECTOR FIELDS ARE THE SAME KIND AND SIZE.
C
      CALL CMPLBL(0,IBUF,0,JBUF,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6025) IBUF,JBUF
        CALL                                       PXIT('ZXPLOT',-4)
      ENDIF
C
C     * PUT CROSS-SECTIONS INTO VS AND WS WITH LEVELS AND LATITUDES REVERSED.
C     * MULTIPLY MERIDIONAL VECTOR BY -1 TO COMPENSATE FOR VECTOR DEFAULT
C     * OF POSITIVE VALUES AS SOUTHWARD.
C     * VERTICAL VECTOR IS UNALTERED, SO THAT POSITIVE VALUES IMPLY UPWARD;
C     * HENCE THIS FIELD MUST BE MODIFIED BEFORE RUNNING THE PROGRAM IF USING
C     * AN "OMEGA-LIKE" VERTICAL MOTION FIELD.

      IF (IMODEZX.NE.1) THEN
        IF(NLAT.NE.IBUF(5).OR.KHEM.NE.IBUF(7))CALL PXIT('ZXPLOT',-5)
      ELSE
        NLAT=IBUF(5)
        KHEM=IBUF(7)
      ENDIF
C
C      CALL GSLWSC(2)
      CALL VVSETR('LWD', 2.)
      CALL LVDCODE(PR,LEV,NLEV)
      DO 320 L=1,NLEV
        K=NLEV+1-L
        IF(KAX.EQ.1) THEN
           PR(L)=LOG(PR(L))
        ELSE IF(KAX.EQ.2) THEN
           PR(L)=PR(L)/10.0
        ENDIF
        DO 320 J=1,NLAT
          N=L*NLAT+1-J
          VS(J,K)=-1.*VXS(N)
          WS(J,K)=WXS(N)
  320 CONTINUE
C
      IF (IMODEZX.EQ.1) THEN
C
C       * CALCULATE THE LATITUDE GRID OF THE INPUT FILE.
C
        IF(LLFLD)THEN
C
C         * LAT-LON GRID CASE.
C
          IF(SHFT)THEN
            NDT=NLAT
          ELSE
            NDT=NLAT-1
          ENDIF
          IF (IXFLG.EQ.0) THEN
           IF((KHEM.EQ.0).OR.(KHEM.EQ.3))THEN
            ADEG= 180.0/NDT
            ASHFT=-90.0
           ELSE IF(KHEM.EQ.1)THEN
            ADEG=  90.0/NDT
            ASHFT= -90.0
           ELSE IF(KHEM.EQ.2)THEN
            ADEG=  90.0/NDT
            ASHFT= 0.0
           ELSE
             CALL                                  PXIT('ZXPLOT',-113)
           ENDIF
          ELSE
           ASHFT=DLAT1
           ADEG=(DLAT2-DLAT1)/NDT
          ENDIF
          IF(SHFT) ASHFT=ASHFT+(0.5*ADEG)
          DO 321 I=1,NLAT
  321     SLAT(I) = ASHFT+(I-1)*ADEG

        ELSE
C
C         * GAUSSIAN GRID CASE.
C
          ILATH = NLAT/2
          IF (KHEM.EQ.1.OR.KHEM.EQ.2) ILATH = NLAT
          CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)
          IF (KHEM.EQ.0) CALL TRIGL (ILATH,SL,WL,CL,RAD,WOSSL)
          RADADEG = 180./3.14159
          IF (KHEM.EQ.1) THEN
            RADADEG = -180./3.14159
            DO 322 I=1,NLAT/2
              ATEMP = RAD(NLAT+1-I)
              RAD(NLAT+1-I) = RAD(I)
  322         RAD(I) = ATEMP
          ENDIF
          DO 323 I=1,NLAT
             SLAT(I) =  RAD(I)*RADADEG
 323      CONTINUE
        ENDIF
C
        IF (LOVRLY) THEN
          CALL LVDCODE(DPR1,LEV(1),1)
          CALL LVDCODE(DPR2,LEV(NLEV),1)
          IF(IXFLG.EQ.0) THEN
           DLAT1=-1.*SLAT(1)
           DLAT2=-1.*SLAT(NLAT)
          ELSE
           DLAT1=SLAT(1)
           DLAT2=SLAT(NLAT)
          ENDIF
          IF (IOVRLY.LT.0) IFLAG=1
          IF (IOVRLY.EQ.0) IFLAG=2
          IF (IOVRLY.GT.0) IFLAG=3
          SUBAREA=.TRUE.
        ENDIF
C
C       * RESTRICT SUBAREA TO PHYSICAL LIMITS (90,-90), (.001,1100)
C
        IF (IXFLG.EQ.0) THEN
         IF (KHEM.EQ.1.AND..NOT.SUBAREA) DLAT2 = 0.
         IF (DLAT1.GT.90.) DLAT1 = 90.
         IF (KHEM.EQ.1.AND.DLAT2.LT.0) DLAT2 = 0.
         IF (KHEM.EQ.0.AND.DLAT2.LT.-90.) DLAT2 = -90.
         IF (KHEM.EQ.2.AND..NOT.SUBAREA) DLAT2 = -90.
         IF (KHEM.EQ.2.AND.DLAT1.GT.0) DLAT1 = 0.
        ENDIF
        IF (KAX.NE.2) THEN
           IF (DPR1.LT..001) DPR1 = .001
           IF (DPR2.GT.1100.) DPR2 = 1100.
        ENDIF
        IF (KAX.EQ.1)THEN
           DPR1 = LOG(DPR1)
           DPR2 = LOG(DPR2)
        ELSE IF (KAX.EQ.2)THEN
           DPR1 = DPR1/10.0
           DPR2 = DPR2/10.0
        ENDIF
C
C       * SET THE COORDINATES OF THE FRAME
C
        IF (IFLAG.NE.2) THEN
          IF(KAX.EQ.1)THEN
            FYT = LOG(10.)
            FYB = LOG(1000.)
          ELSE IF(KAX.EQ.2)THEN
            FYT = DPR1/10.
            FYB = DPR2/10.
          ELSE
            FYT = 10.
            FYB = 1000.
          ENDIF
        ELSE
          FYT = DPR1
          FYB = DPR2
        ENDIF
        IF (IXFLG.EQ.0 .AND.((IFLAG.NE.2).OR.(LOVRLY))) THEN
          FXL = 90.
          FXR = -90.
          IF (KHEM.EQ.1) FXR = 0.
          IF(KHEM.EQ.2) THEN
             FXR = -90.
             FXL =   0.
          ENDIF
        ELSE
          FXL = DLAT1
          FXR = DLAT2
        ENDIF
C
C       * MAKE SURE THE COORDINATES ARE BOUNDED BY THE INPUT FILE RANGE.
C
        IF(IXFLG.EQ.0) THEN
         IF(DLAT1.GT.-1.*SLAT(1)) DLAT1 = -1.*SLAT(1)
         IF(DLAT2.LT.-1.*SLAT(NLAT)) DLAT2 = -1.*SLAT(NLAT)
        ENDIF
        IF (DPR1.LT.PR(1)) DPR1 = PR(1)
        IF (DPR2.GT.PR(NLEV)) DPR2 = PR(NLEV)
C
C       * SET THE COORDINATES OF THE PLOTTED AREA.
C
        PYT = DPR1
        PYB = DPR2
        PXL = DLAT1
        PXR = DLAT2
C
C       * SET THE DIMENSIONS OF THE INTERPOLATED FIELD.
C
        IF (LX.GT.99) LX=99
        IF(IXFLG.EQ.0) THEN
         NLATP = INT(FLOAT(NLAT)*(PXL-PXR)/(SLAT(NLAT)-SLAT(1)) + .5)
        ELSE
         NLATP = INT(FLOAT(NLAT)*(PXR-PXL)/(SLAT(NLAT)-SLAT(1)) + .5)
        ENDIF
        IF (NLATP.LT.6) NLATP = 6
        ISCLAT=1
        LXP = LX
        ISCLX=1
        IF (LX .LT.4) THEN
          IF (NLATP.LE.24) ISCLAT=4
          IF (NLATP.LE.33) ISCLAT=3
          IF (NLATP.LE.48) ISCLAT=2
          NLATP=ISCLAT*NLATP
          LX = 2*NLAT/3
          LXP = 2*NLATP/3
          ISCLX = ISCLAT
        ENDIF
C
C       * SET THE POSITION OF THE FRAME ON THE PLOTTER PAGE.
C
        IF (IFLAG.EQ.2) THEN
        FHGT=FSCAL*(FLOAT(LXP-1)*(FYB-FYT)/(FLOAT(NLATP-1)*(PYB-PYT)))
        ELSE
          IF (KAX.EQ.1) THEN
             DENOM = LOG(1000.)-LOG(10.)
          ELSE IF (KAX.EQ.2) THEN
             DENOM = DPR2-DPR1
          ELSE
             DENOM = 1000.-10.
          ENDIF
          FHGT=FSCAL*FLOAT(LX-1)*(FYB-FYT)/(DENOM*FLOAT(NLAT-1))
        ENDIF
        IF (IXFLG.EQ.0 .AND. ((IFLAG.NE.2).OR.(LOVRLY))) THEN
          FWDTH = FSCAL*(FXL-FXR)/(SLAT(NLAT)-SLAT(1))
          FLEFT = (1.-FWDTH)/2. + 0.025
        ELSE
          FWDTH = FSCAL
          FLEFT = .075
        ENDIF
C
C       * SET THE POSITION OF THE PLOTTED AREA ON THE PLOTTER PAGE.
C
        PHGT = FHGT*(PYB-PYT)/(FYB-FYT)
        PWDTH = FWDTH*(PXL-PXR)/(FXL-FXR)
        PLEFT = FLEFT + PWDTH*(FXL-PXL)/(PXL-PXR)
        PBOT = YBOT + PHGT*(FYB-PYB)/(PYB-PYT)
        FBOT = YBOT
        IF (IFLAG.EQ.2) FBOT = PBOT
C
C       * SET THE FRAMED AREA EQUAL TO THE PLOTTED AREA
C
        IF (IFLAG.EQ.3) THEN
          IF (.NOT.LOVRLY) THEN
            FXL = PXL
            FXR = PXR
            FWDTH = PWDTH
            FLEFT = PLEFT
          ENDIF
          FYB = PYB
          FYT = PYT
          FHGT = PHGT
          FBOT = PBOT
        ENDIF
C
C       * IF THE PLOT IS TOO TALL, SCALE IT DOWN TO FIT THE PLOTTER PAGE.
C
        IF ((FHGT+FBOT).GT.FSCAL.AND.IFLAG.EQ.2) THEN
          SCALE = FSCAL/(FHGT+FBOT)
          PHGT = SCALE*PHGT
          PWDTH = SCALE*PWDTH
          FWDTH = SCALE*FWDTH
          FHGT = FSCAL-FBOT
          PLEFT = SCALE*(PLEFT-FLEFT) + FLEFT
          PBOT = SCALE*(PBOT-FBOT) + FBOT
        ENDIF

C       * COMPUTE THE EQUALLY SPACED PRESSURE LEVELS AND LATITUDES FOR
C       * INTERPOLATION TO THE DISPLAY GRID.
C
        IF (IXFLG.NE.0) THEN
         DY = (PXR-PXL)/FLOAT(NLATP-1)
         DO 324 I=1,NLATP
  324    SLATP(I) = PXL + DY*FLOAT(I-1)
        ELSE
         DY = (PXL-PXR)/FLOAT(NLATP-1)
         DO 325 I=1,NLATP
  325    SLATP(I) = DY*FLOAT(I-1) - PXL
        ENDIF
        DP = (DPR2-DPR1)/FLOAT(LXP-1)
        DO 335 L=1,LXP
  335   PRX(L)=(L-1)*DP+DPR1
C
        IF (KIND.EQ.3) CALL LGRDC(DEN,PR,NLEV)
      ENDIF
C
C     * INTERPOLATE THE DISPLAY LEVELS ONE COLUMN AT A TIME.
C
      DO 370 J=1,NLAT
        DO 350 L=1,NLEV
          K=NLEV+1-L
          V(L)=VS(J,K)
          W(L)=WS(J,K)
  350   CONTINUE
        IF(KIND.EQ.1)THEN
          CALL LINIL(VX,PRX,LXP,V,PR,NLEV,0.,0.)
          CALL LINIL(WX,PRX,LXP,W,PR,NLEV,0.,0.)
        ENDIF
        IF(KIND.EQ.3)THEN
          CALL LGRIC(VX,PRX,LXP,V,PR,DEN,NLEV,0.,0.)
          CALL LGRIC(WX,PRX,LXP,W,PR,DEN,NLEV,0.,0.)
        ENDIF
        DO 360 L=1,LXP
          N=(L-1)*NLAT+J
          K=LXP+1-L
          VXS(N)=VX(K)
          WXS(N)=WX(K)
  360   CONTINUE
  370 CONTINUE
C
C     * INTERPOLATE HORIZONTALLY
C
      DO 372 L = 1,LXP
        DO 374 I = 1,NLAT
        VX(I) = VXS( (L-1)*NLAT + I)
  374   WX(I) = WXS( (L-1)*NLAT + I)
        CALL LINIL(YX,SLATP,NLATP,WX,SLAT,NLAT,0.,0.)
        CALL LINIL(PRX,SLATP,NLATP,VX,SLAT,NLAT,0.,0.)
        IF(IREV.NE.1)THEN
           DO 376 I = 1,NLATP
              ZXV( (L-1)*NLATP + I) = PRX(I)
              ZXW( (L-1)*NLATP + I) = YX(I)
 376       CONTINUE
        ELSE
           DO 377 I = 1,NLATP
              ZXV( (L-1)*NLATP + NLATP-I+1 ) = -PRX(I)
              ZXW( (L-1)*NLATP + NLATP-I+1 ) = YX(I)
 377       CONTINUE            
         ENDIF
 372  CONTINUE
      DO 378 I = 1,LXP*NLATP
         WXS(I) = ZXW(I)
         VXS(I) = ZXV(I)
 378  CONTINUE
C
C     * SKIP POINTS AND SCALE THE ZONALLY AVERAGED V AND W FIELDS.
C
      INCY= ISCLAT*INCY
      INCZ= ISCLX*INCZ
      LYR = NLATP
      LXR = LXP
      IF (INCY.GE.1 .AND. INCZ.GE.1) THEN
        LYR=(NLATP-1)/INCY+1
        LXR=(LXP-1)/INCZ+1
        DO 380 J=1,LXR
          JJ=(J-1)*INCY+1
          DO 380 I=1,LYR
            N=I +(J-1)*LYR
            NN=(I-1)*INCY+1 + (JJ-1)*NLATP
            IF(ABS(VXS(NN)).GE.SPVAL)THEN
               ZXV(N)=SPVAL
            ELSE
               ZXV(N)=VXS(NN)*VSCAL
            ENDIF
            IF(ABS(WXS(NN)).GE.SPVAL)THEN
               ZXW(N)=SPVAL
            ELSE
               ZXW(N)=WXS(NN)*VSCAL
            ENDIF
  380   CONTINUE
      ENDIF
C
C     * COMPENSATE FOR SKIPPED POINTS AT TOP AND RIGHT OF FIELD
C
      NRIGHT = NLATP-(LYR-1)*INCY-1
      NTOP   = LXP-(LXR-1)*INCZ-1
      DRIGHT = FLOAT(NRIGHT)*PWDTH*DY/(PXL-PXR)
      DTOP   = FLOAT(NTOP)*PHGT *DP/(PYB-PYT)
C
C    * DRAW THE VECTOR PLOT.
C      --------------------
C
      IF (PBOT+PHGT.GT.1.) THEN
        CALL                                       PXIT('ZXPLOT',-114)
      ENDIF
      XPOS = FLOAT(LYR)
      YPOS = FLOAT(LXR)
      PWDTH = PWDTH-DRIGHT
      PHGT  = PHGT -DTOP
      CALL SET(PLEFT,PLEFT+PWDTH,PBOT,PBOT+PHGT,1.,XPOS,1.,YPOS,1)

C     Find the maximum vector length (VMAX)

      VMAX = 0.0
      VMISS=1.0E+38
      DO 495 I=1,LXR*LYR
         IF((ZXW(I).LT.VMISS).AND.(ZXV(I).LT.VMISS))THEN
            VLEN = SQRT(ZXW(I)*ZXW(I) + ZXV(I)*ZXV(I))
         ENDIF
        IF (VLEN .GT. VMAX) VMAX = VLEN
  495 CONTINUE
      PRINT *,'VMAX=',VMAX

C     Calculate the multiplying factor

      IFAC = INT(VMAX/VI + 1.0)

C     Calculate the maximum vector magnitude to plot

      VMHI = IFAC * VI

C     Calculate the grid length with respect to the plotting area

      CALL GETUSV('XF',ISX)
      CALL GETSET(TXA,TXB,TYA,TYB,XC,XD,YC,YD,LTYPE)
      IGRID = INT(((TXB - TXA) * 2.**ISX + 1.) / (LXR - 1.))

C     Claculate the maximum vector length to plot

      MLEN = IFAC * IGRID

      CALL VELVCT(ZXV,LYR,ZXW,LYR,LYR,LXR,VLO,VMHI,-1,MLEN,0,SPV)

C
C     * WRITE LABEL FOR VECTOR PLOT
C
C      CALL SET(.01,.99,.01,.99, .01,.99,.01,.99, 1)
      CALL SET(0.,1.,0.,1.,0.,1.,0.,1., 1)

C     * DETERMINE THE LABEL'S POSITION ABOVE THE GRAPH.  THIS LABEL WILL
C     * BE VERTICALLY POSITIONED BETWEEN THE LABELS FOR THE FIRST AND
C     * SECOND SCALAR FIELDS.
C     * FHGT + FBOT ==> TOP OF THE GRAPH
C     * CPFY(ISIZ/2) ==> THE TOPMOST PRESSURE LABEL WILL STICK UP THIS
C     *                  MUCH ABOVE THE GRAPH
C     * (CHRATIO - 1)*CPFY(ISIZ) ==> THE AMOUNT OF WHITE SPACE NEEDED ABOVE
C     *                              THE TOPMOST PRESSURE LABEL
C
      YPOS = FHGT + FBOT + CPFY(ISIZ/2) + (CHRATIO - 1)*CPFY(ISIZ)
     1       + CPFY(ITSIZ/2) + CHRATIO*CPFY(ITSIZ)


CCCC      CALL SHORTN(LABEL,83,LEAD,TRAIL,NC)
      CALL GETUSV('LW',IOLDLW)
      CALL SETUSV('LW',MAJLW)

C     * ADJUST THE CHARACTER SIZE FOR THE TITLE, DEPENDING ON THE LENGTH.
C     * NOTE: THIS MAGIC FORMULA WAS DETERMINED ENTIRELY FROM EXPERIMENTATION.
C     * SINCE CHARACTER WIDTHS WILL VARY FOR PCHIQU, THERE IS NO REAL WAY
C     * OF DETERMINING HOW SCALING SHOULD DONE BUT I FOUND THAT THIS FORMULA
C     * USUALLY WORKS.
      IF(DOTITLE) THEN
         CHSIZE=1.0/90.0
         CALL PCHIQU(0.525,YPOS,
     1        ALABEL(LSTRBEG(ALABEL):LSTREND(ALABEL)),CHSIZE,0,0)
      ENDIF

      CALL SETUSV('LW',IOLDLW)
C
      WRITE(6,6050) ALABEL
C-----------------------------------------------------------------------
C

  700 CONTINUE

C
C     * LABEL THE LEFT-HAND AND RIGHT-HAND LATITUDES OF THE FRAME.
C
C     * FLAB = Y OFFSET FOR LATITUDE LABELS
      FLAB = FBOT - CPFX(ISIZ + 8)
      YLABLAT = FBOT - 2.5 * CPFX(ISIZ + 8)
      WRITE(NORTH,'(A1)') 'N' 
      WRITE(SOUTH,'(A1)') 'S'
      WRITE(EQ,'(A2)') 'EQ'
C
      CALL GETUSV('LW',IOLDLW)
      IF (.NOT.PUB) THEN
C       * USE MINOR LINE WIDTH WHEN DRAWING LABELS
        CALL SETUSV('LW',MINLW)
        FXLFT=FXL
        FXRGT=FXR
        IF(IXFLG.EQ.0.AND.IREV.EQ.1) THEN
           FXLFT=FXR
           FXRGT=FXL
        ENDIF
        WRITE(IPNT2, '(I4,3X)') INT(FXLFT)
        CALL SHORTN(IPRT, 7, .TRUE., .TRUE., NC)
        IF (INDEX(IPNT2, '-') .NE. 0) NC = NC + 1
        CALL AGPWRT(FLEFT,FLAB,IPNT2(4:NC),NC-3,ISIZ,0,0)
        IF(XLAB.EQ.0) THEN
           IF(INT(FXLFT).LT.0) THEN
              CALL AGPWRT(FLEFT,YLABLAT,SOUTH,1,ISIZ,0,0)
           ELSEIF(INT(FXLFT).GT.0)THEN
              CALL AGPWRT(FLEFT,YLABLAT,NORTH,1,ISIZ,0,0)
           ELSE
              CALL AGPWRT(FLEFT,YLABLAT,EQ,2,ISIZ,0,0)
           ENDIF
        ENDIF
C        
        WRITE(IPNT2, '(I4,3X)') INT(FXRGT)
        CALL SHORTN(IPRT, 7, .TRUE., .TRUE., NC)
        IF (INDEX(IPNT2, '-') .NE. 0) NC = NC + 1
        CALL AGPWRT(FLEFT+FWDTH,FLAB,IPNT2(4:NC),NC-3,ISIZ,0,0)
        IF(XLAB.EQ.0) THEN
           IF(INT(FXRGT).LT.0) THEN
              CALL AGPWRT(FLEFT+FWDTH,YLABLAT,SOUTH,1,ISIZ,0,0)
           ELSEIF(INT(FXRGT).GT.0)THEN
              CALL AGPWRT(FLEFT+FWDTH,YLABLAT,NORTH,1,ISIZ,0,0)
           ELSE
              CALL AGPWRT(FLEFT+FWDTH,YLABLAT,EQ,2,ISIZ,0,0)
           ENDIF
        ENDIF
        CALL SETUSV('LW',IOLDLW)

        CALL FRSTPT(FLEFT,FBOT-CPFX(8))
        CALL VECTOR(FLEFT,FBOT)
        CALL FRSTPT(FLEFT+FWDTH,FBOT-CPFX(8))
        CALL VECTOR(FLEFT+FWDTH,FBOT)
      ENDIF
C
C     * DETERMINE THE SPACING OF LAT TICKS (INTT) AND LABELS (INTL).
C
      IF (IXFLG.EQ.0) THEN
        RANGE = FXL-FXR
        NPNTS = 90
      ELSE
        RANGE = FXR-FXL
        NPNTS = 3+MAX(ABS(FXL),ABS(FXR))
      ENDIF
      IF (RANGE.LE.10.) INTT = 1
      IF (RANGE.GT.10..AND.RANGE.LE.20.) INTT = 2
      IF (RANGE.GT.20..AND.RANGE.LE.50.) INTT = 5
      IF (RANGE.GT.50..AND.RANGE.LE.100.) INTT = 10
      IF (RANGE.GT.100..AND.RANGE.LT.180.) INTT = 15
      INTL = INTT
      IF (RANGE.EQ.180.) INTL = 30
      IF (RANGE.EQ. 90.) INTL = 15
      IF (RANGE.EQ.180.) INTT = 10
      IF (RANGE.EQ. 90.) INTT =  5
C
C     * WRITE LATITUDES AT BOTTOM OF FRAME.
C
      CALL SETUSV('LW',MINLW)
      DO 710 I=0,NPNTS,INTL

C       * WRITE POSITIVE LATITUDES
        IF((IXFLG.EQ.0.AND.(FLOAT(I).LE.FXL.AND.FLOAT(I).GE.FXR)).OR.
     1     (IXFLG.NE.0.AND.(FLOAT(I).GE.FXL.AND.FLOAT(I).LE.FXR)))THEN
           IF(IXFLG.NE.0.OR.IREV.NE.1) THEN
              XPOS = FLEFT + FWDTH*(FLOAT(I)-FXL)/(FXR-FXL)
           ELSEIF(IXFLG.EQ.0.AND.IREV.NE.1) THEN
              XPOS = FLEFT + FWDTH*(FXL-FLOAT(I))/(FXL-FXR)
           ELSEIF(IXFLG.EQ.0.AND.IREV.EQ.1)THEN
           XPOS = FLEFT + FWDTH*(1.-(FXL-FLOAT(I))/(FXL-FXR))
           ENDIF
          IF(PUB.OR.(XPOS-FLEFT.GT..02.AND.FLEFT+FWDTH-XPOS.GT..02))THEN
            WRITE(IPNT2,'(I4,3X)') I
            CALL SHORTN(IPRT, 7, .TRUE., .TRUE., NC)
            CALL AGPWRT(XPOS,FLAB,IPNT2(4:NC),NC-3,ISIZ,0,0)
          ENDIF
        ENDIF

C       * WRITE NEGATIVE LATITUDES
        IF(I.NE.0.AND.(
     1      (IXFLG.EQ.0.AND.
     2       (FLOAT(-1*I).LE.FXL.AND.FLOAT(-1*I).GE.FXR)).OR.
     3      (IXFLG.NE.0.AND.
     4       (FLOAT(-1*I).GE.FXL.AND.FLOAT(-1*I).LE.FXR)))) THEN
          IF(IXFLG.NE.0.OR.IREV.NE.1) THEN
           XPOS = FLEFT + FWDTH*(FLOAT(-1*I)-FXL)/(FXR-FXL)
          ELSEIF(IXFLG.EQ.0.AND.IREV.NE.1) THEN
           XPOS = FLEFT + FWDTH*(FXL-FLOAT(-1*I))/(FXL-FXR)
          ELSEIF(IXFLG.EQ.0.AND.IREV.EQ.1)THEN
           XPOS = FLEFT + FWDTH*(1.-(FXL-FLOAT(-1*I))/(FXL-FXR))
          ENDIF
          IF(PUB.OR.(XPOS-FLEFT.GT..02.AND.FLEFT+FWDTH-XPOS.GT..02))THEN
            WRITE(IPNT2,'(I4,3X)') -1*I
            CALL SHORTN(IPRT, 7, .TRUE., .TRUE., NC)
            IF (INDEX(IPNT2, '-') .NE. 0) NC = NC + 1
            CALL AGPWRT(XPOS,FLAB,IPNT2(4:NC),NC-3,ISIZ,0,0)
          ENDIF
        ENDIF

  710 CONTINUE
      CALL SETUSV('LW',IOLDLW)

C
C     * WRITE LAT TICKS AT THE BOTTOM OF THE FRAME.
C
      DO 720 I=0,NPNTS,INTT
        IF((IXFLG.EQ.0.AND.(FLOAT(I).LE.FXL.AND.FLOAT(I).GE.FXR)).OR.
     1     (IXFLG.NE.0.AND.(FLOAT(I).GE.FXL.AND.FLOAT(I).LE.FXR)))THEN
          IF(IXFLG.NE.0.OR.IREV.NE.1) THEN
           XPOS = FLEFT + FWDTH*(FLOAT(I)-FXL)/(FXR-FXL)
          ELSEIF(IXFLG.EQ.0.AND.IREV.NE.1) THEN
           XPOS = FLEFT + FWDTH*(FXL-FLOAT(I))/(FXL-FXR)
          ELSEIF(IXFLG.EQ.0.AND.IREV.EQ.1)THEN
           XPOS = FLEFT + FWDTH*(1.-(FXL-FLOAT(I))/(FXL-FXR))
          ENDIF
          IF(PUB.OR.(XPOS-FLEFT.GT..02.AND.FLEFT+FWDTH-XPOS.GT..02))THEN
            CALL FRSTPT(XPOS,FBOT-CPFX(8))
            CALL VECTOR(XPOS,FBOT)
          ENDIF
        ENDIF
        IF(I.NE.0.AND.(
     1      (IXFLG.EQ.0.AND.
     2       (FLOAT(-1*I).LE.FXL.AND.FLOAT(-1*I).GE.FXR)).OR.
     3      (IXFLG.NE.0.AND.
     4       (FLOAT(-1*I).GE.FXL.AND.FLOAT(-1*I).LE.FXR)))) THEN
          IF(IXFLG.NE.0.OR.IREV.NE.1) THEN
           XPOS = FLEFT + FWDTH*(FLOAT(-1*I)-FXL)/(FXR-FXL)
          ELSEIF(IXFLG.EQ.0.AND.IREV.NE.1) THEN
           XPOS = FLEFT + FWDTH*(FXL-FLOAT(-1*I))/(FXL-FXR)
          ELSEIF(IXFLG.EQ.0.AND.IREV.EQ.1)THEN
           XPOS = FLEFT + FWDTH*(1.-(FXL-FLOAT(-1*I))/(FXL-FXR))
          ENDIF
          IF(PUB.OR.(XPOS-FLEFT.GT..02.AND.FLEFT+FWDTH-XPOS.GT..02))THEN
            CALL FRSTPT(XPOS,FBOT-CPFX(8))
            CALL VECTOR(XPOS,FBOT)
          ENDIF
        ENDIF
  720 CONTINUE
C
C     * LABEL THE HIGHEST PRESSURE (I.E. HIGHEST ABOVE THE GROUND).
C
C     IF (.NOT.PUB) THEN
C          ITOP = INT(FYT+.5)
C          IF (KAX.EQ.1) ITOP = INT(EXP(FYT)+.5)
C
C          CALL SETUSV('LW',MINLW)
C          WRITE(IPNT,'(I4)') ITOP
C          CALL AGPWRT(FLEFT-CPFX(4),FBOT+FHGT,IPNT,4,ISIZ,0,1)
C          CALL SETUSV('LW',IOLDLW)
C
C          OLDY = FBOT + FHGT
C     ELSE
C          OLDY = 1.E36
C     ENDIF
      IF (.NOT.PUB) THEN
         IF(FYT.GT.0..OR.KAX.EQ.0)THEN
           IF (KAX.EQ.1) THEN
              ITOP = INT(EXP(FYT)+.5)
           ELSE
              ITOP = INT(FYT+.5)
           ENDIF
           WRITE(IPNT,'(I4)') ITOP
C
         ELSE
C
           FYTEMP=EXP(FYT)*1.00001
           IF(FYTEMP.GE..1)THEN
             WRITE(IPNT,'(F4.1)')FYTEMP
           ELSE
             IF(FYTEMP.GE..01)THEN
               WRITE(IPNT,'(F4.2)')FYTEMP
             ELSE
               WRITE(IPNT,'(F4.3)')FYTEMP
             ENDIF
           ENDIF
C
        ENDIF

           CALL SETUSV('LW',MINLW)
           CALL AGPWRT(FLEFT-CPFX(4),FBOT+FHGT,IPNT,4,ISIZ,0,1)
           CALL SETUSV('LW',IOLDLW)

           OLDY = FBOT + FHGT
      ELSE
           OLDY = 1.E36
      ENDIF


C
C     * CALCULATE THE YPOS OF THE LOWEST PRESSURE LEVEL TO ENSURE
C     * THAT IT IS ALWAYS LABELED.  NOTE: LOWEST MEANS CLOSEST TO THE
C     * GROUND.
      FLBOT = FBOT + FHGT * (PR(NLEV) - FYB) / (FYT - FYB)


C     * DRAW ALL OF THE TICK MARKS AND LABELS EXCEPT FOR THE
C     * LOWEST ONE.
C      DO 730 L=1, NLEV - 1
C           IF (.NOT. (PR(L).GT.FYB.OR.PR(L).LT.FYT)) THEN
CC
CC               * YPOS = SCREEN HEIGHT OF THE CURRENT PRESSURE LEVEL.
C                YPOS=FBOT+FHGT*(PR(L)-FYB)/(FYT-FYB)
CC
CC               *  DRAW A TICK ON EACH SIDE OF THE PLOT.
CC
C                IF (PUB.OR.(FHGT+FBOT-YPOS.GE..01)) THEN
C                     CALL FRSTPT(FLEFT,YPOS)
C                     CALL VECTOR(FLEFT+CPFX(8),YPOS)
C                     CALL FRSTPT(FLEFT+FWDTH-CPFX(8),YPOS)
C                     CALL VECTOR(FLEFT+FWDTH,YPOS)
C
CC
CC                    * SET IPNT TO THE FOUR DIGITS OF THE PRESSURE
CC                    * AND WRITE THE LABEL NEXT TO THE TICK MARK AS LONG
CC                    * AS IT DOESN'T OVERLAP WITH PREVIOUS LABELS.
C                     IF (OLDY - YPOS .GT. CHRATIO * CPFY(ISIZ) .AND.
C     +                   YPOS - FLBOT .GT. CHRATIO * CPFY(ISIZ)) THEN
C
C                          CALL SETUSV('LW',MINLW)
C
C                          WRITE(IPNT,'(I4)') LEV(L)
C                          CALL AGPWRT(FLEFT-CPFX(4),YPOS,IPNT,4,ISIZ,
C     +                                0,1)
C
C                          OLDY = YPOS
C                          CALL SETUSV('LW',IOLDLW)
C                     ENDIF
C                ENDIF
C           ENDIF
C  730 CONTINUE

C     * DRAW ALL OF THE TICK MARKS AND LABELS EXCEPT FOR THE
C     * LOWEST ONE.
      DO 730 L=1, NLEV - 1
           IF (.NOT. (PR(L).GT.FYB.OR.PR(L).LT.FYT)) THEN
C
C               * YPOS = SCREEN HEIGHT OF THE CURRENT PRESSURE LEVEL.
                YPOS=FBOT+FHGT*(PR(L)-FYB)/(FYT-FYB)
C
C               *  DRAW A TICK ON EACH SIDE OF THE PLOT.
C
                IF (PUB.OR.(FHGT+FBOT-YPOS.GE..01)) THEN
                     CALL FRSTPT(FLEFT,YPOS)
                     CALL VECTOR(FLEFT+CPFX(8),YPOS)
                     CALL FRSTPT(FLEFT+FWDTH-CPFX(8),YPOS)
                     CALL VECTOR(FLEFT+FWDTH,YPOS)

C
C                    * SET IPNT TO THE FOUR DIGITS OF THE PRESSURE
C                    * AND WRITE THE LABEL NEXT TO THE TICK MARK AS LONG
C                    * AS IT DOESN'T OVERLAP WITH PREVIOUS LABELS.
                     IF (OLDY - YPOS .GT. CHRATIO * CPFY(ISIZ) .AND.
     1                   YPOS - FLBOT .GT. CHRATIO * CPFY(ISIZ)) THEN

                          CALL SETUSV('LW',MINLW)
C
                          IF(LEV(L).GT.0)THEN
C
                             IF(KAX.EQ.2) THEN
                                WRITE(IPNT,'(I6)') INT(LEV(L)/10.0)
                             ELSE
                                WRITE(IPNT,'(I6)') LEV(L)
                             ENDIF
                          ELSE
                            YTEMP=PR(L)
                            IF(KAX.EQ.1)YTEMP=EXP(YTEMP)
                            IF(YTEMP.GE..1)THEN
                              WRITE(IPNT,'(F6.1)')YTEMP
                            ELSE
                              IF(YTEMP.GE..01)THEN
                                WRITE(IPNT,'(F6.2)')YTEMP
                              ELSE
                                WRITE(IPNT,'(F6.3)')YTEMP
                              ENDIF
                           ENDIF
                          ENDIF

                          CALL AGPWRT(FLEFT-CPFX(6),YPOS,IPNT,6,ISIZ,
     1                                0,1)
C

                          OLDY = YPOS
                          CALL SETUSV('LW',IOLDLW)
                     ENDIF
                ENDIF
           ENDIF
  730 CONTINUE
C
C     * DRAW THE LAST TICK MARK (IF NEEDED) AND LABEL IT.
C
      IF (PR(NLEV) .LT. FYB) THEN
           CALL FRSTPT(FLEFT,FLBOT)
           CALL VECTOR(FLEFT+CPFX(8),FLBOT)
           CALL FRSTPT(FLEFT+FWDTH-CPFX(8),FLBOT)
           CALL VECTOR(FLEFT+FWDTH,FLBOT)
      ENDIF

      CALL SETUSV('LW',MINLW)
      IF(KAX.EQ.2) THEN
         WRITE(IPNT,'(I6)') INT(LEV(NLEV)/10.0)
      ELSE
         WRITE(IPNT,'(I6)') LEV(NLEV)
      ENDIF
      CALL AGPWRT(FLEFT - CPFX(6), FLBOT, IPNT, 6, ISIZ, 0, 1)
      CALL SETUSV('LW',IOLDLW)


C     * DRAW THE FRAME AROUND THE PLOT
C
      IF (FBOT+FHGT.GT.1.) THEN
        CALL                                       PXIT('ZXPLOT',-115)
      ENDIF
      CALL FRSTPT(FLEFT, FBOT)
      CALL VECTOR(FLEFT + FWDTH, FBOT)
      CALL VECTOR(FLEFT + FWDTH, FBOT + FHGT)
      CALL VECTOR(FLEFT, FBOT + FHGT)
      CALL VECTOR(FLEFT, FBOT)
C      CALL SET(.01,.99,.01,.99, .01,.99,.01,.99, 1)
      CALL SET(0.,1.,0.,1.,0.,1.,0.,1., 1)
C
C     * THE LAST CHARACTER OF THE PLOT IS AN X IN THE UPPER RIGHT CORNER
C
CCCC      CALL PWRITY(.97,.97,'X',1,0,0,0)
C
C     * LAST LINE DRAWN
C
      CALL FRAME
      GO TO 150

C     * E.O.F. ON INPUT.

  900 CONTINUE
      CALL                                         PXIT('ZXPLOT',0)

C     * E.O.F. ON FILE Z.

  901 CALL                                         PXIT('ZXPLOT',-116)

C     * OTHER IMPROPER EXITS.

  902 CALL                                         PXIT('ZXPLOT',-117)
  903 CALL                                         PXIT('ZXPLOT',-118)
  904 CALL                                         PXIT('ZXPLOT',-119)
  905 CALL                                         PXIT('ZXPLOT',-120)
  906 CALL                                         PXIT('ZXPLOT',-121)
  907 CALL                                         PXIT('ZXPLOT',-122)
  908 CALL                                         PXIT('ZXPLOT',-123)
  909 CALL                                         PXIT('ZXPLOT',-124)
C---------------------------------------------------------------------

  992 FORMAT(I7)
 2000 FORMAT(1P1E10.1)
 2002 FORMAT('0')
 2030 FORMAT(F6.1)
 2031 FORMAT(F7.2)
 2032 FORMAT(F8.3)
C
 5010 FORMAT(10X,1X,A4,I5,2I1,I3,I2,I3,4E10.0,I5,I1,2I2)                        
 5011 FORMAT(10X,1X,A4,I5,2I1,I3,I5,4E10.0,I5,1X,2I2)                           
 5012 FORMAT(80A1)                                                              
 5014 FORMAT(10X,I1,I4,7I5)                                                     
 5015 FORMAT(15X,7I5)                                                           
 5016 FORMAT(10X,7E10.0)                                                        
 5018 FORMAT(10X,4E10.0,2I5)                                                    
 5020 FORMAT(10X,3E10.0,2I5)                                                    
 6010 FORMAT('0..EOF LOOKING FOR  ',A4)
 6020 FORMAT('0NAME,MODEZX,ICOSH1,ICOSH2,MS,LX,SCAL,FLO,HI,FINCN,KAX',
     1 ',IOVRLY,KIND',/,1X,A4,1X,I3,I6,I7,2I3,4(1X,E10.4),3I2)
 6025 FORMAT(' ',A4,I10,2X,A4,5I6)
 6041 FORMAT('0 CROSS-SECTION   ',80A1)
 6050 FORMAT('  VECTOR PLOT     ',A80)
 6060 FORMAT('0DLAT1,DLAT2,IXFLG=',2E10.4,I5)
 6090 FORMAT(/,' *** WARNING: PACKING DENSITY = ', I4,' ***',/)
 6099 FORMAT('LATITUDE')
 6100 FORMAT('PRESSURE')
 6102 FORMAT('OCEAN DEPTH')
      END
