      PROGRAM VSINT 
C     PROGRAM VSINT (XIN,       XOUT,       INPUT,       OUTPUT,        )       J2
C    1         TAPE1=XIN, TAPE2=XOUT, TAPE5=INPUT, TAPE6=OUTPUT)
C     ----------------------------------------------------------                J2
C                                                                               J2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2
C     JAN 14/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     FEB 25/85 - B.DUGAS                                                       
C                                                                               J2
CVSINT   - VERTICAL SIGMA INTEGRAL OF A FILE (FULL OR HALF LEVELS)      1  1 C  J1
C                                                                               J3
CAUTHOR  - J.D.HENDERSON                                                        J3
C                                                                               J3
CPURPOSE - VERTICAL INTEGRATION OF A FILE OF SETS ON SIGMA LEVELS               J3
C          NOTE - MINIMUM NUMBER OF LEVELS IS 3, MAX IS $L$.                    J3
C                                                                               J3
CINPUT FILE...                                                                  J3
C                                                                               J3
C      XIN  = FILE OF SIGMA LEVEL SETS ON EITHER FULL OR HALF LEVELS.           J3
C                                                                               J3
COUTPUT FILE...                                                                 J3
C                                                                               J3
C      XOUT = OUTPUT SERIES OF SIGMA VERTICAL INTEGRALS OF XIN.                 J3
C 
CINPUT PARAMETERS...
C                                                                               J5
C      SIGTYP = 4 CHARACTER CODE FOR TYPE OF SIGMA LEVELS.                      J5
C              (4HFULL FOR FULL LEVELS, 4HHALF FOR HALF LEVELS)                 J5
C      CONST  = MULTIPLICATIVE CONSTANT FOR THE INTEGRALS.                      J5
C                                                                               J5
CEXAMPLE OF INPUT CARD...                                                       J5
C                                                                               J5
C*VSINT    FULL       1.0                                                       J5
C---------------------------------------------------------------------------
C 
  
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/GG(SIZES_LONP1xLAT),ACC(SIZES_LONP1xLAT)
  
      LOGICAL OK,SPEC 
      INTEGER LEV(SIZES_MAXLEV)
      REAL SF(SIZES_MAXLEV),SH(SIZES_MAXLEV),DS(SIZES_MAXLEV)
  
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_MAXLEV/ 
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
  
C     * READ SIGMA LEVEL TYPE (FULL OR HALF) AND THE SCALING FACTOR.
  
      READ(5,5010,END=900) SIGTYP,CONST                                         J4
      WRITE(6,6005) SIGTYP,CONST
  
C     * GET SIGMA VALUES FROM XIN.
  
      CALL FILEV(LEV,NLEV,IBUF,1) 
      IF(NLEV.EQ.0) CALL                           XIT('VSINT',-1)
      WRITE(6,6007) NLEV,(LEV(L),L=1,NLEV)
      IF(NLEV.LT.3.OR.NLEV.GT.MAXL) CALL           XIT('VSINT',-2)
      NLEVM=NLEV-1
      CALL LVDCODE(SF,LEV,NLEV)
      DO 155 L=1,NLEV 
         SF(L)=SF(L)*0.001E0 
  155 CONTINUE
  
C     * COMPUTE THE INTEGRATION INCREMENTS DS 
  
      IF (SIGTYP.EQ.NC4TO8("HALF"))   THEN
  
C         * FOR HALF LEVELS, COMPUTE FULL LEVELS FIRST. 
  
          DO 160 L=1,NLEV 
             SH(L)=SF(L)
  160     CONTINUE
          SF(NLEV)=SH(NLEV)**2
          DO 165 L=NLEVM,1,-1 
             SF(L)=SH(L)**2/SF(L+1) 
  165     CONTINUE
          DO 170 L=1,NLEVM
             DS(L)=SF(L+1)-SF(L)
  170     CONTINUE
          DS(NLEV)=1.E0-SF(NLEV)
  
      ELSE
  
C         * FOR FULL LEVELS, COMPUTE HALF LEVELS FIRST. 
  
          DO 175 L=1,NLEVM
             SH(L)=SQRT(SF(L)*SF(L+1))
  175     CONTINUE
          SH(NLEV)=SQRT(SF(NLEV)) 
          DS(1)=SH(1) 
          DO 180 L=2,NLEVM
             DS(L)=SH(L)-SH(L-1)
  180     CONTINUE
          DS(NLEV)=1.E0-SH(NLEVM) 
  
      ENDIF 
  
C     * DETERMINE THE FIELD SIZE. 
  
      KIND=IBUF(1)
      NAME=IBUF(3)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
  
C---------------------------------------------------------------------
C     * READ THE NEXT SET AND COMPUTE SIGMA VALUES. 
  
      NSETS=0 
  185 CALL GETFLD2(1,ACC,KIND,-1,NAME,LEV(1),IBUF,MAXX,OK) 
      IF( .NOT.OK )   THEN
          WRITE(6,6025) JBUF
          WRITE(6,6020) NSETS 
          CALL                                     XIT(' VSINT',0)
      ENDIF 
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF 
      ITIM=IBUF(2)
  
C     * INTEGRATE IN SIGMA. 
  
      C=CONST*DS(1) 
      DO 190 I=1,NWDS 
         ACC(I)=ACC(I)*C
  190 CONTINUE
  
      DO 200 L=2,NLEV 
  
         C=CONST*DS(L)
         CALL GETFLD2(1,GG,KIND,ITIM,NAME,LEV(L),JBUF,MAXX,OK) 
         IF( .NOT.OK ) CALL                        XIT(' VSINT',-4-L) 
         DO 195 I=1,NWDS
            ACC(I)=ACC(I)+GG(I)*C 
  195    CONTINUE 
  200 CONTINUE
  
C     * PUT THE RESULT ONTO FILE 2. 
  
      JBUF(4)=1 
      CALL PUTFLD2(2,ACC,JBUF,MAXX)
      NSETS=NSETS+1 
      GO TO 185 
  
C     * E.O.F. ON INPUT.
  
  900 CALL                                         XIT('VSINT',-3)
C---------------------------------------------------------------------
 5010 FORMAT(11X,A4,E10.0)                                                      J4
 6005 FORMAT('0 VSINT  ',A4,1PE12.4)
 6007 FORMAT('0NLEV,SIGMA =',I3,2X,20I5/(18X,20I5))
 6020 FORMAT('0',I6,' SETS WERE PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
