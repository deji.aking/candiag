      program pakgcm9_gaschem

!     PROGRAM PAKGCM9 (OUTGCM,       NPAKSS,       NPAKGS,       NPAKEN,     
!    1                                                           OUTPUT,)    
!    2           TAPE1=OUTGCM,TAPE21=NPAKSS,TAPE22=NPAKGS,TAPE23=NPAKEN,     
!    3                                                     TAPE6=OUTPUT)     
!     ------------------------------------------------------------------     
!                                                                           
!     NOV 11/18 - S.KHARIN (DO NO PRINT IBUF IF NO RECORDS ARE FOUND).       
!     JUL 24/13 - K.VONSALZEN (USE npack = 1 FOR "PLA" VARIABLES, AND CHANGE   
!                              DEFAULT NPACK=4 TO npack = 2)                   
!     JUL 24/13 - K.VONSALZEN PREVIOUS VERSION PAKGCM8.
!                                                                            
!PAKGCM9 - SAVES SPECTRAL, GRID AND ENERGY OUTPUT FROM THE HYBRID GCM   1  3 
!                                                                            
!AUTHOR  - R. LAPRISE                                                        
!                                                                            
!PURPOSE - SEPARATES AND PACKS THE THREE KINDS OF HYBRID GCM RUN OUTPUT     
!          DATA (SPECTRAL, GRID, ENERGY) INTO THEIR RESPECTIVE HISTORY FILES.
!                                                                            
!          NOTE - SPECTRAL AND GRID FIELDS ARE PACKED, ENERGIES ARE NOT PACKED.
!                 "NPAKEN" FILE IS GENERATED ONLY IF THE CORRESPONDING FILE   
!                 FOR IT IS SPECIFIED ON THE PROGRAM CALL.                   
!                 GRID FIELDS ARE CONVERTED FROM MODEL INTERNAL CHAINED     
!                 S-N ALTERNATING QUARTETS TO REGULAR (S-N) ORDERING PRIOR 
!                 TO BEING WRITTEN.                                       
!                 SIMILARILY, SPEC FIELDS ARE CONVERTED FROM HIGH-LOW    
!                 ZONAL WAVENUMBER PAIRS INTO USUAL TRIANGULAR ORDER.   
!                                                                      
!INPUT FILE(S)...                                                     
!                                                                    
!      OUTGCM       = FILE CONTAINING ALL FIELDS SAVED BY THE LAST JOB OF THE
!                     HYBRID GCM.                                           
!                                                                          
!OUTPUT FILES...                                                          
!                                                                        
!      NPAKSS,GS,EN = PACKED HISTORY FILES OF SPECTRAL, GRID AND ENERGY WITH
!                     LATEST COMPUTED HYBRID GCM RUN FIELDS APPROPRIATELY  
!                     SEPARATED INTO THEM.                                
!                     (IF NOT EMPTY, ANY EXISTING RECORDS WILL GET OVERWRITTEN)
!-------------------------------------------------------------------------    

      use diag_sizes, only : sizes_lmtp1,
     &                       sizes_lonp1,
     &                       sizes_maxlev,
     &                       sizes_ntrac,
     &                       sizes_nwordio

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      integer, parameter :: MAXX =
     & (SIZES_LONP1+1)*SIZES_MAXLEV*(4+SIZES_NTRAC)*SIZES_NWORDIO

      integer lsr(2,sizes_lmtp1+1)
      logical ok

      integer kt(3),nrec(3),npak(3),ibufs(8,3)

      common/blanck/ f((sizes_lonp1+1)*sizes_maxlev*(4+sizes_ntrac)),
     & g((sizes_lonp1+1)*sizes_maxlev*(4+sizes_ntrac))
      common/bufcom/ ibuf(8),idat(maxx)
!----------------------------------------------------------------------

      kt(1) = nc4to8("SPEC")
      kt(2) = nc4to8("GRID")
      kt(3) = nc4to8("ENRG")

      nfil = 5
      call jclpnt (nfil,1,21,22,23,6)

      ! * Returned by JCLPNT, then adjust NKIND variable accordingly.
      if (nfil < 4) call xit('pakgcm9',-1)

      if (nfil < 5) then
        nkind = 2
      else
        nkind = 3
      endif

      ! * Setup for each KIND and its respective output file.
      do n = 1, nkind
        nrec(n) = 0
        npak(n) = 20 + n
        rewind npak(n)
      enddo

      ! * Process the input data records.
      rewind 1
      nrecs = 0
      nsp = 0
 
      ! * Process the input fields and write selected ones to the appropriate
      ! * output file.
  210 call recget(1,-1,-1,-1,-1,ibuf,maxx,ok)
      if (.not. ok) goto 400
      nrecs = nrecs + 1

      ! ****************************
      ! * Skip "DATA" named records.
      ! ****************************
      if (ibuf(3) == nc4to8("DATA")) goto 210
 
      ! *****************************************************
      ! * Check "ENRG" kind records and process if requested.
      ! *****************************************************

      if (ibuf(1) == kt(3)) then
        n = 3
        if (nkind < 3) then
          ! * Skip if ENRG is not asked for.
          goto 210
        else
          nrec(n) = nrec(n)+1
          ! * Set default packing density for matching records.
          npack = 1
 
          ! * Unpack the field only if the packing density is changed. Otherwise
          ! * write the read data out without invoking the packer routines.
          if (ibuf(8) /= npack) then

          ! * "unpack" the field
            call recup2(f,ibuf)
            ibuf(8) = npack
            goto 270

          else
            ! * Preserve the label of the first matching record to be displayed
            ! * at the end to the standard output file.
            if (nrec(n) == 1 ) then
              do i = 1, 8
                ibufs(i,n) = ibuf(i)
              enddo
            endif
 
            call recput(npak(n),ibuf)
            goto 210
          endif
        endif
      endif
 
      ! ********************
      ! * SPEC kind records
      ! ********************

      if (ibuf(1) == kt(1)) then
        n = 1
        nrec(n) = nrec(n)+1

        ! Set default packing density for matching records. Calculate spectral
        ! triangle information (invariant, so only need to do this once).
        npack = 2
        if (nsp == 0) call dimgt(lsr,la,lr,lm,ktr,ibuf(7))
        nsp = 1

        ! Convert ordered low-high zonal wavenumber pairs to usual spectral
        ! triangle order, ie (1,lm,2,lm-1...) -> (1,2,...lm-1,lm).
        call recup2(g,ibuf)
        ibuf(8) = npack
        call rec2tri (f,g,la,lsr,lm)

        if (ibuf(3) == nc4to8("LNSP")) then
          ! * Convert log of surface pressure LNSP from Pa to mb.
          ibuf(4) = 1
          f(1) = f(1) - log(100.e0)*sqrt(2.e0)
        endif
        goto 270
      endif
 
      ! **********************************************************************
      ! * Other records, including "GRID" kind, not matching any of the above.
      ! **********************************************************************
      n = 2
      nrec(n) = nrec(n) + 1

      ! Set default packing density for matching records.
      npack = 2
      name = ibuf(3)

      if (name==nc4to8("PVEG") .or. name==nc4to8("SVEG") .or.
     &    name==nc4to8("SOIL") .or. name==nc4to8("PBLT") .or.
     &    name==nc4to8("  GC") .or. name==nc4to8(" TCV") .or.
     &    name==nc4to8("WTAB") .or. name==nc4to8("SUNL")) npack = 1

      if (name==nc4to8("CF01") .or. name==nc4to8("CF02") .or.
     &    name==nc4to8("CF03") .or. name==nc4to8("CF04")) npack = 1

      if (name==nc4to8("OT01") .or. name==nc4to8("OT02") .or.
     &    name==nc4to8("OT03") .or. name==nc4to8("OT04") .or.
     &    name==nc4to8("OT05") .or. name==nc4to8("OT06") .or.
     &    name==nc4to8("OT07") .or. name==nc4to8("OT08") .or.
     &    name==nc4to8("OT09") .or. name==nc4to8("OT10") .or.
     &    name==nc4to8("OT11") .or. name==nc4to8("OT12") .or.
     &    name==nc4to8("OT13") .or. name==nc4to8("OT14") .or.
     &    name==nc4to8("OT15") .or. name==nc4to8("OT16") .or.
     &    name==nc4to8("OT17") .or. name==nc4to8("OT18") .or.
     &    name==nc4to8("OT19") .or. name==nc4to8("OT20")) npack = 1

      if (name==nc4to8("OT21") .or. name==nc4to8("OT22") .or.
     &    name==nc4to8("OT23") .or. name==nc4to8("OT24") .or.
     &    name==nc4to8("OT25") .or. name==nc4to8("OT26") .or.
     &    name==nc4to8("OT27") .or. name==nc4to8("OT28") .or.
     &    name==nc4to8("OT29") .or. name==nc4to8("OT30") .or.
     &    name==nc4to8("OT31") .or. name==nc4to8("OT32") .or.
     &    name==nc4to8("OT33") .or. name==nc4to8("OT34") .or.
     &    name==nc4to8("OT35") .or. name==nc4to8("OT36") .or.
     &    name==nc4to8("OT37") .or. name==nc4to8("OT38") .or.
     &    name==nc4to8("OT39") .or. name==nc4to8("OT40")) npack = 1

      if (name==nc4to8("OT41") .or. name==nc4to8("OT42") .or.
     &    name==nc4to8("OT43") .or. name==nc4to8("OT44") .or.
     &    name==nc4to8("OT45") .or. name==nc4to8("OT46") .or.
     &    name==nc4to8("OT47") .or. name==nc4to8("OT48") .or.
     &                               name==nc4to8("OT49")) npack = 1

      if (name==nc4to8(" PCP") .or. name==nc4to8("  GT") .or.
     &    name==nc4to8(" SIC") .or. name==nc4to8(" SNO") .or.
     &    name==nc4to8(" HFS") .or. name==nc4to8(" QFS") .or.
     &    name==nc4to8(" UFS") .or. name==nc4to8("OUFS") .or.
     &    name==nc4to8(" VFS") .or. name==nc4to8("OVFS") .or.
     &    name==nc4to8(" BEG") .or. name==nc4to8("OBEG") .or.
     &    name==nc4to8(" BWG") .or. name==nc4to8("OBWG") .or.
     &    name==nc4to8("  FN") .or. name==nc4to8("  ZN")) npack = 2

      if (name==nc4to8("VOAE") .or. name==nc4to8("VBCE") .or.
     &    name==nc4to8("VASE") .or. name==nc4to8("VMDE") .or.
     &    name==nc4to8("VSSE") .or. name==nc4to8("VOAW") .or.
     &    name==nc4to8("VBCW") .or. name==nc4to8("VASW") .or.
     &    name==nc4to8("VMDW") .or. name==nc4to8("VSSW") .or.
     &    name==nc4to8("VOAD") .or. name==nc4to8("VBCD") .or.
     &    name==nc4to8("VASD") .or. name==nc4to8("VMDD") .or.
     &    name==nc4to8("VSSD") .or. name==nc4to8("VOAG") .or.
     &    name==nc4to8("VBCG") .or. name==nc4to8("VASG") .or.
     &    name==nc4to8("VMDG") .or. name==nc4to8("VSSG") .or.
     &    name==nc4to8("VOAC") .or. name==nc4to8("VBCC") .or.
     &    name==nc4to8("VASC") .or. name==nc4to8("VMDC") .or.
     &    name==nc4to8("VSSC") .or. name==nc4to8("VASI") .or.
     &    name==nc4to8("VAS1") .or. name==nc4to8("VAS2") .or.
     &    name==nc4to8("VAS3")) npack = 1

      ! Additional fields for gas-phase chemistry.
      ! Chemical concentration fields:
      if (name==nc4to8("  O3") .or. name==nc4to8("O3SF") .or.
     &    name==nc4to8("  HH") .or. name==nc4to8("  O1") .or.
     &    name==nc4to8("  OO") .or. name==nc4to8("  H1") .or.
     &    name==nc4to8("  H2") .or. name==nc4to8("  M2") .or.
     &    name==nc4to8("SHNO") .or. name==nc4to8("SH2O")) npack = 2

      if (name==nc4to8("  N1") .or. name==nc4to8("  N2")) npack = 1

      ! Vertically integrated 2-D fields of chemical diagnostics.
      if (name==nc4to8("LT01") .or. name==nc4to8("LT02") .or.
     &    name==nc4to8("LT03") .or. name==nc4to8("LT04") .or.
     &    name==nc4to8("LT05") .or. name==nc4to8("LT06") .or.
     &    name==nc4to8("LT07") .or. name==nc4to8("LT08") .or.
     &    name==nc4to8("LT09") .or. name==nc4to8("LT10") .or.
     &    name==nc4to8("LT11") .or. name==nc4to8("LT12") .or.
     &    name==nc4to8("LT13") .or. name==nc4to8("LT14") .or.
     &    name==nc4to8("LT15") .or. name==nc4to8("LT16") .or.
     &    name==nc4to8("LT17") .or. name==nc4to8("LT18") .or.
     &    name==nc4to8("LT19") .or. name==nc4to8("LT20")) npack = 1

      if (name==nc4to8("LT21") .or. name==nc4to8("LT22") .or.
     &    name==nc4to8("LT23") .or. name==nc4to8("LT24") .or.
     &    name==nc4to8("LT25") .or. name==nc4to8("LT26") .or.
     &    name==nc4to8("LT27") .or. name==nc4to8("LT28") .or.
     &    name==nc4to8("LT29") .or. name==nc4to8("LT30") .or.
     &    name==nc4to8("LT31") .or. name==nc4to8("LT32")) npack = 1
  
      if (name==nc4to8("LS01") .or. name==nc4to8("LS02") .or.
     &    name==nc4to8("LS03") .or. name==nc4to8("LS04") .or.
     &    name==nc4to8("LS05") .or. name==nc4to8("LS06") .or.
     &    name==nc4to8("LS07") .or. name==nc4to8("LS08") .or.
     &    name==nc4to8("LS09") .or. name==nc4to8("LS10") .or.
     &    name==nc4to8("LS11") .or. name==nc4to8("LS12") .or.
     &    name==nc4to8("LS13") .or. name==nc4to8("LS14") .or.
     &    name==nc4to8("LS15") .or. name==nc4to8("LS16") .or.
     &    name==nc4to8("LS17") .or. name==nc4to8("LS18") .or.
     &    name==nc4to8("LS19") .or. name==nc4to8("LS20")) npack = 1

      if (name==nc4to8("LS21") .or. name==nc4to8("LS22") .or.
     &    name==nc4to8("LS23") .or. name==nc4to8("LS24") .or.
     &    name==nc4to8("LS25") .or. name==nc4to8("LS26")) npack = 1

      ! Lightning diagnostics (2-D)
      if (name==nc4to8("ELNI") .or. name==nc4to8("ELCG") .or.
     &    name==nc4to8("ELCC")) npack = 1

      ! 2-D accumulated flux diagnostics
      if (name==nc4to8("WF01") .or. name==nc4to8("WF02") .or.
     &    name==nc4to8("WF03") .or. name==nc4to8("WF04") .or.
     &    name==nc4to8("WF05") .or. name==nc4to8("WF06")) npack = 1

      ! 3-D accumulated chemical diagnostics
      if (name==nc4to8("CH01") .or. name==nc4to8("CH02") .or.
     &    name==nc4to8("CH03") .or. name==nc4to8("CH04") .or.
     &    name==nc4to8("CH05") .or. name==nc4to8("CH06") .or.
     &    name==nc4to8("CH07") .or. name==nc4to8("CH08") .or.
     &    name==nc4to8("CH09") .or. name==nc4to8("CH10") .or.
     &    name==nc4to8("CH11") .or. name==nc4to8("CH12") .or.
     &    name==nc4to8("CH13")) npack = 2

      ! * "Unpack" the field
      call recup2(g,ibuf)

      ! Convert from model internal format to "regular" grid.
      ! Note that no grid conversion is done if ibuf(7) /=0 since this
      ! special flag is set only for s/l fields or RCM.
      ! Reset ibuf(7) to zero if s/l field so that diagnostics will work
      ! (for instance, CMPLBL of transformed temperature from spectral
      ! versus s/l moisture). This is not done for the RCM cases.

      if (ibuf(7) == 0) then
        call cgtorg (f,g,ibuf(5),ibuf(6))
      else
        do i = 1, ibuf(5)*ibuf(6)
          f(i) = g(i)
        enddo
        if (ibuf(7) == 10) ibuf(7) = 0
      endif
      ibuf(8) = npack
  270 continue

      ! Preserve the label of the first matching record to be displayed at
      ! the end to the standard output file.
      if (nrec(n) == 1 ) then
        do i = 1, 8
          ibufs(i,n) = ibuf(i)
        enddo
      endif
 
      call putfld2 (npak(n),f,ibuf,maxx)
      goto 210
 
  400 continue
 
      ! Abort on empty input file.
      if (nrecs == 0) then
        write(6,6000)
        call xit('pakgcm9',-2)
      endif

      ! *********************************************************
      ! * For each kind; display the label of the first processed
      ! *                record and the number of records added.
      ! *********************************************************
 
      write(6,6010) nrecs
      write(6,*) "pakgcm9_gaschem package found"
      do n = 1, nkind
        if (nrec(n) > 0) write(6,6030) (ibufs(i,n),i=1,8)
        write(6,6020) kt(n),nrec(n)
      enddo
      call xit('PAKGCM9',0)

!--------------------------------------------------------------------
 6000 format ('pakgcm9_gaschem: EMPTY INPUT FILE')
 6010 format ('pakgcm9_gaschem: processed a total of ', i9,' input
     &        records,', ' and wrote:',/)
 6020 format (' ',a4,' records =',I9)
 6030 format (' ',50x,a4,i10,1x,a4,i10,4i6)

      end program pakgcm9_gaschem
