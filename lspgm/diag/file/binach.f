      PROGRAM BINACH
C     PROGRAM BINACH (BIN,       CHAR,       OUTPUT,                    )       B2
C    1          TAPE1=BIN, TAPE2=CHAR, TAPE6=OUTPUT)
C     ----------------------------------------------                            B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     DEC 18/02 - F.MAJAESS (REVISE FOR 128-CHARACTERS/LINE IN "CHAR" RECORDS)  B2
C     MAR 05/02 - F.MAJAESS (REVISED TO HANDLE "CHAR" KIND RECORDS)             
C     JUL 06/92 - E. CHAN  (MODIFY TO WRITE SUPERLABEL AS CHARACTER DATA)       
C     FEB 17/92 - E. CHAN  (FLAG UNIT 2 FOR FORMATTED I/O IN CALL TO JCLPNT)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     OCT 30/91 - F.MAJAESS (ALLOW PRESERVING ORIGINAL PACKING DENSITY)
C     DEC 06/83 - R.LAPRISE, S.J.LAMBERT, B.DUGAS.
C                                                                               B2
CBINACH  - CONVERT A STANDARD CCRN FILE TO CHARACTER FORM               1  1    B1
C                                                                               B3
CAUTHORS - R.LAPRISE, S.J.LAMBERT                                               B3
C                                                                               B3
CPURPOSE - CONVERT A STANDARD CCRN FILE FROM BINARY TO CHARACTER FORM.          B3
C          CHARACTER FILE SHOULD BE DECLARED BLOCKED FIXED LENGTH RECORDS       B3
C          OF LENGTH 80 FOR PORTABILITY.                                        B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      BIN = BINARY (PACKED) STANDARD CCRN FILE.                                B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      CHAR = CODED CHARACTER FILE.  FOR TRANSPORTABILITY,  THIS FILE           B3
C             SHOULD BE DECLARED AS BLOCKED,  FIXED LENGTH RECORDS,             B3
C             OF LENGTH 80.  UNDER SCOPE,  THE JCL FOR THIS IS...               B3
C             FILE(CHAR,BT=K,RT=F,RB=200,FL=80,MBL=16000,CM=YES,PD=OUTPUT)      B3
C-------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/LCM/F(SIZES_BLONP1xBLAT)
C
      COMMON/BUFCOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C
      CHARACTER*80 LABEL
      CHARACTER*8  CBUF(1)
      EQUIVALENCE (LABEL,IDAT),(CBUF,IDAT)
C
      LOGICAL OK
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C-----------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,-2,6)
      REWIND 1
      REWIND 2
      NRECS=0
C
C     * READ IN BINARY RECORD.
C
100   CALL GETFLD2(1,F,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6000) NRECS
        IF(NRECS.EQ.0)THEN
          CALL                                     XIT('BINACH',-1)
        ELSE
          CALL                                     XIT('BINACH',0)
        ENDIF
      ENDIF
      NWDS=IBUF(5)*IBUF(6)
      IF(IBUF(1).EQ.NC4TO8("SPEC").OR.
     +   IBUF(1).EQ.NC4TO8("FOUR")    ) NWDS=NWDS*2
C
C     * WRITE OUT CHARACTER RECORD.
C
      IF(IBUF(8).GT.0) IBUF(8)=-IBUF(8)
      IF(IBUF(1).NE.NC4TO8("LABL").AND.IBUF(1).NE.NC4TO8("CHAR"))THEN
        WRITE(2,6010) IBUF,(F(I),I=1,NWDS)
      ELSE
        IF(IBUF(1).EQ.NC4TO8("CHAR"))THEN
          WRITE(2,6040) IBUF,(CBUF(I),I=1,NWDS)
        ELSE
          WRITE(2,6030) IBUF,LABEL
        ENDIF
      ENDIF
      NRECS=NRECS+1
      GO TO 100
C-----------------------------------------------------------------------
 6000 FORMAT('0BINACH CONVERTED ',I5,' RECORDS')
 6010 FORMAT(1X,A4,I10,1X,A4,5I10,10X,/,(1P6E22.15))
 6030 FORMAT(1X,A4,I10,1X,A4,5I10,10X,/,A80)
 6040 FORMAT(1X,A4,I10,1X,A4,5I10,10X,/,(16A8))
      END
