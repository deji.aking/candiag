      PROGRAM XLFILE
C     PROGRAM XLFILE (FILE,       XLFIL,       INPUT,       OUTPUT,     )       B2
C    1          TAPE1=FILE, TAPE2=XLFIL, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------------             B2
C                                                                               B2
C     JUL 19/04 - F.MAJAESS (FIX "LBUF" WRITE STATEMENT)                        B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       
C     FEB 21/92 - E. CHAN  (CONVERT SUPERLABELS TO A64 FORMAT)                  
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAR 06/89 - F.MAJAESS (INCREASE FIELD DIMENSION TO 66000)                 
C     DEC 06/83 - B.DUGAS.
C     JAN 12/81 - J.D.HENDERSON 
C                                                                               B2
CXLFILE  - ADDS SUPERLABELS TO A FILE FROM CARDS                        1  1 C  B1
C                                                                               B3
CAUTHOR  - J.D.HENDERSON                                                        B3
C                                                                               B3
CPURPOSE - TO CREATE A FILE OF SUPERLABELED FIELDS FROM A FILE OF               B3
C          ONE LEVEL FIELDS WITHOUT SUPERLABELS.                                B3
C          THE SUPERLABELS ARE READ FROM CARDS (ONE FOR EACH FIELD).            B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      FILE  = CONTAINS A SERIES OF ONE LEVEL FIELDS.                           B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      XLFIL = FILE COPIED FROM "FILE", (RECORDS CONVERTED TO XSAVE FORMAT),    B3
C              WITH SUPERLABELS ADDED TO EACH FIELD AS READ FROM CARDS.         B3
C 
CINPUT PARAMETER... 
C                                                                               B5
C      LABEL = SUPERLABEL TO BE ADDED TO THE OUTPUT FILE.                       B5
C              ONE SUPERLABEL IS READ FOR EACH FIELD IN "FILE".                 B5
C                                                                               B5
CEXAMPLE OF INPUT CARD(S)...                                                    B5
C                                                                               B5
C*  XLFILE    GROUND COVER (DAY 00001)                                          B5
C*  XLFILE    GROUND COVER (DAY 00032)                                          B5
C*  XLFILE    GROUND COVER (DAY 00182)                                          B5
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LBUF(28) 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
      CHARACTER LABEL*80,LABLRD*64 
      EQUIVALENCE (LBUF(9),LABEL)
C 
      LOGICAL OK
      COMMON/MACHTYP/MACHINE,INTSIZE
C 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C-------------------------------------------------------------------- 
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
      CALL SETLAB(LBUF,NC4TO8("LABL"),0,NC4TO8("LABL"),0,10,1,0,1)
C 
C     * READ THE NEXT RECORD FROM THE FILE. 
C 
  150 CALL RECGET(1,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                       XIT('XLFILE',0)
      ENDIF 
C 
C     * READ THE NEXT LABEL CARD. 
C 
      READ(5,5010,END=902) LABLRD                                               B4
      LABEL(1:64)=LABLRD
      LABEL(65:80)='                '
C 
C     * WRITE THE LABEL AND RECORD TO FILE XLFIL. 
C 
      CALL FBUFOUT(2,LBUF,10*MACHINE+8,K)
      WRITE(6,6024) (LBUF(J),J=1,8),LABLRD
      CALL RECPUT(2,IBUF) 
      WRITE(6,6024) IBUF
      GO TO 150 
C 
C     * E.O.F. ON INPUT.
C 
  902 CALL                                         XIT('XLFILE',-1) 
C-------------------------------------------------------------------- 
 5010 FORMAT(10X,A64)                                                           B4
 6024 FORMAT(' ',A4,I10,2X,A4,I10,4I6,5X,A64)
      END
