      PROGRAM SUBAREA
C     PROGRAM SUBAREA (XIN,       XOUT,       INPUT,       OUTPUT,      )       D2
C    1           TAPE1=XIN, TAPE2=XOUT, TAPE5=INPUT, TAPE6=OUTPUT,
C    2          TAPE45=PLTINFO                                   )
C     ------------------------------------------------------------              D2
C                                                                               D2
C     NOV 02/04 - S.KHARIN (MODIFIED "GGPNL2" SUBROUTINE TO RESPECT MISSING     D2
C                           VALUES. ADD OPTION FOR A CUSTOM STEREOGRAPHIC       D2
C                           PROJECTION.)                                        D2
C     MAR 17/04 - F.MAJAESS (ENSURE CLOSURE OF UNIT 45 BEFORE REMOVING          
C                            "pltinfo" FILE VIA A SYSTEM CALL)                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     MAR 11/96 - F.MAJAESS (ISSUE WARNING WHEN PACKING DENSITY <= 0)
C     AUG 21/95 - F.MAJAESS (CHANGE SAVED DATA PACKING DENSITY TO 1 FROM 3)
C     MAR 08/93 - T. JANG    (CHANGE FILNAME "PLTINFO" FROM UPPERCASE TO
C                            LOWERCASE)
C     JAN 26/93 - E. CHAN    (OPEN UNIT 45 FOR UNFORMATTED I/O)
C     JUL 13/92 - E. CHAN    (DIMENSION SELECTED VARIABLES AS REAL*8)
C     JAN 29/92 - E. CHAN    (CONVERT HOLLERITH LITERALS TO ASCII, REPLACE
C                             CALL ASSIGN WITH OPEN, AND REPLACE CALL
C                             RELEASE WITH A CALL SYSTEM TO REMOVE THE FILE)
C     AUG 13/90 - F. MAJAESS (ADD "SLON" ARRAY, MODIFY "LLCAL" CALL)
C     JUL 04/90 - F. MAJAESS (IMPLEMENT ISSUING A WARNING WHEN SUB-AREA
C                                 COORDINATES IN "PLTINFO" GET CHANGED)
C     FEB 28/90 - F. MAJAESS  (CORRECT D60 VALUE (=3.81E5))
C     OCT 30/89 - F. MAJAESS  (CORRECT LAT/LON VALUES FOR P.S. PLOT)
C     AUG 10/87 - M.SUTCLIFFE, M.LAZARE, R.DALEY
C                                                                               D2
CSUBAREA - CONVERTS A GLOBAL CARTESIAN GRID TO A SMALLER AREA           1  1 C GD1
C                                                                               D3
CAUTHORS - M.SUTCLIFFE, R.DALEY, M.LAZARE.                                      D3
C                                                                               D3
CPURPOSE - CONVERTS A FILE OF GAUSSIAN OR LAT-LON GRIDS TO A FILE OF POLAR      D3
C          STEREOGRAPHIC, GAUSSIAN OR LAT-LON GRIDS REPRESENTATIVE OF A         D3
C          SUB-AREA OF THE GLOBE, BY LINEAR OR CUBIC INTERPOLATION.             D3
C          NOTE - IN ADDITION TO XOUT AND THE OUTPUT FILE, SUBAREA ALSO         D3
C                 WRITES CO-ORDINATE INFORMATION TO "PLTINFO" FILE              D3
C                 (UNIT 45) TO BE SUBSEQUENTLY USED BY GGPLOT.                  D3
C            ***  THE USER SHOULD CALL GGPLOT IMMEDIATELY AFTER SUBAREA         D3
C                 AND BEFORE CALLING SUBAREA AGAIN, (EXCEPT WHEN PLOTTING       D3
C                 MULTIPLE FIELDS WITH A SINGLE CALL TO GGPLOT).                D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      XIN  = FILE CONTAINING GAUSSIAN-GRID OR LAT-LONG GLOBAL FIELDS.          D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      XOUT = FILE CONTAINING POLAR-STEREOGRAPHIC OR CARTESIAN FIELDS           D3
C             REPRESENTING A SUB-AREA OF THE GLOBE.                             D3
C
CINPUT PARAMETERS...
C                                                                               D5
C      CARD 1-                                                                  D5
C      ------                                                                   D5
C      DLAT1,DLON1 = CARTESIAN COORDINATE IN DEGREES OF LOWER LEFT CORNER       D5
C                    OF PLOT. LATITUDES RANGE FROM -90 TO +90, FOR THE          D5
C                    SOUTH AND NORTH POLES RESPECTIVELY. LONGITUDES RANGE       D5
C                    FROM -180 TO +180, WITH GREENWHICH MERIDIAN AT 0.          D5
C      DLAT2,DLON2 = CARTESIAN COORDINATE IN DEGREES OF UPPER RIGHT CORNER      D5
C                    OF PLOT. COORDINATES NOT USED FOR DEFAULT P-S OUTPUT.      D5
C      DGRW        = ANGLE OF GREENWHICH MERIDIAN OFF HORIZONTAL,               D5
C                    NON-DEFAULT P-S OUTPUT ONLY.                               D5
C      LX          > 0, THE RESOLUTION OF THE INTERPOLATED P-S OUTPUT FILE.     D5
C                       LX GRID POINTS ARE USED ALONG THE X-AXIS, WITH THE      D5
C                       SAME RESOLUTION USED ALONG Y-AXIS.                      D5
C                       VALUES OF 30 TO 110 ARE SUGGESTED.                      D5
C                  <=0, CHOOSES ONE OF THE DEFAULT P-S PROJECTIONS BELOW.       D5
C                  = 0, STANDARD POLAR STEREOGRAPHIC PROJECTION OVER N OR       D5
C                       S POLE BASED ON CHOICE FOR NHEM.                        D5
C                       IF NHEM=1 (NH) THIS IS EQUIVALENT TO:                   D5
C                         LX=100, DLAT1=-9.4., DLON1=-122.8.,                   D5
C                         DLAT2=-9.4., DLON2=57.2, DGRW=-10.                    D5
C                       IF NHEM=2 (SH) THIS IS EQUIVALENT TO:                   D5
C                         LX=100, DLAT1=9.4., DLON1=122.8.,                     D5
C                         DLAT2=9.4., DLON2=-57.2, DGRW=-10.                    D5
C                  =-1, DEFAULT NORTH AMERICAN P-S SUB-AREA.                    D5
C                       EQUIVALENT TO:                                          D5
C                         LX=100, DLAT1=15., DLON1=-115.,                       D5
C                         DLAT2=70., DLON2=1., DGRW=-10., NHEM=1.               D5
C                  =-2, DEFAULT ASIAN P-S SUB-AREA.                             D5
C                       EQUIVALENT TO:                                          D5
C                         LX=100, DLAT1=-10., DLON1=63.,                        D5
C                         DLAT2=45., DLON2=179., DGRW=-180., NHEM=1.            D5
C                  =-3, DEFAULT EUROPEAN P-S SUB-AREA.                          D5
C                       EQUIVALENT TO:                                          D5
C                         LX=100, DLAT1=26., DLON1=-7.,                         D5
C                         DLAT2=70., DLON2=90., DGRW=-115., NHEM=1.             D5
C                  =-4, DEFAULT AUSTRALIAN P-S SUB-AREA.                        D5
C                       EQUIVALENT TO:                                          D5
C                         LX=100, DLAT1=-40., DLON1=84.,                        D5
C                         DLAT2=2., DLON2=156., DGRW=-135., NHEM=2.             D5
C                  =-5, DEFAULT ARCTIC OR ANTARCTIC P-S SUB-AREA, BASED ON      D5
C                       CHOICE FOR NHEM.                                        D5
C                       IF NHEM=1 (NH) THIS IS EQUIVALENT TO:                   D5
C                         LX=100, DLAT1=45., DLON1=-122.,                       D5
C                         DLAT2=45., DLON2=57.4., DGRW=-10., NHEM=1.            D5
C                       IF NHEM=2 (SH) THIS IS EQUIVALENT TO:                   D5
C                         LX=100, DLAT1=-45., DLON1=122.,                       D5
C                         DLAT2=-45., DLON2=-57.4., DGRW=-10., NHEM=2.          D5
C                  =-9, CUSTOM P-S SUB-AREA.                                    D5
C                       THE PARAMETERS LX,LY,IP,JP,D60 ARE READ FROM CARD2.     D5
C                              
C                  USED ONLY FOR P-S GRID OUTPUT.                               D5
C      NHEM        =0,1 PLOT NORTH HEMISPHERE.                                  D5
C                  = 2  PLOT SOUTH HEMISPHERE.                                  D5
C                  USED FOR NON-DEFAULT P-S OUTPUT, AND WHEN LX= 0 OR -5        D5
C      KIND        = 1,       USE LINEAR INTERPOLATION,                         D5
C                  OTHERWISE, USE CUBIC  INTERPOLATION.                         D5
C                  USED ONLY FOR P-S OUTPUT.                                    D5
C      NINTYP      = 0,       INPUT FILE IS ON GAUSSIAN GRID,                   D5
C                  OTHERWISE, INPUT FILE IS ON LAT-LON  GRID.                   D5
C      NOUTYP      = 0, OUTPUT FILE IS ON A SUBSET OF THE INPUT CARTESIAN       D5
C                       GRID. CHOSEN COORDINATES ARE ADJUSTED TO THE            D5
C                       NEAREST COORDINATES ON THE INPUT GRID.                  D5
C                  OTHERWISE, OUTPUT FILE IS INTERPOLATED TO A P-S              D5
C                       PROJECTION WITH RESOLUTION LX.                          D5
C                                                                               D5
C      CARD 2-                                                                  D5
C      ------                                                                   D5
C              THIS CARD IS READ ONLY WHEN LX=-9 (CUSTOM P-S SUB-AREA).         D5
C                                                                               D5
C      LX,LY = POLAR STEREOGRAPHIC GRID DIMENSIONS.                             D5
C      IP,JP = GRID COORDINATES OF THE POLE                                     D5
C      D60   = GRID LENGTH AT 60 DEGREES (METERS)                               D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C*SUBAREA.      -80.        5.       80.      179.                              D5
C(SUBSET OF THE INPUT GRID,NOUTYP=0)                                            D5
C                                                                               D5
C*SUBAREA.                                                      0    1    1  0 1D5
C(STANDARD NH POLAR STEROEGRAPHIC,NOUTYP=1,LX=0,NHEM=1.                         D5
C THE ABOVE CARD IS EQUIVALENT TO THE CARD BELOW)                               D5
C*SUBAREA.      -9.4    -122.8      -9.4      57.2      -10.  100    1    1  0 1D5
C                                                                               D5
C*SUBAREA.        0.    -122.8        0.      57.2      -10.  100    1    1  0 1D5
C(CUSTOM NH POLAR STEROEGRAPHIC,NOUTYP=1,LX=100,NHEM=1.                         D5
C THIS IS A SOMEWHAT REDUCED VERSION OF THE ABOVE NH POLAR-STEREOGRAPHIC AREA.  D5
C                                                                               D5
C*SUBAREA.                                               20.   -9    1    1  0 1D5
C*SUBAREA   125   95   38  105    5.00E4                                        D5
C(CUSTOM NH POLAR STEREOGRAPHIC, CANADA)                                        D5
C-------------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/GPS(SIZES_BLONP1xBLAT),GG(SIZES_BLONP1xBLAT)
      LOGICAL OK, EX, STAT
      REAL SLAT(SIZES_BLAT),SLON(SIZES_BLONP1)
      REAL*8 SL,CL,WL,WOSSL,RAD
C
      COMMON/GAUS/SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT)
      COMMON/GAUS/WOSSL(SIZES_BLAT),RAD(SIZES_BLAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C----------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * CARTESIAN COORDINATES OF LOWER LEFT AND UPPER RIGHT POINTS,
C     * DEFINED BY (DLAT1,DLON1) AND (DLAT2,DLON2), RESPECTIVELY, YIELD
C     * POLAR STEREOGRAPHIC GRID SIZE (LX,LY) WITH POLE AT (IP,JP).
C     * GRID SIZE IS D60(M) AND ORIENTATION IS DGRW (DEG).
C     * OR, IF LAT-LON OUTPUT IS CHOSEN, THESE CO-ORDINATES YIELD
C     * LAT-LON GRID INDICES (I1,J1), (I2,J2).
C
      READ(5,5010,END=902) DLAT1,DLON1,DLAT2,DLON2,DGRW,LX,NHEM,KIND,           D4
     1                     NINTYP,NOUTYP                                        D4
      INTERP=3
      IF(KIND.EQ.1) INTERP=1
      IF(NHEM.EQ.0) NHEM=1
      WRITE(6,6005) INTERP
      WRITE(6,6010) LX,DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM,NINTYP,NOUTYP
C
C     * GET GRID SIZE FROM FIRST FIELD IN THE FILE.
C
      CALL RECGET(1,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('SUBAREA',-1)
      IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)
      REWIND 1
      WRITE(6,6025) IBUF
      ILG1=IBUF(5)
      ILAT=IBUF(6)
C
C
C     * COMPUTE LAT-LON OR GAUSSIAN LATITUDES OF INPUT FILE IN
C     * DEGREES FROM THE SOUTH POLE
C
      IF (NINTYP.NE.0) THEN
C
        NLAT=ILAT-1
        RY=180.E0/NLAT
        RY=1.E-10*ANINT(1.E10*RY)
        SLAT(1)=0.E0
        DO 100 J=1,NLAT
  100   SLAT(J+1)=SLAT(J)+RY
C
      ELSE
C
        ILATH=ILAT/2
        CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)
        CALL TRIGL (ILATH,SL,WL,CL,RAD,WOSSL)
        DO 110 I=1,ILAT
          SLAT(I)=(RAD(I)*180.E0/3.14159E0)+90.E0
  110   CONTINUE
C
      ENDIF
C
C
C     * SET INPUT, OUTPUT COORDINATES FOR POLAR STEREOGRAPHIC OR LAT-LON GRID
C
      NDFLT=1
      IF (NOUTYP.NE.0) THEN
C
C       * POLAR-STEREOGRAPHIC OUTPUT CASE
C
        IF (LX.EQ.-1) THEN
          LX=100
          DLAT1=15.E0
          DLON1=-115.E0
          DLAT2=70.E0
          DLON2=1.E0
          DGRW=-10.E0
          NHEM=1
          WRITE(6,6012)
C
        ELSE IF (LX.EQ.-2) THEN
          LX=100
          DLAT1=-10.E0
          DLON1=63.E0
          DLAT2=45.E0
          DLON2=179.E0
          DGRW=-180.E0
          NHEM=1
          WRITE(6,6015)

        ELSE IF (LX.EQ.-3) THEN
          LX = 100
          DLAT1=26.E0
          DLON1=-7.E0
          DLAT2=70.E0
          DLON2=90.E0
          DGRW=-115.E0
          NHEM=1
          WRITE(6,6016)

        ELSE IF (LX.EQ.-4) THEN
          LX=100
          DLAT1=-40.E0
          DLON1=84.E0
          DLAT2=2.E0
          DLON2=156.E0
          DGRW=-135.E0
          NHEM=2
          WRITE(6,6017)
C
        ELSE IF (LX.EQ.-5) THEN
          LX=100
          IF (NHEM.EQ.1) THEN
            DLAT1=45.E0
            DLON1=-122.E0
            DLAT2=45.E0
            DLON2=57.4E0
            WRITE(6,6018)
          ELSE
            DLAT1=-45.E0
            DLON1=122.E0
            DLAT2=-45.E0
            DLON2=-57.4E0
            WRITE(6,6019)
          ENDIF
          DGRW=-10.E0
C
        ELSE IF (LX.EQ.-9) THEN
C
C         * READ THE SECOND CARD
C
          READ(5,5020,END=903) LX,LY,IP,JP,D60                                  D4
          WRITE(6,6009) LX,LY,IP,JP,D60
          NDFLT=0
          X=FLOAT(1-IP)
          Y=FLOAT(1-JP)
          CALL LLFXY(DLAT1,DLON1,X,Y,D60,DGRW,NHEM) 
          X=FLOAT(LX-IP)
          Y=FLOAT(LY-JP)
          CALL LLFXY(DLAT2,DLON2,X,Y,D60,DGRW,NHEM) 
          WRITE(6,6013)
C
        ELSE IF (LX.LT.0) THEN
          WRITE(6,6011)
          CALL                                     XIT('SUBAREA',-2)
        ENDIF
C
        IF(LX.GT.0) THEN
          WRITE(6,6023) DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM
          CALL PSCAL(LY,IP,JP,D60,LX,DGRW,DLAT1,DLON1,DLAT2,DLON2,
     1               NHEM,NDFLT,NBADPS)
          IF (NBADPS.NE.0) THEN
            WRITE(6,6026) NBADPS
            CALL                                   XIT('SUBAREA',-3)
          ENDIF
        ELSE
          IF (NHEM.EQ.1) THEN
            DLAT1=-9.4E0
            DLON1=-122.8E0
            DLAT2=-9.4E0
            DLON2=57.2E0
          ELSE
            DLAT1=9.4E0
            DLON1=122.8E0
            DLAT2=9.4E0
            DLON2=-57.2E0
          ENDIF
          NDFLT=0
          LX=51
          LY=55
          IP=26
          JP=28
          D60=3.81E5
          DGRW=-10.E0
          WRITE(6,6014)
          WRITE(6,6023) DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM
        ENDIF
C
        WRITE(6,6020) LX,LY,IP,JP,D60
        WRITE(6,6022)DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM
C
      ELSE
C
C       * CARTESIAN GRID OUTPUT CASE.
C       * CALCULATE THE INPUT GRID INDICES AND ADJUST DLAT,DLON TO AGREE
C
        WRITE(6,6023)DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM
        CALL LLCAL(DLAT1,DLON1,DLAT2,DLON2,DGRW,I1,J1,I2,J2,SLAT,
     1             SLON,ILG1,ILAT,NBADLL)
        IF (NBADLL.NE.0) THEN
          WRITE(6,6026) NBADLL
          CALL                                     XIT('SUBAREA',-4)
        ENDIF
        WRITE(6,6021)J1,I1,J2,I2
        WRITE(6,6022)DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM
C
      ENDIF
C
C
C     * CHECK AND WRITE CO-ORDINATE INFORMATION OUT FOR GGPLOT
C
      INQUIRE(FILE='pltinfo',EXIST=EX)
      INQUIRE(45,OPENED=STAT)
      IF (STAT) THEN
       CLOSE(45)
      ENDIF
      OPEN(45,FILE='pltinfo',FORM='UNFORMATTED')
      REWIND 45
      IF (EX) THEN
C!!!        READ(45,END=120) DLAT1P,DLON1P,DLAT2P,DLON2P,DGRWP,
C!!!     1                   NHEMP,NOUTYPP
        CALL READPLTINFO(45,DLAT1P,DLON1P,DLAT2P,DLON2P,DGRWP,
     1                   NHEMP,NOUTYPP,IOST)
        IF(IOST.LT.0) GOTO 120

        OK = (DLAT1P.EQ.DLAT1) .AND. (DLON1P  .EQ.DLON1  ) .AND.
     1       (DLAT2P.EQ.DLAT2) .AND. (DLON2P  .EQ.DLON2  ) .AND.
     2       (DGRWP .EQ.DGRW ) .AND. (NHEMP   .EQ.NHEM   ) .AND.
     3                               (NOUTYPP .EQ.NOUTYP)
        PRINT *,' P=',DLAT1P,DLON1P,DLAT2P,DLON2P,DGRWP,NHEMP,NOUTYPP
        PRINT *,' O=',DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM,NOUTYP
        IF (OK) THEN
          GO TO 140
        ELSE
          WRITE(6,6040) DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM,NOUTYP
          GO TO 130
        ENDIF
  120   WRITE(6,6035)
  130   CONTINUE
        CLOSE(45)
        CALL SYSTEM('\rm -f pltinfo')
        OPEN(45,FILE='pltinfo',FORM='UNFORMATTED')
        REWIND 45
      ENDIF
C
C!!!      WRITE(45) DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM,NOUTYP
      CALL WRITEPLTINFO(45,DLAT1,DLON1,DLAT2,
     +     DLON2,DGRW,NHEM,NOUTYP,IOST)
C
C     * PRODUCE THE STEREOGRAPHIC OR LAT-LON SUB-PLOT(S) FOR GGPLOT
C
  140 NR=0
  150 CALL GETFLD2(1,GG,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6030) NR
        CALL                                       XIT('SUBAREA',0)
      ENDIF
      IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)
C
C     * CONVERT FIELD TO LAT-LON OR P.S. GRID
C
      IF (NOUTYP.EQ.0) THEN
        LX=IBUF(5)
        CALL GGILL(GPS,GG,I1,I2,J1,J2,LX,LY)
      ELSE
        CALL GGIPS2(GPS,LX,LY,IP,JP,D60,DGRW,NHEM,
     1               GG,ILG1,ILAT,SLAT,INTERP)
      ENDIF
C
C     * SAVE THE GRIDS. PACKING IS 1.
C
      CALL SETLAB(IBUF,NC4TO8("SUBA"),-1,-1,-1,LX,LY,NHEM,1)
      CALL PUTFLD2(2,GPS,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1
      GO TO 150
C
C
C     * E.O.F. ON INPUT.
C
  902 CALL                                         XIT('SUBAREA',-5)
  903 CALL                                         XIT('SUBAREA',-6)
C-----------------------------------------------------------------------
 5010 FORMAT(10X,5E10.0,3I5,1X,2I2)                                             D4
 5020 FORMAT(10X,4I5,E10.0)                                                     D4
 6005 FORMAT('0INTERP = ',I5)
 6009 FORMAT('0LX,LY,IP,JP,D60 =',4(1X,I5),1X,E12.4)
 6010 FORMAT('0LX,DLAT1,DLON1,DLAT2,DLON2,DGRW,IHEM,NINTYP,NOUTYP =',I4,
     1        5(1X,E12.4),3I3)
 6011 FORMAT('0VALUE OF LX INVALID, ABORTING')
 6012 FORMAT('0STANDARD NORTH AMERICAN POLAR-STEREOGRAPHIC GRID CHOSEN')
 6013 FORMAT('0CUSTOM POLAR-STEREOGRAPHIC GRID CHOSEN')
 6014 FORMAT('0STANDARD POLAR-STEREOGRAPHIC GRID CHOSEN ')
 6015 FORMAT('0STANDARD ASIAN POLAR-STEREOGRAPHIC GRID CHOSEN')
 6016 FORMAT('0STANDARD EUROPEAN POLAR-STEREOGRAPHIC GRID CHOSEN')
 6017 FORMAT('0STANDARD AUSTRALIAN POLAR-STEREOGRAPHIC GRID CHOSEN')
 6018 FORMAT('0STANDARD ARCTIC POLAR-STEREOPRAPHIC GRID CHOSEN')
 6019 FORMAT('0STANDARD ANTARCTIC POLAR-STEREOGRAPHIC GRID CHOSEN')
 6020 FORMAT('0RESULTING LX,LY,IP,JP,D60 =',4I5,E12.4)
 6021 FORMAT('0RESULTING J1,I1,J2,I2 = ',4I5)
 6022 FORMAT('0ADJUSTED DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM = ',5E12.4,I5)
 6023 FORMAT('0CHOSEN DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM = ',5E12.4,I5)
 6025 FORMAT(1X,A4,I10,2X,A4,I10,4I6)
 6026 FORMAT(1X,'ERROR : SUB-AREA CO-ORDINATES INVALID. CODE ',I5)
 6030 FORMAT('0',I6,' RECORDS READ')
 6035 FORMAT(/' ** PLTINFO "FT45" FILE EXISTS AND EMPTY !'/)
 6040 FORMAT(/,
     1       ' **  WARNING   ** SUB-AREA COORDINATES CHANGED IN',
     2       ' PLTINFO "FT45" FILE FOR GGPLOT.'/
     3       ' **  REMINDER  ** NORMAL PROCEDURE IS TO PLOT',
     4       ' PREVIOUSLY "SUBAREAED" FIELDS BY GGPLOT BEFORE '/
     5       '                  PROCEEDING WITH ANOTHER SET OF',
     6       ' FIELDS HAVING DIFFERENT SUB-AREA COORDINATES.'//
     7       ' ** NEW VALUES ** DLAT1,DLON1,DLAT2,DLON2,DGRW,NHEM,',
     8       'NOUTYP=',5E12.4,2I5/)
 6090 FORMAT(/,' *** WARNING: PACKING DENSITY = ', I4,' ***',/)
      END
