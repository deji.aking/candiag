      PROGRAM PSALL 
C     PROGRAM PSALL (PSGRID,       LLGRID,       INPUT,       OUTPUT,   )       D2
C    1         TAPE1=PSGRID, TAPE2=LLGRID, TAPE5=INPUT, TAPE6=OUTPUT) 
C     ---------------------------------------------------------------           D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     JAN 29/92 - E. CHAN    (CONVERT HOLLERITH LITERALS TO ASCII)              
C     NOV   /83 - S. LAMBERT                                                  
C                                                                               D2
CPSALL   - CONVERT POLAR STEREOGRAPHIC GRIDS TO LAT. LONG. GRIDS.       1  1 C  D1
C                                                                               D3
CAUTHOR  - S. LAMBERT                                                           D3
C                                                                               D3
CPURPOSE - CONVERTS A FILE OF POLAR STEREOGRAPHIC GRIDS TO A FILE               D3
C          OF LATITUDE-LONGITUDE GRIDS(ONE HEMISPHERE ONLY).                    D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      PSGRID = INPUT FILE OF POLAR STEREOGRAPHIC GRIDS                         D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      LLGRID = OUTPUT FILE OF LAT. LONG. GRIDS                                 D3
C 
CINPUT PARAMETERS...
C                                                                               D5
C      (IP,JP) = GRID COORDINATES OF THE POLE                                   D5
C      D60     = GRID LENGTH IN METRES                                          D5
C      DGRW    = ORIENTATION OF GREENWICH MERIDIAN                              D5
C      ILG1    = NUMBER OF LONGITUDES IN OUTPUT GRID                            D5
C      ILAT    = NUMBER OF LATITUDES  IN OUTPUT GRID                            D5
C      DLAT    = LATITUDE INCREMENT   OF OUTPUT GRID                            D5
C                                                                               D5
C      NOTE - OUTPUT GRID WILL EXTEND OVER THE ENTIRE 360 DEGREES OF            D5
C             LONGITUDE.  THE TOP OR BOTTOM ROW OF THE LAT-LONG GRID            D5
C             WILL BE THE POLE DEPENDING ON THE HEMISPHERE AND THE              D5
C             LATITUDES WILL BE SPACED DLAT DEGREES APART UNTIL ALL             D5
C             ILAT LONGITUDES ARE USED.                                         D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C* PSALL.    47   51    3.81E6      -10.   73   15        5.                    D5
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION PS(SIZES_LONP1xLAT),GL(SIZES_LONP1xLAT) 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      LOGICAL OK
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C 
C-----------------------------------------------------------------------
C 
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ CARD CONTAINING INPUT AND OUTPUT GRID PARAMETERS 
C 
      READ(5,5000) IP,JP,D60,DRGW,ILG1,ILAT,DLAT                                D4
      ILG=ILG1-1
      NRECS=0 
C 
C     * READ A POLAR STEREOGRAPHIC GRID FROM FILE 1 
C 
  100 CALL GETFLD2(1,PS,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NRECS.EQ.0)THEN
          CALL                                     XIT('PSALL',-1)
        ELSE
          WRITE(6,6010) NRECS 
          CALL                                     XIT('PSALL',0) 
        ENDIF 
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
      NHEM=IBUF(7)
      NI=IBUF(5)
      NJ=IBUF(6)
C 
C     * COMPUTE POINTS ON LAT-LONG GRID 
C 
      DO 150 I=1,ILG
      DO 150 J=1,ILAT 
      DLONG=360.E0/FLOAT(ILG) 
      DLONG=1.E-10*ANINT(1.E10*DLONG) 
      XLONG=DLONG*FLOAT(I-1)
      XLAT=90.E0-DLAT*FLOAT(ILAT-J) 
      IF(NHEM.EQ.2) XLAT=-90.E0+DLAT*FLOAT(J-1) 
      CALL XYFLL(X,Y,XLAT,XLONG,D60,DRGW,NHEM)
      FI=X+FLOAT(IP)
      FJ=Y+FLOAT(JP)
      CALL GINTC2(VAL,PS,NI,NJ,FI,FJ) 
      GL((J-1)*ILG1+I)=VAL
  150 CONTINUE
C 
C     COPY LEFT COLUMN OF GRID TO THE RIGHT COLUMN
C 
      DO 200 J=1,ILAT 
  200 GL(J*ILG1)=GL((J-1)*ILG1+1) 
      IBUF(5)=ILG1
      IBUF(6)=ILAT
      CALL PUTFLD2(2,GL,IBUF,MAXX) 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
      NRECS=NRECS+1 
      GO TO 100 
C 
C-----------------------------------------------------------------------
C 
 5000 FORMAT(10X,2I5,2E10.0,2I5,E10.0)                                          D4
 6010 FORMAT('0',I6,' RECORDS PROCESSED')
 6025 FORMAT(1X,A4,I10,2X,A4,I10,4I6)
      END
