      PROGRAM ZXRHUM
C     PROGRAM ZXRHUM (ZXT,       ZXSH,       ZXRH,       OUTPUT,        )       F2
C    1          TAPE1=ZXT, TAPE2=ZXSH, TAPE3=ZXRH, TAPE6=OUTPUT)
C     ----------------------------------------------------------                F2
C                                                                               F2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       F2
C     JAN 15/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/83 - R.LAPRISE.                                                    
C     NOV 07/80 - J.D.HENDERSON 
C                                                                               F2
CZXRHUM  - CROSS-SECTION CALCULATION OF RELATIVE HUMIDITY               2  1    F1
C                                                                               F3
CAUTHOR  - J.D.HENDERSON                                                        F3
C                                                                               F3
CPURPOSE - CROSS-SECTION CALCULATION OF RELATIVE HUMIDITY.                      F3
C          ZXT  CONTAINS ONE CROSS-SECTION OF TEMPERATURE (DEG K).              F3
C          ZXSH CONTAINS ONE CROSS-SECTION OF SPECIFIC HUMIDITY.                F3
C          RELATIVE HUMIDITY IS SAVED ON FILE ZXRH.                             F3
C          NOTE - MAXIMUN LATITUDES IS $BJ$.                                    F3
C                                                                               F3
CINPUT FILES...                                                                 F3
C                                                                               F3
C      ZXT  = TEMPERATURE       CROSS-SECTIONS                                  F3
C      ZXSH = SPECIFIC HUMIDITY CROSS-SECTIONS                                  F3
C                                                                               F3
COUTPUT FILE...                                                                 F3
C                                                                               F3
C      ZXRH = RELATIVE HUMIDITY CROSS-SECTIONS                                  F3
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/F(SIZES_BLAT),G(SIZES_BLAT) 
C 
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_BLATxNWORDIO)
      DATA A,B,EPS1,EPS2/ 21.656E0, 5423.E0, 0.622E0, 0.378E0 / 
      DATA MAXX/SIZES_BLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * GET THE NEXT LEVEL OF TEMPERATURE AND SPECIFIC HUMIDITY.
C 
      NR=0
  150 CALL GETFLD2(1,F,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('ZXRHUM',-1) 
        CALL                                       XIT('ZXRHUM',0)
      ENDIF 
      WRITE(6,6025) IBUF
      CALL GETFLD2(2,G,NC4TO8("ZONL"),-1,-1,-1,JBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('ZXRHUM',-2) 
      WRITE(6,6025) JBUF
C 
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK) CALL                             XIT('ZXRHUM',-3) 
C 
C     * COMPUTE THE RELATIVE HUMIDITY.
C     * IF THE TEMPERATURE IS ZERO (UNDER THE MTNS DUE TO DELTA-HAT)
C     * THEN THE RELATIVE HUMIDITY IS JUST SET TO 0.5 . 
C     * ANY LEVEL ABOVE 400 MB IS ALSO SET TO 0.5 . 
C 
      CALL LVDCODE(PRES,IBUF(4),1)
      NLAT=IBUF(5)
      DO 210 I=1,NLAT 
      T=F(I)
      Q=G(I)
      F(I)=0.5E0 
      IF(T.EQ.0.E0) GO TO 210 
      IF(PRES.LT.400.E0) GO TO 210
      E=EXP(A-B/T)
      SAT=EPS1*E/(PRES-EPS2*E)
      F(I)=Q/SAT
  210 CONTINUE
C 
C     * PUT THE RELATIVE HUMIDITY ON FILE 3.
C 
      IBUF(3)=NC4TO8("RHUM")
      CALL PUTFLD2(3,F,IBUF,MAXX)
      NR=NR+1 
      GO TO 150 
C---------------------------------------------------------------------
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)
      END
