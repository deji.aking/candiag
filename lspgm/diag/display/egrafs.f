      PROGRAM EGRAFS
C     PROGRAM EGRAFS (NPAKEN,       INPUT,       OUTPUT,                )       A2
C    1          TAPE1=NPAKEN, TAPE5=INPUT, TAPE6=OUTPUT)
C     --------------------------------------------------                        A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JAN 31/96 - F.MAJAESS(REVISE TO HANDLE UP TO 7 LEVELS INSTEAD OF 6)       
C     JUL 21/92 - E. CHAN  (REPLACE UNFORMATTED I/O WITH I/O ROUTINES)          
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAR 02/84 - R.LAPRISE.                                                    
C                                                                               A2
CEGRAFS  - GRAPHS ENERGIES FOR THE GCM                                  1  0 C  A1
C                                                                               A3
CAUTHOR  - R.LAPRISE                                                            A3
C                                                                               A3
CPURPOSE - GRAPHS ENERGIES FOR THE GCM BETWEEN TIMESTEPS MIN AND MAX.           A3
C          NOTE - HAVING READ THE ENERGY FROM NPAKEN FILE, THE 7 LEVELS         A3
C                 ARE...                                                        A3
C                    1) KINETIC ENERGY                                          A3
C                    2) POTENTIAL ENERGY                                        A3
C                    3) TOTAL ENERGY                                            A3
C                    4) TOTAL PRECIPITABLE WATER                                A3
C                    5) MEAN SQUARE VORTICITY                                   A3
C                    6) MEAN SQUARE DIVERGENCE                                  A3
C                    7) HYBRID MOISTURE VARIABLE                                A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C     NPAKEN = GCM ENERGY FILE                                                  A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C      MIN,MAX   = MINIMUM AND MAXIMUM TIMESTEPS                                A5
C                                                                               A5
C      LEV       = LEVEL IN E WHERE GRAPH IS TO BEGIN (1 TO 7).                 A5
C      NC        = NUMBER OF CURVES TO BE PLOTTED AT ONCE (-1,-2,-3,1,2,3)      A5
C                  IF NC IS NEGATIVE SUBTRACT FIRST VALUE FROM EACH CURVE       A5
C      INCR      = INCREMENT BETWEEN SUCCESSIVE GRAPH POINTS.                   A5
C      GMIN,GMAX = RANGE OF THE GRAPH (COMPUTED IF BOTH ARE ZERO).              A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C*EGRAFS           0 999999999                                                  A5
C*  6    1    1                                                                 A5
C*  1   -1    1                                                                 A5
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_TSL

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/ E(SIZES_TSL,7) 
      COMMON/ICOM/   IBUF(8),IDAT(14)
C 
      REAL EN(7)
      LOGICAL OK
C 
      DATA MAXL/14/, LI/SIZES_TSL/
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
C 
C     * READ GRAPH TIMES FROM A CARD. 
C 
      READ(5,5005,END=903) MINN,MAXX                                            A4
      LENGTH=MAXX-MINN+1
      WRITE(6,6010) MINN,MAXX,LENGTH
C 
C     * READ ENERGIES FOR NEXT STEP FROM FILE 1 (UNPACKED). 
C 
      REWIND 1
      N=0 
      LST=-1
      NLEV=-1
  210 CALL GETFLD2 (1,EN,-1,0,NC4TO8("NEXT"),0,IBUF,MAXL,OK)
      IF(.NOT.OK) GOTO 310
      IF(N.EQ.0) WRITE(6,6025) IBUF 
      NST=IBUF(2) 
      LABL=IBUF(3)
      IF(LABL.NE.NC4TO8("ENRG")) CALL              XIT('EGRAFS',-1)
C 
C     * CHECK STEP NUMBER FOR VALIDITY. 
C 
      IF((NST.LT.MINN).OR.(NST.EQ.LST)) GO TO 210
      IF(NST.GT.MAXX) GO TO 310
      IF(N.GT.LI) GO TO 310 
C 
C     * IF OK, PUT ENERGIES INTO NEXT POSITION  IN E. 
C 
      IF(NLEV.LT.0) NLEV=IBUF(5)
      LST=NST 
      N=N+1 
      DO 220 L=1,NLEV
  220 E(N,L)=EN(L)
      GO TO 210 
C 
C     * CHECK GRAPH LENGTH FOR VALIDITY.
C 
  310 IF(N.LE.1)THEN
        WRITE(6,6015) N 
        CALL                                       XIT('EGRAFS',-2) 
      ENDIF 
C 
C     * READ GRAPH CONTROL CARD AND CHECK VALIDITY. 
C     * LEV = LEVEL IN E WHERE GRAPH IS TO BEGIN (1 TO 7).
C     * NC = NUMBER OF CURVES TO BE PLOTTED AT ONCE (1 TO 3). 
C     * INCR = INCREMENT BETWEEN SUCCESSIVE GRAPH POINTS. 
C     * GMIN,GMAX = RANGE OF THE GRAPH (COMPUTED IF BOTH ARE ZERO). 
C 
  330 READ(5,5010,END=900) LEV,NC,INCR,GMIN,GMAX                                A4
      OK=.TRUE. 
      IF(ABS(NC).GT.3) OK=.FALSE.
      IF(LEV.LT.1.OR.LEV.GT.7) OK=.FALSE. 
      IF(.NOT.OK)THEN 
        WRITE(6,6030) LEV,NC
        GO TO 330 
      ENDIF 
C 
C     * IF  NC IS NEGATIVE SUBTRACT FIRST VALUE FROM EACH CURVE . 
C 
      IF( NC.GT.0) GO TO 410
      NC=-NC
      LEVNC=LEV+NC - 1
      DO 350 L=LEV,LEVNC
      EFIRST=E(1,L) 
      DO 350 I=1,N
      E(I,L)=E(I,L)-EFIRST
  350 CONTINUE
C 
C     * PLOT THE GRAPH AND GO BACK FOR THE NEXT CONTROL CARD. 
C 
  410 CALL SPLAT2(E(1,LEV),LI,NC,N,INCR,GMIN,GMAX)
      WRITE(6,6035) LEV,NC,INCR 
      GO TO 330 
C 
C     * E.O.F. ON INPUT.
C 
  900 CALL                                         XIT('EGRAFS',0)
  903 CALL                                         XIT('EGRAFS',-3) 
C---------------------------------------------------------------------
 5005 FORMAT(10X,2I10)                                                          A4
 5010 FORMAT(3I5,2E10.0 )                                                       A4
 6010 FORMAT('0GRAPH FROM STEP',I12,'  TO',I12,3X,'LENGTH =',I6)
 6015 FORMAT(' NOT ENOUGH POINTS TO GRAPH  N=',I6)
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT(' ILLEGAL CARD - LEV,NC=',2I6)
 6035 FORMAT('0 LEV,NC,INCR =',3I6)
      END
